import z3
from collections.abc import Iterable

class Var:
    """
    Hashable class representing named variables in formulas.
    Variables with the same name are considered equal.
    """

    def __init__(self, name):
        """
        Initialise a new variable with a given name.
        two variable with the same name are considered equal.
        """
        self.name = name

    def __str__(self):
        return self.name

    def to_z3(self, prefix = ""):
        """ Return a z3 Integer Variable. Several different variables can be
        obtained by modifying the 'prefix' optional parameter.
        """
        return z3.Int(prefix + self.name)

    def __hash__(self):
        return hash(self.name)
    
    def __eq__(self, other):
        """ Check if two objects represents the same variable."""
        return self.name == other.name

    def __add__(self, other):
        """ Creates a Term containing self + other """
        return Term(self) + other
    
    def __radd__(self, other):
        """ Same as __add__, useful for python magic """
        return self.__add__(other)
    
    def __mul__(self, factor):
        """ Return a Term encoding factor * self.
        Because of linearity constraints, only integer factors are supported.
        """
        return Term(self) * factor
    
    def __rmul__(self, factor):
        """ Same as __mul__, useful for python magic """
        return self.__mul__(factor)
    
    def __neg__(self):
        """ Return a term encoding -self """
        return -Term(self)
    
    def __sub__(self, other):
        """ Creates a Term containing self - other """
        return Term(self) - other
    
    def __rsub__(self, other):
        """ Creates a Term containing other - self. """
        return -Term(self) + other

class Term:
    """
    Represent linear terms in formulas.
    Consist of a linear expression over Var variables plus a constant term.
    """

    def __init__(self, arg):
        """
        This constructor can be used to create constant terms by providing
        an integer, or a term consisting of a single variable by providing
        a Var object. More complex terms can be built using +, - and *.
        Raise TypeError if arg is not an integer or a Var.
        """
        self.const = 0
        self.factors = {}
        if isinstance(arg, Var):
            self.factors[arg] = 1
        elif isinstance(arg, int):
            self.const = arg
        else:
            raise TypeError("Cannot convert {} to {}".format(arg, type(self)))

    def get_factor(self, v):
        """
        Return the factor assigned to the variable v. Return 0 by default.
        """
        return self.factors.get(v, 0)

    def cleanup(self):
        """
        Removes any variable with assigned 0 factors from self.factors.
        """
        self.factors = {v: f for v, f in self.factors.items() if f != 0}

    def copy(self):
        "Return a cleaned up copy of the object."
        other = Term(0)
        other.factors = {v: f for v, f in self.factors.items() if f != 0}
        other.const = self.const
        return other

    def to_z3(self, prefix = ""):
        """
        Return a z3 Term representing the object. Modifying prefix modifies all the
        variables that appear in the term using their respective prefix argument.
        """
        list_monome = [v.to_z3(prefix) * f for v, f in self.factors.items()]
        return z3.Sum(list_monome) + self.const

    def var_set(self):
        """ Returns the set of variables that appear in the term """
        return set(self.factors.keys())

    def __str__(self):
        self.cleanup()
        first_tuple = True
        result = ""
        for v, f in self.factors.items():
            assert(f != 0)
            if first_tuple and f == 1:
                # Do not print f nor its sign
                first_tuple = False
                result = str(v)
            elif first_tuple and f == -1:
                # Do not print f but print its sign
                first_tuple = False
                result = "-{}".format(v)
            elif first_tuple:
                # print f, its sign can be handled by integer str method
                first_tuple = False
                result = "{} {}".format(f, v)
            elif f == 1:
                # Do not print f nor its sign
                result = "{} + {}".format(result, v)
            elif f == -1:
                # Do not print f but print its sign
                result = "{} - {}".format(result, v)
            elif f > 0:
                result = "{} + {} {}".format(result, f, v)
            else:
                # case f < -1, not first tuple
                result = "{} - {} {}".format(result, abs(f), v)
        if first_tuple:
            # If self.factors is empty, just print the const
            result = str(self.const)
        elif self.const > 0:
            result = "{} + {}".format(result, self.const)
        elif self.const < 0:
            result = "{} - {}".format(result, abs(self.const))
        return result

    def convert(arg):
        """
        Try to return a Term that encodes arg. raise TypeError
        if no conversion exists
        """
        if isinstance(arg, Term):
            return arg
        else:
            return Term(arg)

    def __iadd__(self, other):
        """
        Add other to self, modifying self in place.
        """
        try:
            other = Term.convert(other)
        except TypeError:
            # There is some python magic going on here
            return NotImplemented
        self.const += other.const
        for v, f in other.factors.items():
            self.factors[v] = self.factors.get(v, 0) + f
        return self

    def __add__(self, other):
        """
        Create a new Term encoding self + other
        """
        result = self.copy()
        result = result.__iadd__(other) 
        # Not sure what happens if last line is replaced with: result += other
        return result
    
    def __radd__(self, other):
        """ Useful for some python magic """
        return self.__add__(other) # + is commutative
        
    def __imul__(self, factor):
        """"
        Multiply self by factor. Because of linearity constraints,
        only an integer factor is supported.
        """
        try:
            self.const *= factor
            self.factors = {v: f * factor for v, f in self.factors.items()}
        except TypeError: return NotImplemented
        return self

    def __mul__(self, factor):
        """
        Create a new Term encoding self * factor
        """
        result = self.copy()
        return result.__imul__(factor)
    
    def __rmul__(self, factor):
        """ Useful for some python magic """
        return self.__mul__(factor) # * is commutative
    
    def __neg__(self):
        """ Return the oposite of self """
        return self.__mul__(-1)
    
    def __isub__(self, other):
        """
        Substract other from self, modifying self in place.
        """
        try:
            other = Term.convert(other)
        except TypeError:
            # There is some python magic going on here
            return NotImplemented
        self.const -= other.const
        for v, f in other.factors.items():
            self.factors[v] = self.factors.get(v, 0) - f
        return self
    
    def __sub__(self, other):
        """
        Create a new Term encoding self - other
        """
        result = self.copy()
        result = result.__isub__(other) 
        # Not sure what happens if last line is replaced with: result -= other
        return result
    
    def __rsub__(self, other):
        """ Useful for some python magic """
        return self.__neg__().__add__(other) # other - self = -self + other

class Formula:
    """
    An interface that shall be overloaded by operators such as Not, Or, And...

    Subclasses should implement the __str__, to_z3 and var_set methods.
    """

class Proposition(Formula):
    """
    Represent basic propositions, contains a term that is meant to be non negative. Hashable.
    """
    def __init__(self, term):
        self.term = term
    
    def __str__(self):
        return "{} >= 0".format(self.term)
    
    def to_z3(self, prefix=""):
        """ 
        return a z3 formula encoding not(self.son).
        The optional argument prefix is applied to all the variables
        within the formula.
        """
        return self.term.to_z3(prefix) >= 0
    
    def var_set(self):
        """ Return the set of variables that appear in the proposition """
        return self.term.var_set()

class Not(Formula):
    """
    Encode the negation of a Formula.
    """
    def __init__(self, formula):
        self.son = formula
    
    def __str__(self):
        return "not({})".format(self.son)
    
    def to_z3(self, prefix = ""):
        """ 
        Return a z3 formula encoding not(self.son).
        The optional argument prefix is applied to all the variables
        within the formula.
        """
        return z3.Not(self.son.to_z3(prefix))
    
    def var_set(self):
        """ Return the set of variables that appear in the formula """
        return self.son.var_set()

class Or(Formula):
    """
    Encode a disjunction of Formulas.
    Can be constructed either by providing all the formulas as
    arguments or a single iterator.
    """
    def __init__(self, *arg):
        self.sons = []
        if (len(arg) == 1 and isinstance(arg[0], Iterable)):
            # Only one iterable argument provided
            self.sons = arg[0]
        else:
            self.sons = arg
        
    def __str__(self):
        if not(self.sons):
            return "false"
        mapped = ["(" + str(f) + ")" for f in self.sons]
        return " \/ ".join(mapped)
    
    def to_z3(self, prefix=""):
        """ 
        Return a z3 formula encoding Or(self.sons).
        The optional argument prefix is applied to all the variables
        within the formula.
        """
        mapped = [f.to_z3(prefix) for f in self.sons]
        return z3.Or(mapped)
    
    def var_set(self):
        """ Return the set of variables that appear in the formula """
        res = set()
        for son in self.sons:
            res.update(son.var_set())
        return res

class And(Or): # Inheritance to avoid duplicating code
    """
    Encode a conjunction of Formulas.
    Can be constructed either by providing all the formulas as
    arguments or a single iterator.
    """
        
    def __str__(self):
        if not(self.sons):
            return "true"
        mapped = ["("+str(f)+")" for f in self.sons]
        return " /\ ".join(mapped)
    
    def to_z3(self, prefix=""):
        """ 
        Return a z3 formula encoding Or(self.sons).
        The optional argument prefix is applied to all the variables
        within the formula.
        """
        mapped = [f.to_z3(prefix) for f in self.sons]
        return z3.And(mapped)

true = And()
false = Or()

def le(term1, term2):
    """ Return a formula encoding term1 <= term2 """
    return Proposition(Term.convert(term2) - term1)

def lt(term1, term2):
    """ Return a formula encoding term1 < term2 """
    return le(term1, term2 - 1)

def ge(term1, term2):
    """ Return a formula encoding term1 >= term2 """
    return Proposition(Term.convert(term1) - term2)

def gt(term1, term2):
    """ Return a formula encoding term1 > term2 """
    return ge(term1, term2 + 1)

def eq(term1, term2):
    """ return a formula encoding term1 == term2 """
    return And(le(term1, term2), ge(term1, term2))

def ne(term1, term2):
    """ return a formula encoding term1 != term2 """
    return Or(lt(term1, term2), gt(term1, term2))
    
