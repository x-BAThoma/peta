
from Paths import Checker
from BDD import Handler
from utils import frozendict
from collections import namedtuple

TRUE = "t"
UNKNOWN = "_"
FALSE = "."


class State:
    def __init__(self, root_layer, length, valuation):
        self.root_layer = root_layer
        self.length = length
        self.valuation = frozendict(valuation)
        for (lay, _) in valuation.keys():
            assert self.root_layer <= lay < self.root_layer + self.length

    def __hash__(self):
        return hash((self.root_layer, self.length, self.valuation))

    def __eq__(self, other):
        return (
            self.root_layer == other.root_layer and
            self.length == other.length and
            self.valuation == other.valuation
        )

    def __getitem__(self, prop):
        return self.valuation[self.root_layer + self.length - 1, prop]

class AutomatonLayer:
    true = "t"
    unknown = "_"
    false = "f"
    def __init__(self, lta, root_layer, length):
        self.lta = lta
        self.root_layer = root_layer
        self.length = length
        self.checker = Checker(lta, self.root_layer, self.length)
        self.handler = Handler()
        for lay in range(self.root_layer, self.root_layer + self.length):
            for prop in self.lta.get_propositions(lay).keys():
                self.handler.add_variable((lay, prop))
        self.bdd = self.handler.constant(UNKNOWN)

    def find_extentions(self, partial_valuation):
        """ Find all the states that extend the input valuation """
        uncomputed_res = self.handler.get_short_valuation(self.bdd, UNKNOWN, partial_valuation)
        while uncomputed_res is not None:
            is_sat, valuation = self.checker.check(uncomputed_res)
            if is_sat:
                self.bdd = self.handler.update(self.bdd, valuation, TRUE)
            else:
                self.bdd = self.handler.update(self.bdd, valuation, FALSE)
            uncomputed_res = self.handler.get_short_valuation(self.bdd, UNKNOWN, partial_valuation)
        valuations = self.handler.enumerate_consistant_val(self.bdd, TRUE, partial_valuation)
        return [State(self.root_layer, self.length, val) for val in valuations]

class GuardAutomaton:
    true = "t"
    unknown = "_"
    false = "f"
    def __init__(self, lta):
        """ Initialise a guard Automaton for input lta. layer_settings
        should be a list of the length of the paths checked at each layers """
        self.lta = lta

        initial_aut_layer = AutomatonLayer(self.lta, 0, 1)
        self.automata_layers = {(0, 1): initial_aut_layer}
        self.cycle_length = self.lta.num_layers()

    initial_state = State(0, 0, {})

    def right_extentions(self, state, constraints=None):
        if constraints is None:
            constraints = {}
        root_layer = state.root_layer
        new_length = state.length + 1
        for prop in constraints.keys():
            assert prop in self.lta.propositions[(root_layer + new_length - 1) % self.cycle_length]
        aut_layer = None
        if (root_layer, new_length) in self.automata_layers:
            aut_layer = self.automata_layers[root_layer, new_length]
        else:
            aut_layer = AutomatonLayer(self.lta, root_layer, new_length)
            self.automata_layers[root_layer, new_length] = aut_layer
        partial_valuation = dict(state.valuation)
        partial_valuation.update({
            (root_layer + new_length - 1, prop): value
            for prop, value in constraints.items()
        })
        return aut_layer.find_extentions(partial_valuation)

    def pop_left(self, state):
        new_root_layer = state.root_layer + 1
        new_length = state.length - 1
        new_valuation = {
            (lay, prop): val
            for (lay, prop), val in state.valuation.items()
            if lay >= new_root_layer
        }
        if new_root_layer >= self.cycle_length:
            # Everything should be shifted left by one cycle
            new_root_layer -= self.cycle_length
            new_valuation = {
                (lay - self.cycle_length, prop): val
                for (lay, prop), val in new_valuation.items()
                if lay >= new_root_layer
            }
        return State(new_root_layer, new_length, new_valuation)

    def get_successors(self, state, constraints=None):
        edges = self.right_extentions(state, constraints)
        return [self.pop_left(edge) for edge in edges]

    def get_initial_states(self, constraints=None):
        return self.right_extentions(self.initial_state, constraints)

    def print_val_states(self, layer, states):
        prop_names = self.lta.propositions[layer % self.cycle_length].keys()
        max_len_prop = max([len(name) for name in prop_names])
        max_len_state = len(str(len(states) - 1))
        result = " " * (max_len_prop + 3)
        state_names = []
        for i in range(len(states)):
            name = str(i)
            padding = " " * (max_len_state - len(str(i)))
            state_names.append(padding + name)
        result += " ".join(state_names)
        result += "\n"
        for name in prop_names:
            padding = " " * (max_len_prop - len(name))
            result += name + padding + " : "
            vals = []
            for state in states:
                val = UNKNOWN
                if (layer, name) in state.valuation:
                    if state.valuation[(layer, name)]:
                        val = TRUE
                    else:
                        val = FALSE
                padding = " " * (max_len_state - len(val))
                vals.append(padding + val)
            result += " ".join(vals)
            result += "\n"
        return result
