from collections import deque
from utils import AssociativeList, frozendict

class Node:
    """ A class for the nodes of a bdd. These objects should be created by Handler """
    def __init__(self, variable, son_true, son_false):
        self.variable = variable
        self.son_true = son_true
        self.son_false = son_false

    def __hash__(self):
        return hash((self.variable, self.son_true, self.son_false))

    def __eq__(self, other):
        if isinstance(other, Node):
            return (self.variable, self.son_true, self.son_false ==
                    other.variable, other.son_true, other.son_false)
        return False
    
    def __str__(self):
        return "{} ? {} : {}".format(self.variable, self.son_true, self.son_false)

class Leaf:
    """ A class for the leaves of a bdd. These objects should be created by Handler """
    def __init__(self, value):
        self.value = value

    def __hash__(self):
        return hash(self.value)

    def __eq__(self, other):
        if isinstance(other, Leaf):
            return self.value == other.value
        return False
    
    def __str__(self):
        return str(self.value)


class Handler:
    """
    A factory for manipulating BDD. Makes sure each BDD is only created once.
    May consume lots of memory during an execution, might need to be fixed later.
    """
    def __init__(self):
        self.variables = AssociativeList()
        self.nodes = AssociativeList()

    def add_variable(self, var):
        """
        Add the input variable (any hashable object) to the handler.
        It is not necessary to add the variables before using them,
        but this method can be used to control the ordering of the
        variables in the BDDs.
        """
        _ = self.variables.get_index(var)

    def constant(self, value):
        """ Return a Boolean Function that is always equal to the input value. """
        val = Leaf(value)
        return self.nodes.get_index(val)

    def update(self, bdd, valuation, value):
        """ Return a boolean function g equals to f except that g(valuation) = value.
        If valuation is partial, then any valuation that extends it is set to value. """
        index_valuation = {self.variables.get_index(v): value for v, value in valuation.items()}
        return self._update(bdd, index_valuation, value, 0)

    def get_short_valuation(self, bdd, value, partial_val=None):
        """ Find a valuation such that f(valuation) = value. If a partial valuation is provided,
        then the output will extend it. Return None if no such valuation exists.
        Tries to output a valuation as short as possible. """
        index_partial_val = {}
        if partial_val is not None:
            index_partial_val = {self.variables.get_index(v): val
                                 for v, val in partial_val.items()}
        ancestor = {}
        ancestor[bdd] = None
        for node in self.breadth_first_search(bdd, index_partial_val):
            # We use a BFS to build a spanning tree
            if self.is_leaf(node):
                if self.get_value(node) == value:
                    break
            else:
                var = self.get_node_variable(node)
                for val, son in self.get_sons(node, index_partial_val).items():
                    if son not in ancestor:
                        ancestor[son] = (node, val)
        node = self.constant(value)
        if node not in ancestor:
            # The Depth First Search did not encounter node
            return None
        result = {}
        # The resulting valuation is built by going up the tree constructed during the DFS
        while ancestor[node] is not None:
            node, val_node = ancestor[node]
            var_index = self.get_node_variable(node)
            var = self.variables.get_object(var_index)
            result[var] = val_node
        return result

    def enumerate_consistant_val(self, bdd, value, partial_val=None):
        """
        Output all the valuation of the input set of variables that can
        be extended into a full valuation such that bdd(valuation) = value.
        If partial_val is provided, then only valuations that extends partial_val
        will be considered.
        """
        index_partial_val = {}
        if partial_val is not None:
            index_partial_val = {self.variables.get_index(v): val
                                 for v, val in partial_val.items()}
        index_result = self._enumerate_consistant_val(bdd, value, index_partial_val, 0)
        for valuation in index_result:
            yield {self.variables.get_object(var): value for var, value in valuation.items()}

    def get_node(self, variable, son_true, son_false):
        """ Return the index of a node encoding variable ? son_true : son_false' """
        if son_true == son_false:
            # Reduction
            return son_true
        node = Node(variable, son_true, son_false)
        return self.nodes.get_index(node)

    def get_sons(self, bdd, partial_val=None):
        """
        Output a dictionary whose keys are True or False, and whose objects are the
        corresponding successors of the node. Output an empty dictionary if bdd is a leaf.
        If partial_val is provided, restrict the graph accordingly.
        """
        assert not self.is_leaf(bdd)
        if partial_val is not None:
            var = self.get_node_variable(bdd)
            if var in partial_val:
                if partial_val[var]:
                    return {True: self.nodes.get_object(bdd).son_true}
                return {False: self.nodes.get_object(bdd).son_false}
        return {
            False: self.nodes.get_object(bdd).son_false,
            True: self.nodes.get_object(bdd).son_true
        }

    def is_leaf(self, node):
        """ Return True if the node is a leaf (aka a constant)"""
        return isinstance(self.nodes.get_object(node), Leaf)

    def get_value(self, leaf):
        """ Return the value of an input leaf, will fail if the node is not a leaf"""
        return self.nodes.get_object(leaf).value

    def get_node_variable(self, node):
        """ Return the variable of the input node, will fail if the node is a leaf"""
        return self.nodes.get_object(node).variable

    def breadth_first_search(self, root, partial_val=None):
        """
        Iterate over all the descendant of root in breadth-first-search order.
        partial_val can be used to restrict the search.
        """
        queue = deque([root])
        visited = set()
        while queue:
            node = queue.popleft()
            visited.add(node)
            if not self.is_leaf(node):
                for son in self.get_sons(node, partial_val).values():
                    queue.append(son)
            yield node

    def post_iter(self, root, partial_val=None):
        """ Iterate over all the nodes accessible from root such that the
        successors of a node are always outputed before their parent """
        stack = [root]
        visited = set()
        while stack:
            node = stack[-1]
            if node in visited:
                stack.pop()
                yield node
            else:
                visited.add(node)
                for son in self.get_sons(node, partial_val).values():
                    stack.append(son)

    def _update(self, bdd, index_valuation, value, current_var):
        """ An auxiliary recursive function of update. Same effect as update,
        but valuation should be defined on variable indices and only handle variables
        greater or equal to current_var """
        if current_var == len(self.variables):
            # All the variables have been processed
            return self.constant(value)
        if self.is_leaf(bdd) or current_var < self.get_node_variable(bdd):
            # bdd does not care about current_var
            if current_var not in index_valuation:
                # index_valuation does not care about current_var either
                return self._update(bdd, index_valuation, value, current_var+1)
            # bdd does not care about current_var but the valuation does, we add a node.
            son_true, son_false = None, None
            if index_valuation[current_var]:
                son_true = self._update(bdd, index_valuation, value, current_var + 1)
                son_false = bdd
            else:
                son_true = bdd
                son_false = self._update(bdd, index_valuation, value, current_var + 1)
            return self.get_node(current_var, son_true, son_false)
        assert not self.is_leaf(bdd)
        var = self.get_node_variable(bdd)
        son_true = self.get_sons(bdd)[True]
        son_false = self.get_sons(bdd)[False]
        assert current_var == var
        if current_var not in index_valuation:
            son_true = self._update(son_true, index_valuation, value, current_var + 1)
            son_false = self._update(son_false, index_valuation, value, current_var + 1)
        elif index_valuation[current_var]:
            son_true = self._update(son_true, index_valuation, value, current_var + 1)
        else:
            son_false = self._update(son_false, index_valuation, value, current_var + 1)
        return self.get_node(self.get_node_variable(bdd), son_true, son_false)

    def _enumerate_consistant_val(self, bdd, value, index_valuation, current_var):
        """ An auxiliary function to enumerate_consistant_val """
        if current_var == len(self.variables):
            assert(self.is_leaf(bdd))
            if self.get_value(bdd) == value:
                # Any valuation works
                return set([frozendict()])
            else:
                # No valuation works
                return set()
        result = set()
        bdd_var = len(self.variables)
        if not self.is_leaf(bdd):
            bdd_var = self.get_node_variable(bdd)
        if current_var < bdd_var:
            # current_var has no influence over the bdd
            recursive_res = self._enumerate_consistant_val(bdd, value, index_valuation, current_var+1)
            possible_values = [True, False]
            if current_var in index_valuation:
                possible_values = [index_valuation[current_var]]
            for current_value in possible_values:
                result |= {valuation.copy_and_update({current_var: current_value}) for valuation in recursive_res}
        else:
            assert current_var == bdd_var
            for val_son, son in self.get_sons(bdd, index_valuation).items():
                recursive_res = self._enumerate_consistant_val(son, value, index_valuation, current_var+1)
                result |= {valuation.copy_and_update({current_var: val_son}) for valuation in recursive_res}
        return result


        
