import Formulas as fm

x = fm.Var("x")
y = fm.Var("y")
z = fm.Var("z")

print("Test terms:")
t0 = fm.Term(3)
print("t0 = {}; expect 3".format(t0))
t1 = fm.Term(x)
print("t1 = {}; expect x".format(t1))
t2 = t0 + t1
print("t2 = {}; expect x + 3".format(t2))
t3 = 3 * t2
print("t3 = {}; expect 3 x + 9".format(t3))
t4 = t3 - 5
print("t4 = {}; expect 3 x + 4".format(t4))
t5 = t4 - 2*y
print("t5 = {}; expect 3 x - 2 y + 4".format(t5))
t6 = fm.Term(0)
print("t6 = {}; expect 0".format(t6))
t7 = -z
print("t7 = {}; expect -z".format(t7))

print("Test Formulas:")
f0 = fm.false
print("f0: {}; expect false".format(f0))
f1 = fm.Not(fm.false)
print("f1: {}; expect not(false)".format(f1))
f2 = fm.le(t2, t7)
print("f2: {}; expect -z - x - 3 >= 0".format(f2))
f3 = fm.lt(t4, y)
print("f3: {}; expect y - 3x - 5 >= 0".format(f3))
f4 = fm.eq(z, t4)
print("f4: {}; expect (3 x - z + 4 >= 0) /\ (z - 3 x - 4 >= 0)".format(f4))
f5 = fm.Or(fm.gt(z,y), f4)
print("f5: {}; expect (z - y - 1 >= 0) \/ (f4)".format(f5))
f6 = fm.And([f1, f3, fm.ne(y, x)])
print("f6: {}; expect (not(false)) /\ (y - 3 x - 5 >= 0) /\ ((x - y - 1 >= 0) \/ (y - x - 1 >= 0))".format(f6))
f7 = fm.Or(f0, f1, f2)
print("f7: {}; expect (false) \/ (not(false)) \/ (-z - x - 3 >= 0)".format(f7))

print("Test Conversions:")
print("f0: {}; expect Or".format(f0.to_z3()))
print("f1: {}; expect Not(Or)".format(f1.to_z3()))
print("f2: {}; expect z*-1 + x*-1 + -3 >= 0".format(f2.to_z3()))
print("f3: {}; expect y*1 + prefix_x*-3 + -5 >= 0".format(f3.to_z3("prefix_")))
print("f4: {}; expect And(prefix_x*3 + prefix_z*-1 + 4 >= 0, prefix_z*1 + prefix_x*-3 + -4 >= 0)".format(f4.to_z3("prefix_")))