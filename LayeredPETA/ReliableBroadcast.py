import Formulas as fm
from LTA import FLTA
from Paths import Checker

lta = FLTA()

# Parameters
n = lta.add_parameter("n")
t = lta.add_parameter("t")
f = lta.add_parameter("f")

# Parameter Constraint
lta.set_parameter_constraint(fm.And( fm.lt(3*t, n), fm.le(f, t) ))

# State and layers
l0 = lta.append_layer(["v0", "v1"])
v0 = l0["v0"]
v1 = l0["v1"]
l1 = lta.append_layer(["x"])
x = l1["x"]
l2 = lta.append_layer(["acc"])
acc = l2["acc"]

# Guards
lta.set_guard(v1, x, fm.true)
lta.set_guard(v0, x, fm.ge(v1 + f, t + 1))
lta.set_guard(x, acc, fm.ge(x + f, n - t))

# layer_constraint
lta.set_layer_constraint(0, fm.eq(v0 + v1 + f, n))

# Propositions
lta.add_proposition(0, "v0 > 0", fm.gt(v0, 0) )
lta.add_proposition(0, "v1 > 0", fm.gt(v1, 0) )
lta.add_proposition(0, "v1 > t", fm.gt(v1, t) )
e0 = lta.edge_variable(v0, x)
e1 = lta.edge_variable(v1, x)
lta.add_proposition(0, "l0_fair", fm.And(
    fm.eq(v1, e1),
    fm.Or(fm.lt(v1, t + 1),
          fm.eq(v0, e0)
    )
))

ex = lta.edge_variable(x, acc)
lta.add_proposition(1, "l1_fair", fm.Or(
    fm.lt(x, n - t),
    fm.eq(x, ex)
))
lta.add_proposition(1, "x > 0", fm.gt(x, 0) )
lta.add_proposition(2, "acc > 0", fm.gt(acc, 0) )
lta.add_proposition(2, "acc >= n-f", fm.ge(acc, n-f) )

# Check Non triviality: it is possible to end up in acc
checker = Checker(lta, 0, 3)
val = checker.get_empty_val()
val[(2, "acc >= n-f")] = True
print("Non triviality, expect True: {}".format(checker.check(val)[0]))

# Check safety: if no one is in v1, then no one will be in acc
val = checker.get_empty_val()
val[0, "v1 > 0"] = False
val[2, "acc > 0"] = True
print("Safety, expect False: {}".format(checker.check(val)[0]))

# Check Liveness: if enough process are in v1, and the execution is fair, everyone is in acc
val = checker.get_empty_val()
val[0, "v1 > t"] = True
val[0, "l0_fair"] = True
val[1, "l1_fair"] = True
val[2, "acc >= n-f"] = False
print("Liveness, expect False: {}".format(checker.check(val)[0]))

# If more than t processes can fail, then safety does not hold
lta.set_parameter_constraint(fm.And( fm.lt(3*t, n), fm.le(f, t+1)))
checker = Checker(lta, 0, 3)
val = checker.get_empty_val()
val[0, "v1 > 0"] = False
val[2, "acc > 0"] = True
print("Non Safety, expect True: {}".format(checker.check(val)[0]))