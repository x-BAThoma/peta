from BDD import Handler

h = Handler()
a = h.constant(False)
for i in range(10):
    h2.add_variable(i)
partial_val = {i: True for i in range(4, 10)}
print("expect None: {}".format(h.get_short_valuation(a, True, partial_val)))
print("expect empty: {}".format(h.get_short_valuation(a, False, partial_val)))

b = h.update(a, {2: True, 3: False}, True)
print("expect (2: True, 3: False): {}".format(h.get_short_valuation(b, True, partial_val)))
print("expect (2: False): {}".format(h.get_short_valuation(b, False, partial_val)))

b = h.update(a, {2: True, 3: False, 5: True}, True)
print("expect (2: True, 3: False): {}".format(h.get_short_valuation(b, True, partial_val)))
print("expect (2: False): {}".format(h.get_short_valuation(b, False, partial_val)))
