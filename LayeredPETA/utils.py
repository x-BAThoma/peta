import collections

class AssociativeList:
    """
    Associate an integer index to a collection of hashable objects,
    the indices are ascribed in the order the objects are added. Can compute
    both the index of an object (method get_index) and the object at a
    given index (method get_object)
    """
    def __init__(self):
        """ Initialises an empty associative list """
        self.objects = []
        self.indices = {}

    def __len__(self):
        return len(self.objects)

    def iter_indices(self):
        """ Iterate over all the indices defined so far.
        Same as range(len(assoc_list)) """
        return range(len(self.objects))

    def iter_objects(self):
        """ Iterate over all the objects defined so far in order"""
        return self.objects

    def items(self):
        """ Iterate over both the indices and the objects,
        same effect as enumerate(assoc_list.()iter_objects)"""
        return enumerate(self.objects)

    def get_index(self, obj):
        """ return the index of the input hashable object. Add to the list if necessary. """
        try:
            return self.indices[obj]
        except KeyError:
            index = len(self.objects)
            self.objects.append(obj)
            self.indices[obj] = index
            return index

    def get_object(self, index):
        """ Return the object at input index. """
        return self.objects[index]

class frozendict(collections.Mapping):
    """
        A hashable wrapper around dictionaries, inspired by the frozendict package,
        but without the necessity of string keys.
        Adds the copy_and_update method to generate more dictionaries.
    """
    def __init__(self, *args, **kwargs):
        self._dict = dict(*args, **kwargs)
        self._hash = None
    
    def __getitem__(self, key):
        return self._dict[key]

    def __contains__(self, key):
        return key in self._dict

    def copy(self):
        return self.__class__(self)

    def __iter__(self):
        return iter(self._dict)

    def __len__(self):
        return len(self._dict)

    def __repr__(self):
        return '<%s %r>' % (self.__class__.__name__, self._dict)

    def __hash__(self):
        if self._hash is None:
            h = 0
            for key, value in self._dict.items():
                h ^= hash((key, value))
            self._hash = h
        return self._hash
    
    def copy_and_update(self, other):
        """ Returns a copy of the dictionary updated with the value in other """
        result = self.copy()
        result._dict.update(other)
        return result
