import Formulas as fm
from LTA import CLTA
from Paths import Checker
from GuardAutomaton import GuardAutomaton

lta = CLTA()

# Parameters
n = lta.add_parameter("n")
t = lta.add_parameter("t")

# Parameter Constraint
lta.set_parameter_constraint(fm.lt(2*t, n))

# Layers and States
R = lta.append_layer(["d0", "r0", "r1", "d1"])
P = lta.append_layer(["d0", "p0", "p?", "p1", "d1"])
Rand = lta.append_layer(["d0", "r0", "r?", "r1", "d1"])

# Guards layer R
for src in [R["r0"], R["r1"]]:
    lta.set_guard(src, P["p0"], fm.gt(2*(R["r0"] + R["d0"]), n))
    lta.set_guard(
        src, P["p?"],
        fm.And(
            fm.ge(R["r0"] + R["d0"] + R["r1"] + R["d1"], n - t),
            fm.ge(2*(R["r0"] + R["d0"]), n - 2*t),
            fm.ge(2*(R["r1"] + R["d1"]), n - 2*t)
        )
    )
lta.set_guard(R["d0"], P["d0"], fm.true)
lta.set_guard(R["d1"], P["d1"], fm.true)


# Guards layer P

for src in [P["p0"], P["p?"], P["p1"]]:
    lta.set_guard(src, Rand["d0"], fm.gt(P["p0"] + P["d0"], t))
    lta.set_guard(
        src, Rand["r0"],
        fm.And(
            fm.ge(P["p?"] + P["p0"] + P["d0"], n - t),
            fm.gt(P["p0"], 0),
            fm.ge(P["p?"], n - 2*t)
        )
    )
    lta.set_guard(src, Rand["r?"], fm.ge(P["p?"], n - t))
    lta.set_guard(
        src, Rand["r1"],
        fm.And(
            fm.ge(P["p?"] + P["p1"] + P["d1"], n - t),
            fm.gt(P["p1"], 0),
            fm.ge(P["p?"], n - 2*t)
        )
    )
lta.set_guard(P["d0"], Rand["d0"], fm.true)
lta.set_guard(P["d1"], Rand["d1"], fm.true)

# Guards layer Rand
lta.set_guard(Rand["d0"], R["d0"], fm.true)
lta.set_guard(Rand["r0"], R["r0"], fm.true)
lta.set_guard(Rand["r?"], R["r0"], fm.true)
lta.set_guard(Rand["r?"], R["r1"], fm.true)
lta.set_guard(Rand["r1"], R["r1"], fm.true)
lta.set_guard(Rand["d1"], R["d1"], fm.true)

# layer_constraint
lta.set_layer_constraint(0, fm.le(sum(R.values()), n))
lta.set_layer_constraint(1, fm.le(sum(P.values()), n))

# Propositions Layer R
lta.add_proposition(0, "d0 > 0", fm.gt(R["d0"], 0))
lta.add_proposition(0, "r0 > 0", fm.gt(R["r0"], 0))
lta.add_proposition(0, "r1 > 0", fm.gt(R["r1"], 0))
lta.add_proposition(0, "d1 > 0", fm.gt(R["d1"], 0))
lta.add_proposition(0, "r0 + d0 + r1 + d1 >= n - t", fm.ge(R["r0"] + R["d0"] + R["r1"] + R["d1"], n - t))
lta.add_proposition(0, "2 (r0 + d0) > n", fm.gt(2*(R["r0"] + R["d0"]), n))
lta.add_proposition(0, "2 (r1 + d1) > n", fm.gt(2*(R["r1"] + R["d1"]), n))
lta.add_proposition(
    0, "guard p?",
    fm.And(
        fm.ge(2*(R["r0"] + R["d0"]), n - 2*t),
        fm.ge(2*(R["r1"] + R["d1"]), n - 2*t)
    )
)
# Propositions Layer P
lta.add_proposition(1, "P d0 > 0", fm.gt(P["d0"], 0))
lta.add_proposition(1, "p0 > 0", fm.gt(P["p0"], 0))
lta.add_proposition(1, "p? > 0", fm.gt(P["p?"], 0))
lta.add_proposition(1, "p1 > 0", fm.gt(P["p1"], 0))
lta.add_proposition(1, "P d1 > 0", fm.gt(P["d1"], 0))
lta.add_proposition(
    1, "d0 + p0 + p? + p1 + d1 >= n - t",
    fm.ge(P["d0"] + P["p0"] + P["p?"] + P["p1"] + P["d1"], n - t)
)




# Bounded checking 1 round
checker = Checker(lta, 0, 4)
val = checker.get_empty_val()
val[0, "d0 > 0"] = False
val[0, "d1 > 0"] = False
val[3, "d0 > 0"] = True
val[3, "d1 > 0"] = True
print("Bounded safety, expect false: {}".format(checker.check(val)[0]))

# Test GA
ga = GuardAutomaton(lta)

def get_successors(state):
    

constraints = {"layer full": True}
print("computing initial states...")
stack = list(ga.get_initial_states(constraints))
states = set()
while stack:
    state = stack.pop()
    if state not in states:
        states.add(state)
        successors = ga.get_successors(state, constraints)
        stack += list(successors)
