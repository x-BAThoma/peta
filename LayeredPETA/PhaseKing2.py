import Formulas as fm
from LTA import CLTA
from Paths import Checker
from GuardAutomaton import GuardAutomaton

lta = CLTA()

# Parameters
n = lta.add_parameter("n")
t = lta.add_parameter("t")
f = lta.add_parameter("f")

# Parameter Constraint
lta.set_parameter_constraint(fm.And(
    fm.lt(4*t, n),
    fm.le(f, t),
    fm.gt(t, 0),
    fm.eq(t, f) # TEST
))

# States and layer
layer0 = lta.append_layer(["v0", "v1"])
v0 = layer0["v0"]
v1 = layer0["v1"]

layer1 = lta.append_layer(["k0", "p0", "k1", "p1"])
k0 = layer1["k0"]
p0 = layer1["p0"]
k1 = layer1["k1"]
p1 = layer1["p1"]

# Guards
for src in [v0, v1]:
    for dest in [p0, k0]:
        lta.set_guard(
            src, dest,
            fm.And(
                fm.eq(v0 + v1 + f, n), # Inforces Synchronicity
                fm.ge(2*v0 + f, n)
            )
        )
    for dest in [p1, k1]:
        lta.set_guard(
            src, dest,
            fm.And(
                fm.eq(v0 + v1 + f, n), # Inforces Synchronicity
                fm.lt(2*v0, n)
            )
        )

for src in [k0, p0, k1, p1]:
    majority0 = fm.gt(2*(p0 + k0 + f), n + 2*t)
    king0 = fm.And(
        fm.le(2*(p0 + k0), n + 2*t),
        fm.le(2*(p1 + k1), n + 2*t),
        fm.eq(k1, 0)
    )
    lta.set_guard(
        src, v0,
        fm.And(
            fm.eq(p0 + k0 + p1 + k1 + f, n), # Inforces Synchronicity
            fm.Or(
                majority0,
                king0
            )
        )
    )
    majority1 = fm.gt(2*(p1 + k1 + f), n + 2*t)
    king1 = fm.And(
        fm.le(2*(p0 + k0), n + 2*t),
        fm.le(2*(p1 + k1), n + 2*t),
        fm.eq(k0, 0)
    )
    lta.set_guard(
        src, v1,
        fm.And(
            fm.eq(p0 + k0 + p1 + k1 + f, n), # Inforces Synchronicity
            fm.Or(
                majority1,
                king1
            )
        )
    )

# layer_constraint
lta.set_layer_constraint(
    0, fm.le(v0 + v1 + f, n)
)
lta.set_layer_constraint(
    1,
    fm.And(
        fm.le(k0 + k1, 1), # At most one king
        fm.le(k0 + p0 + p1 + k1 + f, n)
    )
)

# Propositions
lta.add_proposition(0, "layer0 full", fm.ge(v0 + v1 + f, n))
lta.add_proposition(0, "v0 > 0", fm.gt(v0, 0))
lta.add_proposition(0, "v1 > 0", fm.gt(v1, 0))
lta.add_proposition(0, "2v0 + 2f > n", fm.gt(2*v0 + 2*f, n))
lta.add_proposition(0, "2v0 < n", fm.lt(2*v0, n))

lta.add_proposition(1, "k0 > 0", fm.gt(k0, 0))
lta.add_proposition(1, "k1 > 0", fm.gt(k1, 0))
lta.add_proposition(1, "p0 > 0", fm.gt(p0, 0))
lta.add_proposition(1, "p1 > 0", fm.gt(p1, 0))
lta.add_proposition(1, "layer1 full", fm.ge(k0 + p0 + p1 + k1 + f, n))
lta.add_proposition(1, "majority 0", fm.gt(2*(k0 + p0 + f), n + 2*t))
lta.add_proposition(1, "majority 1", fm.gt(2*(k1 + p1 + f), n + 2*t))
lta.add_proposition(1, "strong majority 0", fm.gt(2*(k0 + p0), n + 2*t))
lta.add_proposition(1, "strong majority 1", fm.gt(2*(k1 + p1), n + 2*t))

ga = GuardAutomaton(lta)
ini = ga.initial_state
constraints0 = {"layer0 full": True}
constraints1 = {"layer1 full": True}

print("computing initial states...")
initials0 = ga.right_extentions(ini, constraints0)
initials1 = set()
for s in initials0:
    initials1.update(ga.right_extentions(s, constraints1))

def successors(state):
    popped = ga.pop_left(state)
    middle = ga.right_extentions(popped, constraints0)
    middle2 = {ga.pop_left(s) for s in middle}
    res = set()
    for s in middle2:
        res.update(ga.right_extentions(s, constraints1))
    return res

print("computing state list...")

stack = list(initials1.copy())
visited_states = set()
while stack:
    s = stack.pop()
    if s not in visited_states:
        visited_states.add(s)
        succ = successors(s)
        stack += list(succ)

state_list = list(visited_states)
print(ga.print_val_states(1, state_list))

filtered = [
    s for s in visited_states if
    s.valuation[(1, "k0 > 0")] or s.valuation[(1, "k1 > 0")]
]

# filtered_succs = set()
# for s in filtered:
#     filtered_succs.update(successors(s))

# print("Computing Live States")

# print(ga.print_val_states(1, list(filtered_succs)))

result = ""
max_len_state = len(str(len(state_list) - 1))
for i, state in enumerate(state_list):
    succ = successors(state)
    index_succ = [state_list.index(s) for s in succ]
    index_succ.sort()
    padded_succ = [" " * (max_len_state - len(str(s))) + str(s) for s in index_succ]
    result += "Successors({}){}: ".format(str(i), " " * (max_len_state - len(str(i))))
    result += ", ".join(padded_succ)
    result += "\n"
print(result)
