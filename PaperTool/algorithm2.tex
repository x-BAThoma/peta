\section{LTL Model Checking Algorithm}

Properties on configurations can be expressed as \emph{linear temporal
  logic} (LTL) formulas. The propositions that appear in these
formulas are linear arithmetic formula over the variables of the
corresponding layer (state, edges or parameters). For example, the
formula $\G \left(k_0 + k_1 \leq 1\right)$ on the LTA of
Algorithm~\ref{alg: phase king} imposes that there is always at most
one king per round, while the formula
$\F \left(k_0 + k_1 \geq 1 \right)$ imposes that at some point a
correct process is king.

The problem our tool solves consists then in checking wether
\emph{every} coherent configuration of an LTA verify an input LTL
formula $\varphi$. The LTAs our tool is able to handle are cyclic,
meaning that they have a \emph{period} $\per > 0$ such that
$\LTAS_{\lay} = \LTAS_{\lay + \per}$ (lasso shaped LTAs where this
property only hold after some point are also allowed).

Our tool uses \emph{predicate abstraction} in order to generate a
\emph{finite automaton} that over approximates the set of coherent
configurations of the input LTA. `Traditional' model checking methods
can then be applied on this automaton in order to verify
$\varphi$. The following explains the construction of this automaton.

We fix for each $\lay \in \N$ a finite set of \emph{predicates}
$\LTAG_{\lay}$ which are linear arithmetic formulas with variables in
layer $\lay$ (states, edges or parameters).  We suppose that the
sequence of sets $\LTAG_{\lay}$ repeats with the same period as the
LTA, and that together, they contain at least the propositions of
$\varphi$. The predicates in $\LTAG_{\lay}$ can be evaluated against a
configuration by replacing each variable with its corresponding value
at layer $\lay$. Therefore, to every (concrete) configuration is
associated an (abstract) infinite word
$\LTAgamma_0 \LTAgamma_1 \dotso \LTAgamma_{\lay} \dotso$ where each
$\LTAgamma_{\lay}: \LTAG_{\lay} \to \{\false, \true\}$ is a valuation
of the corresponding predicates.

The set of all such words can be over approximated by the language of
an automaton built as follow: its states consists of the possible
valuations of the predicates (the same as its alphabet) and there is
an edge between two states
$\LTAgamma \in \{\false, \true\}^{\LTAG_{\lay}}$ and
$\LTAgamma' \in \{\false, \true\}^{\LTAG_{\lay + 1}}$ if it is
possible to \emph{concretize} the transition in a configuration. This
test can easily be done with an SMT solver.

If the language of the resulting automaton verify $\varphi$, then it
is guaranteed that every configuration of the LTA (and therefore any
execution of the original algorithm) is correct as well. Otherwise, a
callo-shaped abstract counter-example
$\LTAgamma_0 \dotso \LTAgamma_{i-1}\left(\LTAgamma_i \dotso
  \LTAgamma_{n-1}\right)^\omega$ can be found. This abstract
counter-example may however not corresponds to any \emph{concrete}
configuration. This notably happens when few predicates were initially
provided. It is easy enough to write an SMT querry that checks if such
a path is concretisable. In its current version, our tool handles the
different cases as follow:
\begin{itemize}
\item if the abstract path can be concretised, then the property does
  not hold and the concrete counter-example is returned.
\item if the \emph{finite} path
  $\LTAgamma_0 \dotso \LTAgamma_{i-1} \LTAgamma_i \dotso
  \LTAgamma_{n-1} \LTAgamma_i$ (without conditions imposing the
  concrete equality between layer $i$ and $n$) cannot be concretised,
  then we use \emph{Kraig's interpolants}\todo{cite} to compute
  additional predicates $\LTAg_0 \dotso \LTAg_n$ that are added (after
  filtering the trivial ones) to the corresponding $\LTAG_{\lay}$. The
  procedure then restart with these new predicates.
\item if this finite path can be concretised but not the loop, then
  our tool fails in its current version. Techniques such as those used
  in \todo{cite Ic3IA} may be applied here, but we have yet to
  implement them.
\end{itemize}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
