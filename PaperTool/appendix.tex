\section{Tool Demonstration}
In this section, we detail how the tool will be demonstrated.  We will
start by giving an example of a simple distributed algorithm, called
the Flood-Min Algorithm, that can be modeled and verified with PyLTA.
This algorithm is simpler than Phase King described in the core of the
paper; although it does not illustrate all the capabilities of the
tool, it is more convenient and easier to understand for an oral
presentation.

We will thus focus on the modeling aspects, and the usage of the tool
by showing a property that is easy to prove, and another property that
requires some interaction, thus showing the strengths but also the
limitations of the tool.

The Flood-Min algorithm is from~\cite{ChaudhuriHLT00} and given in
Algorithm~\ref{alg: flood min}.

\begin{algorithm}[h]
  \small
  \DontPrintSemicolon{}
  \SetKwProg{Fn}{Process}{:}{}
  \Fn{$\mathrm{FloodMin}(n, t, v)$}{
    \KwData{
      $n$ processes,
      $t$ allowed crashes,
      $v \in \{0, 1\}$.
    }
    \For(\tcp*[f]{$t+1$ iterations}){$i = 0$ to $t$}{
      broadcast $v$\;
      receive values $u_0, \dotsc, u_{l-1}$ from all\;
      $v \leftarrow \min \{ u_0, \dotsc, u_{l-1}\}$\;
    }
    return $v$;
  }
  \caption{The Flood-Min algorithm~\cite{ChaudhuriHLT00}, is a
    synchronous algorithm that solves consensus under at most $t$
    crashes.}\label{alg: flood min}
\end{algorithm}

We will directly explain configurations and the semantics of threshold automata 
without excessively formalizing these.
%An layered threshold automaton is a way of defining the set of
%\emph{configurations} of an algorithm. 
A configuration can be thought
of as some records of events that happened up to a point of an
execution. A configuration of Algorithm~\ref{alg: flood min} is
represented on Figure~\ref{fig: config flood min}.

\begin{figure}[h]
  \centering
  %\resizebox{\textwidth}{!}{
    \begin{tikzpicture}[xscale = 2.5,yscale=0.5, every node/.style={}]
      \node (val) at (1.5, -3.5) {Parameter values: $n : 5, t : 2$};
      \node (v0) at (0, 3) {$\lay=0$};
      \node (l1) at (1, 3) {$\lay=1$};
      \node (l2) at (2, 3) {$\lay=2$};
      \node (l3) at (3, 3) {$\lay=3$};
      \node[gray] (ldots) at (3.5, 3) {$\cdots$};
      \node[gray] (dots0) at (3.5, 1.5) {$\cdots$};
      \node[gray] (dots1) at (3.5, -1.5) {$\cdots$};

      \node[draw, gray] (s00) at (0, 1) {$v_0: 0$};
      \node[draw] (c00) at (0, 2) {$c_0: 1$};
      \node[draw] (s01) at (0, -1) {$v_1: 4$};
      \node[draw, gray] (c01) at (0, -2) {$c_1: 0$};

      \node[draw, gray] (s10) at (1, 1) {$v_0: 0$};
      \node[draw] (c10) at (1, 2) {$c_0: 1$};
      \node[draw] (s11) at (1, -1) {$v_1: 3$};
      \node[draw, gray] (c11) at (1, -2) {$c_1: 0$};

      \node[draw] (s20) at (2, 1) {$v_0: 1$};
      \node[draw, gray] (c20) at (2, 2) {$c_0: 0$};
      \node[draw] (s21) at (2, -1) {$v_1: 2$};
      \node[draw, gray] (c21) at (2, -2) {$c_1: 0$};

      \node[draw] (s30) at (3, 1) {$v_0: 3$};
      \node[draw, gray] (c30) at (3, 2) {$c_0: 0$};
      \node[draw, gray] (s31) at (3, -1) {$v_1: 0$};
      \node[draw, gray] (c31) at (3, -2) {$c_1: 0$};
      
      \draw (s01.east) edge[-latex'] (c10.west);
      \draw (s01.east) edge[-latex'] node[below] {$\times 3$} (s11.west);

      \draw (s11.east) edge[-latex'] (s20.west);
      \draw (s11.east) edge[-latex'] node[below] {$\times 2$} (s21.west);

      \draw (s20.east) edge[-latex'] (s30.west);
      \draw (s21.east) edge[-latex'] node[below] {$\times 2$} (s30.west);
      
    \end{tikzpicture}%
    \caption{A configuration of the Flood-Min algorithm
      (Algorithm~\ref{alg: flood min}).}
  \label{fig: config flood min}
\end{figure}

Figure~\ref{fig: config flood min} specifies the valuation of
parameters $n$ and $t$. Additionally, each column contains the
possible states of a process at each iteration of the loop. Such a
column is called a \emph{layer}. The shown configuration contains 
the number of processes that are at each state of each layer.
%It assigns to each state of each
%layer the corresponding number of processes.
For example, state $v_1$
(resp. $v_0$) in layer $\lay = 0$ has value $4$ (resp. $0$) meaning
that 4 (resp. $0$) correct processes had $v=1$ (resp. $v=0$) at the
first iteration of the loop.

The states $c_0$ (resp. $c_1$) represent processes that had values
$0$ (resp. $1$) but have crashed during the execution of the round. It
is important to represent them separately since these processes may
not have sent their values to all other processes.

Indeed, in the next layer, we see that a single process updates its
value to $0$ (by going to state $c_0$), meaning that it received a
value $0$ while the others only received $1$'s. This illustrates the
correction argument of the algorithm that depends on the existence of
a round where no process crashes (guaranteed by executing $t + 1$
rounds).

Next, we will show how this algorithm is described in PyLTA's input
format.  The parameters and states of these configurations are defined
as follows:
\begin{small}
\begin{verbatim}
PARAMETERS: n, t
LAYERS: L, L
STATES: L.c0, L.v0, L.v1, L.c1
\end{verbatim}
\end{small}

The first line defines the two parameters \verb|n| and \verb|t|. 
% The
% second line is slightly less straightforward, for some algorithm, not
% all layers are the same (they often alternate with period 2), and this
% line say 
%that 
The second line says that there is only one layer \verb|L|, and that
each layer \verb|L| is followed by another layer \verb|L|.
The last line defines the four states that appear in layers \verb|L|.

Next, we show how to specify the transitions between the defined states.
In most cases, this depends on the number of processes in
other states \emph{of the same layer}. For example, a process moves
from $v_1$ to $v_0$ if it receives a message $0$ in the current
round. This can only happen if another process is in state $v_0$ or
$c_0$. This condition is encoded with the linear arithmetic expression
$v_0 + c_0 > 0$ where $v_0$ and $c_0$ are interpreted as the
\emph{number} of processes in the corresponding state. Conversely, a
process moves from $v_1$ to $v_1$ (of the next layer) if it did not
receive any message $0$. This can be encoded with the condition
$v_0 = 0$, we omit $c_0$ in this case because \emph{it is possible}
for the process to not receive a message from a crashing process.

In PyLTA, this results in the following code:

\begin{small}
\begin{verbatim}
CASE L.v0:
  IF TRUE THEN L.v0
  IF TRUE THEN L.c0

CASE L.v1:
  IF L.v0 + L.c0 > 0 THEN L.v0
  IF L.v0 + L.c0 > 0 THEN L.c0
  IF L.v0 == 0 THEN L.v1
  IF L.v0 == 0 THEN L.c1
\end{verbatim}
\end{small}

We do not need to give the successors of $c_0$ and $c_1$, processes in
these states will simply not take part in subsequent layers.

Last, we show how to define properties to be verified by PyLTA.
First, we
will show how to prove the \emph{validity} property: `If no process has
$v=0$, then no process will ever have $v=0$'. In order to encode such
a property, we need a predicate that states that a process has $v=0$,
this can be done with $\mathrm{one}_0: v_0 + c_0 > 0$. Then the
property can be stated in linear temporal logic as
$\neg \mathrm{one}_0 \implies \G \neg \mathrm{one}_0$. In PyLTA,
this is written as follow:

\begin{small}
\begin{verbatim}
WITH
  L.one0: L.v0 + L.c0 > 0
VERIFY: ! L.one0 -> G ! L.one0
\end{verbatim}
\end{small}
By this example, we illustrate how predicates (such as {\tt L.one0})
can be defined, as used in temporal logic properties.

On this example, PyLTA immediately outputs:

\begin{small}
\begin{verbatim}
Formula is Valid
\end{verbatim}
\end{small}

Now, let us try to prove the termination of this algorithm. This can
only hold under a fairness property. In this case, an appropriate
fairness property can be that at every layer there are at least $n-t$
non-crashing processes, $\mathrm{fair}: v_0 + v_1 >= n - t$. Under
this assumption, a termination condition can be
$\mathrm{decided}: v_0 + c_0 = 0 \text{ or } v_1 + c_1 = 0$. We will
also add the initial condition
$\mathrm{ini}: v_0 + c_0 + v_1 + c_1 = n$. Then, we can attempt to
give PyLTA the following input:

\begin{small}
\begin{verbatim}
WITH
  L.ini: L.v0 + L.c0 + L.v1 + L.c1 == n
  L.fair: L.v0 + L.v1 >= n - t
  L.decided: L.v0 + L.c0 == 0 | L.v1 + L.c1 == 0
VERIFY: L.ini & G L.fair -> F L.decided
\end{verbatim}
\end{small}

We now run PyLTA on this model, but it fails:

\begin{verbatim}
Cannot verify formula
\end{verbatim}

In order to understand what happened, we run PyLTA again with the verbose flag:

\begin{small}
  \begin{verbatim}
    python -m pylta -v ....
  \end{verbatim}
\end{small}
%Sadly, PyLTA responds with the following (adding a \verb|-v| flag):
We obtain the following output:
\begin{small}
\begin{verbatim}
Found an abstract counter example:
L:       0 1
ini:     T F
fair:    T T
decided: F F

Loop: 1
Prefix concretisation succeeded but loop failed
Cannot verify formula
\end{verbatim}
\end{small}

We see that PyLTA attempted to instantiate an abstract path (i.e. a
Boolean valuation of the predicates). It managed to instantiate a
finite prefix of the path, but did not manage to concretise the
loop. In other terms, it was not able to determine whether for some
valuation of the parameters, there exists an \emph{infinite} execution
matching the abstract counterexample lasso.

In this case, \emph{every finite prefix} of the path is concretisable,
meaning that with predicate abstraction alone, PyLTA will not be able
to solve this problem.  This shows a limitation of the tool which
attempts to solve an undecidable problem.  Future directions to handle
these cases are discussed in the paper and will be shortly mentioned
in the demonstration.

We will then show how to bypass this issue in this case. In fact, the
reason why the algorithm is iterated $t+1$ time is to provide one
\emph{clean} round, meaning a round where no process crashes giving
the predicate $\mathrm{clean}: c_0 + c_1 = 0$. Assuming the existence
of such a round gives us the following:
\begin{small}
\begin{verbatim}
WITH
  L.ini: L.v0 + L.c0 + L.v1 + l.c1 == n
  L.fair: L.v0 + L.v1 >= n - t
  L.clean: L.c0 + L.c1 == 0
  L.decided: L.v0 + L.c0 == 0 | L.v1 + L.c1 == 0
VERIFY: L.ini & G L.fair & F L.clean -> F L.decided
\end{verbatim}
\end{small}

Under this assumption, PyLTA immediately manages to verify the property.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
