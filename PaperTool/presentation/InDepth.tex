
\documentclass[10pt, usenames, dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{xcolor} % Pretty colors
\usepackage{tikz}
\usetikzlibrary{arrows,calc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[noend]{algorithm2e}
\usepackage{booktabs}
\usepackage{verbatimbox}
\usepackage{MyMaccros}

\newcommand\Tr{{\color{ForestGreen}T}}
\newcommand\Fa{{\color{Red}F}}

% These lines replace `else' and `do' in algorithms with `:' (shorter)
\SetKwIF{If}{ElseIf}{Else}{if}{:}{else if}{else}{endif}
\SetKwFor{For}{for}{:}{endfor}
\SetKwFor{While}{while}{:}{endw}
\DontPrintSemicolon{}

% A light gray color for background
\definecolor{NotTooWhite}{RGB}{220,220,220}

% Defining styles
\setbeamertemplate{blocks}[rounded][shadow=false]
\setbeamercolor{structure}{fg=Brown}
\setbeamercolor{background canvas}{bg=NotTooWhite}
\setbeamertemplate{footline}{\hspace*{\fill}\insertframenumber/\inserttotalframenumber\hspace*{0.3cm}}
\setbeamerfont{footline}{series=\bfseries,size=\fontsize{8}{12}\selectfont}
\setbeamertemplate{navigation symbols}{}

% For not numbering backup slides
\newcommand{\beginbackup}{
   \newcounter{framenumbervorappendix}
   \setcounter{framenumbervorappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumbervorappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumbervorappendix}} 
}

% fix spacing when writing math equation inside a block
\newcommand{\blockmathspacefix}{
  \vspace*{-\baselineskip}\setlength\belowdisplayshortskip{0pt}
}

\AtBeginSection[]
{
\begin{frame}<beamer>{Table of Contents}
\tableofcontents[currentsection,
    sectionstyle=show/shaded
]
\end{frame}
}

% Defining Tikz styles
% \tikzset{mynode/.style = []}
% \tikzset{metanode/.style = {fill, Brown!15, rounded corners}}
\tikzset{legendnode/.style = {Brown, thick, font=\bfseries}}
\tikzset{separation/.style = {NotTooWhite}}
% \tikzset{DTSstate/.style = {}}
% \tikzset{DTSedge/.style = {->, thick}}
% \tikzset{DTSlabel/.style = {font=\scriptsize}}
\tikzset{GAstate/.style = {Blue, circle, fill = Blue!20, minimum size = 0.8cm}}
\tikzset{GAedge/.style = {->, thick, Red}}
% \tikzset{posetnode/.style = {fill, black, circle}}
% \tikzset{posetedge/.style = {black}}

\title{In-Depth presentation of PyLTA}
\author{Bastien Thomas, Nathalie Bertrand, Ocan Sankur}
\institute{Univ Rennes, Inria, CNRS, IRISA, France}
\date{}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}

\section{A More Advanced Example: Ben-Or's Algorithm}

\begin{frame}{Ben-Or's Byzantine Asynchronous Consensus Algorithm\footnotemark[1]}
  \begin{block}{}
    \begin{algorithm}[H]
      \small
      \DontPrintSemicolon{}
      \SetKwProg{Fn}{process}{:}{}
      \Fn{$\mathrm{BenOr}(n, t, v)$}{
        $i \leftarrow 0$\;
        \While{not decided}{
          {\color{Blue}
          broadcast $(i, v)$\;
          wait for $n - t$ messages $(i, \_)$\;
          \lIf{received $\frac{n + t}{2}$ $(i, w)$}{$v \leftarrow w$}
          \lElse{$v \leftarrow ?$}
          $i \plusplus$\;}
        {\color{ForestGreen}
          broadcast $(i, v)$\;
          wait for $n - t$ messages $(i, \_)$\;
          \lIf{received $\frac{n + t}{2}$ messages $(i, w)$}{decide $w$}
          \lElseIf{received $t + 1$ messages $(i, w)$}{$v \leftarrow w$}
          \lElse{$v \leftarrow \mathrm{cointoss(0, 1)}$}
          $i \plusplus$\;}
        }
      }
    \end{algorithm}
  \end{block}
  \begin{itemize}
  \item The {\color{Blue} blue} part corresponds to {\color{Blue} even layers}
  \item The {\color{ForestGreen} green} part corresponds to {\color{ForestGreen} odd layers}
  \end{itemize}
  \footnotetext[1]{M.Ben-Or: Another Advantage of Free Choice: Completely Asynchronous Agreement Protocols (Extended Abstract). PODC 1983.}
\end{frame}

\begin{frame}{Executions and Configurations of Ben-Or's Algorithm}
  \begin{block}{}
    \resizebox{\textwidth}{!}{
      \begin{tikzpicture}[
        xscale=2,
        yscale=0.6,
        evenstate/.style={draw, fill=Blue!15, thin},
        oddstate/.style={draw, fill=ForestGreen!15, thin},
        arrow/.style={draw, thick, Red, -latex'}
        ]

        \node (pvaluation) at (2.5, -4) {$n: 6$, $t: 1$, $f: 1$};
        \node (l0) at (0, 4) {\color{Blue}$i = 0$};
        \node (l1) at (1, 4) {\color{ForestGreen}$i = 1$};
        \node (l2) at (2, 4) {\color{Blue}$i = 2$};
        \node (l3) at (3, 4) {\color{ForestGreen}$i = 3$};
        \node (l4) at (4, 4) {\color{Blue}$i = 4$};
        \node (dots1) at (5, 2) {$\cdots$};
        \node (dots2) at (5, 0) {$\cdots$};
        \node (dots3) at (5, -2) {$\cdots$};

        \node[evenstate] (s0d0) at (0, 3) {$d_0: {\color{Gray}0}$};
        \node[evenstate] (s00) at (0, 1) {$a_0: {\color{Red}3}$};
        \node[evenstate] (s01) at (0, -1) {$a_1: {\color{Red}2}$};
        \node[evenstate] (s0d1) at (0, -3) {$d_1: {\color{Gray}0}$};

        \node[oddstate] (s10) at (1, 2) {$b_0: \alt<2->{\color{Red}1}{\color{Gray}0}$};
        \node[oddstate] (s1u) at (1, 0) {
          $b_?: \alt<3->{
            \alt<4->{
              \alt<6->{
                \color{Red}4
                }{
                  \color{Red}3
                }
            }{
              \color{Red}1
            }
          }{
            \color{Gray}0
          }$
        };
        \node[oddstate] (s11) at (1, -2) {$b_1: {\color{Gray}0}$};

        \node[evenstate] (s2d0) at (2, 3) {$d_0: {\color{Gray}0}$};
        \node[evenstate] (s20) at (2, 1) {
          $a_0: \alt<5->{
            \alt<6->{
              \color{Red}2
            }{
              \color{Red}1
            }
          }{
            \color{Gray}0
          }$
        };
        \node[evenstate] (s21) at (2, -1) {
          $a_1: \alt<6->{
            \color{Red}3
          }{
            \color{Gray}0
          }$
        };
        \node[evenstate] (s2d0) at (2, -3) {$d_1: {\color{Gray}0}$};

        \node[oddstate] (s30) at (3, 2) {$b_0: {\color{Gray}0}$};
        \node[oddstate] (s3u) at (3, 0) {
          $b_?: \alt<6->{
            \color{Red}1
          }{
            \color{Gray}0
          }$
        };
        \node[oddstate] (s31) at (3, -2) {
          $b_1: \alt<6->{
            \color{Red}3
          }{
            \color{Gray}0
          }$
        };

        \node[evenstate] (s4d0) at (4, 3) {$d_0: {\color{Gray}0}$};
        \node[evenstate] (s40) at (4, 1) {$a_0: {\color{Gray}0}$};
        \node[evenstate] (s41) at (4, -1) {$a_1: {\color{Gray}0}$};
        \node[evenstate] (s4d1) at (4, -3) {
          $d_1: \alt<6->{
            \color{Red}1
          }{
            \color{Gray}0
          }$
        };
        \onslide<2->{
          \path (s00.10) edge[arrow] (s10.190);
        }
        \onslide<3->{
          \alt<6->{
            \path (s01.10) edge[arrow] node[below] {$\times 2$} (s1u.190);
          }{
            \path (s01.10) edge[arrow] (s1u.190);
          }
        }
        \onslide<4->{
          \path (s00.350) edge[arrow] node[above] {$\times 2$} (s1u.170);
        }
        \onslide<5->{
          \path (s1u.10) edge[arrow] (s20.190);
        }
        \onslide<6->{
          \path (s10.350) edge[arrow] (s20.170);
          \path (s1u.350) edge[arrow] node[above] {$\times 3$} (s21.170);
          \path (s21.350) edge[arrow] node[above] {$\times 3$} (s31.170);
          \path (s20.350) edge[arrow] (s3u.170);
          \path (s31.350) edge[arrow] (s4d1.170);
        }
        
      \end{tikzpicture}
    }
  \end{block}
  \onslide<7->{
    \begin{alertblock}{Warning:}
      For asynchronous algorithms, the layer indices do \emph{not} represent time!
    \end{alertblock}
  }
\end{frame}

\section{The SMT encoding}

\begin{frame}{Guards of an LTA}
  \begin{itemize}
  \item<1-> A process can move to $b_1$ if it has received $\frac{n+t}{2}$ messages $(i, 1)$.
  \item<2-> These either come from processes in $a_1$ or byzantine ones.
  \item<3-> Meaning $\LTAguard(\_, b_1)$ is $a_1 + f \geq \frac{n+t}{2}$
  \item<4-> A process moves to $b_?$ if it receives $n-t$ messages \emph{without} receiving $\frac{n+t}{2}$ messages $(i, 0)$ or $(i, 1)$
  \item<5-> All sent messages are not necessarily received, so this is written:
    \[
      \min\left(a_0, \left\lceil\frac{n+t}{2}\right\rceil - 1\right) + \min\left(a_1, \left\lceil\frac{n+t}{2}\right\rceil - 1\right) + f \geq n - t
    \]
  \end{itemize}
  \begin{block}<6->{Guards are Monotonous}
    Once a guard hold, it cannot become false anymore
  \end{block}
  \begin{block}<7->{Theorem}
    A configuration is reachable iff:
    \begin{itemize}
    \item It respects the \emph{flow conditions}
    \item The guards of taken edges are satisfied \emph{in the last configuration}
    \end{itemize}
  \end{block}
  \onslide<7->{
    \alert{We only consider the \emph{last} configuration of an execution}
  }
\end{frame}

\begin{frame}{SMT Encoding}
    \begin{block}{}
    \resizebox{\textwidth}{!}{
      \begin{tikzpicture}[
        xscale=2,
        yscale=0.6,
        evenstate/.style={draw, fill=Blue!15, thin},
        oddstate/.style={draw, fill=ForestGreen!15, thin},
        arrow/.style={draw, thick, Red, -latex'}
        ]

        \node (pvaluation) at (2.5, -4) {$n: 6$, $t: 1$, $f: 1$};
        \node (l0) at (0, 4) {\color{Blue}$i = 0$};
        \node (l1) at (1, 4) {\color{ForestGreen}$i = 1$};
        \node (l2) at (2, 4) {\color{Blue}$i = 2$};
        \node (l3) at (3, 4) {\color{ForestGreen}$i = 3$};
        \node (l4) at (4, 4) {\color{Blue}$i = 4$};
        \node (dots1) at (5, 2) {$\cdots$};
        \node (dots2) at (5, 0) {$\cdots$};
        \node (dots3) at (5, -2) {$\cdots$};

        \node[evenstate] (s0d0) at (0, 3) {$d_0: {\color{Gray}0}$};
        \node[evenstate] (s00) at (0, 1) {$a_0: {\color{Red}3}$};
        \node[evenstate] (s01) at (0, -1) {$a_1: {\color{Red}2}$};
        \node[evenstate] (s0d1) at (0, -3) {$d_1: {\color{Gray}0}$};

        \node[oddstate] (s10) at (1, 2) {$b_0: {\color{Red}1}$};
        \node[oddstate] (s1u) at (1, 0) {$b_?: {\color{Red}4}$};
        \node[oddstate] (s11) at (1, -2) {$b_1: {\color{Gray}0}$};

        \node[evenstate] (s2d0) at (2, 3) {$d_0: {\color{Gray}0}$};
        \node[evenstate] (s20) at (2, 1) {$a_0: {\color{Red}2}$};
        \node[evenstate] (s21) at (2, -1) {$a_1: {\color{Red}3}$};
        \node[evenstate] (s2d0) at (2, -3) {$d_1: {\color{Gray}0}$};

        \node[oddstate] (s30) at (3, 2) {$b_0: {\color{Gray}0}$};
        \node[oddstate] (s3u) at (3, 0) {$b_?: {\color{Red}1}$};
        \node[oddstate] (s31) at (3, -2) {$b_1: {\color{Red}3}$};

        \node[evenstate] (s4d0) at (4, 3) {$d_0: {\color{Gray}0}$};
        \node[evenstate] (s40) at (4, 1) {$a_0: {\color{Gray}0}$};
        \node[evenstate] (s41) at (4, -1) {$a_1: {\color{Gray}0}$};
        \node[evenstate] (s4d1) at (4, -3) {$d_1: {\color{Red}1}$};
        
        \path (s00.10) edge[arrow] (s10.190);
        \path (s01.10) edge[arrow] node[below] {$\times 2$} (s1u.190);
        \path (s00.350) edge[arrow] node[above] {$\times 2$} (s1u.170);
        \path (s1u.10) edge[arrow] (s20.190);
        \path (s10.350) edge[arrow] (s20.170);
        \path (s1u.350) edge[arrow] node[above] {$\times 3$} (s21.170);
        \path (s21.350) edge[arrow] node[above] {$\times 3$} (s31.170);
        \path (s20.350) edge[arrow] (s3u.170);
        \path (s31.350) edge[arrow] (s4d1.170);
      \end{tikzpicture}
    }
  \end{block}
  Any \emph{finite} portion of an LTA can be encoded in linear integer arithmetic.
  
  \pause
  \alert{But this is insufficient to verify complex properties.}
\end{frame}

\section{Predicate Abstraction}

\begin{frame}{Predicate Abstraction}
    \begin{block}{}
    \resizebox{\textwidth}{!}{
      \begin{tikzpicture}[
        xscale=2,
        yscale=0.6,
        evenstate/.style={draw, fill=Blue!15, thin},
        oddstate/.style={draw, fill=ForestGreen!15, thin},
        arrow/.style={draw, thick, Red, -latex'},
        abevenstate/.style={draw, fill=Blue!15, thin},
        aboddstate/.style={draw, fill=ForestGreen!15, thin},
        legendnode/.style={Brown, very thick}
        ]

        \newcommand\ttrue{{\color{ForestGreen}\ensuremath{T}}}
        \newcommand\tfalse{{\color{Red}\ensuremath{F}}}
        

        \node (pvaluation) at (0.5, -4) {$n: 6$, $t: 1$, $f: 1$};
        \node (l0) at (0, 4) {\color{Blue}$i = 0$};
        \node (l1) at (1, 4) {\color{ForestGreen}$i = 1$};

        \node[evenstate] (s0d0) at (0, 3) {$d_0: {\color{Gray}0}$};
        \node[evenstate] (s00) at (0, 1) {$a_0: {\color{Red}3}$};
        \node[evenstate] (s01) at (0, -1) {$a_1: {\color{Red}2}$};
        \node[evenstate] (s0d1) at (0, -3) {$d_1: {\color{Gray}0}$};

        \node[oddstate] (s10) at (1, 2) {$b_0: {\color{Red}1}$};
        \node[oddstate] (s1u) at (1, 0) {$b_?: {\color{Red}4}$};
        \node[oddstate] (s11) at (1, -2) {$b_1: {\color{Gray}0}$};
        
        \path (s00.10) edge[arrow] (s10.190);
        \path (s01.10) edge[arrow] node[below] {$\times 2$} (s1u.190);
        \path (s00.350) edge[arrow] node[above] {$\times 2$} (s1u.170);


        \node (abl0) at (2.5, 4) {\color{Blue}$i = 0$};
        \node (abl1) at (4, 4) {\color{ForestGreen}$i = 1$};
        
        \node[abevenstate] (ab0) at (2.5, 1) {
          \(
          \begin{aligned}
            a_0 + d_0 > 0 &: \ttrue \\
            a_1 + d_1 > 0 &: \ttrue \\
            a_0 + a_1 + f \leq n &: \ttrue \\
            d_0 + d_1 > 0 &: \tfalse \\
            \vdots \quad &
          \end{aligned}
          \)
        };
        \node[aboddstate] (ab1) at (4, 1) {
          \(
          \begin{aligned}
            b_0 > 0 &: \ttrue \\
            b_? > 0 &: \ttrue \\
            b_1 > 0 &: \tfalse \\
            b_? + f \geq n &: \tfalse \\
            \vdots \quad &
          \end{aligned}
          \)
        };

        \path (ab0.south) edge[out=300, in=240, arrow] node[below, thick, Red] {Concretisable Transition} (ab1.south);

        \draw[thick, Brown] (1.5, 5) -- (1.5, -4.4);
        \node[legendnode] at (0.5, 4.7) {Concrete Side};
        \node[legendnode] at (3.15, 4.7) {Abstract Side};
      \end{tikzpicture}
    }
  \end{block}
  \begin{block}{Abstract configuration}
    Sequence of predicate valuation concretisable 2 by 2.
  \end{block}
\end{frame}

\begin{frame}{Guard Automaton}
  \begin{block}{Guard Automaton}
    \centering
    \begin{tikzpicture}[
      xscale=0.5,
      yscale=0.6,
      prednode/.style={},
      valuenode/.style={},
      evenstate/.style={draw, fill=Blue, fill opacity=0.2, thin},
      oddstate/.style={draw, fill=ForestGreen, fill opacity=0.2, thin},
      evenarrow/.style={draw, thick, Blue, -latex'},
      oddarrow/.style={draw, thick, ForestGreen, -latex'}]

      \newcommand\ttrue{{\color{ForestGreen}\ensuremath{T}}}
      \newcommand\tfalse{{\color{Red}\ensuremath{F}}}


      % Predicates
      \node[prednode] at (0, 4) {$p_0$};
      \node[prednode] at (1, 4) {$p_1$};
      \node[prednode] at (2, 4) {$p_2$};
      \node[prednode] at (3, 4) {$p_3$};

      \node[prednode] at (6, 4) {$p'_0$};
      \node[prednode] at (7, 4) {$p'_1$};
      \node[prednode] at (8, 4) {$p'_2$};
      \node[prednode] at (9, 4) {$p'_3$};

      % Even states
      \node[valuenode] (s00) at (0, 3) {\ttrue};
      \node[valuenode] at (1, 3) {\tfalse};
      \node[valuenode] at (2, 3) {\ttrue};
      \node[valuenode] (s03) at (3, 3) {\ttrue};
      \path[evenstate] (s00.north west) rectangle (s03.south east);
      
      \node[valuenode] (s10) at (0, 2) {\ttrue};
      \node[valuenode] at (1, 2) {\tfalse};
      \node[valuenode] at (2, 2) {\ttrue};
      \node[valuenode] (s13) at (3, 2) {\tfalse};
      \path[evenstate] (s10.north west) rectangle (s13.south east);

      \node at (1.5, 1) {$\vdots$};

      \node[valuenode] (s20) at (0, 0) {\tfalse};
      \node[valuenode] at (1, 0) {\ttrue};
      \node[valuenode] at (2, 0) {\tfalse};
      \node[valuenode] (s23) at (3, 0) {\ttrue};
      \path[evenstate] (s20.north west) rectangle (s23.south east);

      % Odd states
      \node[valuenode] (o00) at (6, 3) {\ttrue};
      \node[valuenode] at (7, 3) {\ttrue};
      \node[valuenode] at (8, 3) {\ttrue};
      \node[valuenode] (o03) at (9, 3) {\ttrue};
      \path[oddstate] (o00.north west) rectangle (o03.south east);
      
      \node[valuenode] (o10) at (6, 2) {\ttrue};
      \node[valuenode] at (7, 2) {\ttrue};
      \node[valuenode] at (8, 2) {\tfalse};
      \node[valuenode] (o13) at (9, 2) {\tfalse};
      \path[oddstate] (o10.north west) rectangle (o13.south east);

      \node at (7.5, 1) {$\vdots$};

      \node[valuenode] (o20) at (6, 0) {\tfalse};
      \node[valuenode] at (7, 0) {\tfalse};
      \node[valuenode] at (8, 0) {\tfalse};
      \node[valuenode] (o23) at (9, 0) {\ttrue};
      \path[oddstate] (o20.north west) rectangle (o23.south east);

      % Even Arrows
      %\path (s03.east) edge[evenarrow] (o10.west);
      \path (s03.east) edge[evenarrow] (o20.west);
      \path (s13.east) edge[evenarrow] (o10.west);
      \path (s23.east) edge[evenarrow] (o00.west);

      % Odd Arrows
      \path (o00.west) edge[oddarrow] (s03.east);
      \path (o10.west) edge[oddarrow] (s03.east);
      \path (o10.west) edge[oddarrow] (s23.east);
      %\path (o20.west) edge[oddarrow] (s13.east);
      \path (o20.west) edge[oddarrow] (s23.east);

    \end{tikzpicture}
  \end{block}
  \pause
  \begin{block}{Theorem}
    \vspace*{-\baselineskip}\setlength\belowdisplayshortskip{0pt} % From stackoverflow
    \[
      \text{Reachable Configurations } \subseteq \text{ Language of the Guard Automaton}
    \]
  \end{block}
  \pause
  \begin{block}{Problems}
    \begin{itemize}
    \item Incomplete
    \item Depends on chosen predicates
    \end{itemize}
  \end{block}
\end{frame}

\section{Counter-Example Guided Abstraction Refinement}

\begin{frame}{CEGAR Principle}
  \begin{block}{CEGAR Loop}
    \begin{enumerate}
    \item<2-> Compute an abstraction of the system
    \item<3-> If the abstraction verifies the property {\color{ForestGreen} Valid}
    \item<4-> Otherwise get an abstract counter example
    \item<5-> If the counter example can be concretised then {\color{Red} Invalid}
    \item<6-> \alert{Otherwise use the counter example to refine the abstraction}
    \end{enumerate}
  \end{block}

  \onslide<7->{
    In our case, refining the abstraction means adding new predicates.
  }
\end{frame}


\begin{frame}{Interpolation}
  \begin{block}{Definition Craig's Interpolant}
    If ${\color{Red}A} \wedge {\color{Blue}B}$ is unsat then we can compute ${\color{Green}I}$ such that:
    \begin{itemize}
    \item ${\color{Red}A}$ implies ${\color{Green}I}$
    \item ${\color{Green}I} \wedge {\color{Blue}B}$ is unsat
    \item ${\color{Green}I}$ contains only variables common to ${\color{Red}A}$ and ${\color{Blue}B}$
    \end{itemize}      
  \end{block}
  \pause
  \begin{block}{Application: New Predicates from Unconcretisable Valuations}
    \centering
    \begin{tikzpicture}[
      xscale=1.7,
      yscale=0.4,
      evenstate/.style={draw, fill=Blue!15, thin, minimum height = 0.6cm, minimum width=0.6cm},
      oddstate/.style={draw, fill=ForestGreen!15, thin, minimum height = 0.6cm, minimum width=0.6cm},
      arrow/.style={draw, thick, Red, -latex'}
      ]
      
      \node (dots1) at (4.25, 2) {$\cdots$};
      \node (dots2) at (4.25, 0) {$\cdots$};
      \node (dots3) at (4.25, -2) {$\cdots$};
      
      \node[evenstate] (s0d0) at (0, 3) {$d_0$};
      \node[evenstate] (s00) at (0, 1) {$a_0$};
      \node[evenstate] (s01) at (0, -1) {$a_1$};
      \node[evenstate] (s0d1) at (0, -3) {$d_1$};
      
      \node[oddstate] (s10) at (1, 2) {$b_0$};
      \node[oddstate] (s1u) at (1, 0) {$b_?$};
      \node[oddstate] (s11) at (1, -2) {$b_1$};

      \node[evenstate] (s2d0) at (2, 3) {$d_0$};
      \node[evenstate] (s20) at (2, 1) {$a_0$};
      \node[evenstate] (s21) at (2, -1) {$a_1$};
      \node[evenstate] (s2d0) at (2, -3) {$d_1$};
      
      \node[oddstate] (s30) at (3, 2) {$b_0$};
      \node[oddstate] (s3u) at (3, 0) {$b_?$};
      \node[oddstate] (s31) at (3, -2) {$b_1$};

      \node[evenstate] (s4d0) at (5.5, 3) {$d_0$};
      \node[evenstate] (s40) at (5.5, 1) {$a_0$};
      \node[evenstate] (s41) at (5.5, -1) {$a_1$};
      \node[evenstate] (s4d1) at (5.5, -3) {$d_1$};

      \draw[thick, Red] (-0.2, -3.9) .. controls (1.5, -6) .. (3.2, -3.9);
      \node[Red] at (1.5, -4.5) {$A$};

      \draw[thick, Blue] (2.8, -3.9) .. controls (4.25, -6) .. (5.7, -3.9);
      \node[Blue] at (4.25, -4.5) {$B$};

      \node[Green] at (3, -5) {$I$};
    \end{tikzpicture}
  \end{block}
\end{frame}

\begin{frame}{High Level View of the Algorithm}
  \begin{block}{}
    \begin{algorithm}[H]
      \KwData{LTA $\LTAObj$, Temporal formula $\varphi$}
      $P \leftarrow$ predicates that appear in $\varphi$\;
      \While{$\true$}{
        let $\mathcal{A}$ be the guard automaton of $\LTAObj$ with predicates $P$\;
        \uIf{$\varphi$ is valid in $\mathcal{A}$}{
          \Return `Formula is Valid'\;
        }
        let $\nu_0 \dotso {\left(\nu_i \dotso \nu_{k-1}\right)}^\omega$ be an abstract counter example\;
        \uIf{$\nu_0 \dotso \nu_i \dotso \nu_{k-1} \nu_i$ is concretisable}{
          \uIf{we can additionaly impose layer $i$ equals layer $k$}{
            \Return `Formula has a counter example'
          }
          \uElse{
            \Return `Could not verify Formula'
          }
        }
        interpolate predicates $p_0 \dotso p_k$ from $\nu_0 \dotso \nu_i \dotso \nu_{k-1} \nu_i$\;
        add $p_0 \dotso p_k$ to $P$\;
      }
    \end{algorithm}
  \end{block}
\end{frame}

\section{How the Guard Automaton is Built}

\begin{frame}{Guard Automaton and Boolean Function}
  \begin{block}{Observations}
    Let $P_{i}$ and $P_{i+1}$ be the set of predicates of two successive layers.
    \begin{itemize}
    \item<2-> The states of the layer $i$ are a subset of the valuations of $P_i$, $S_i: \{0, 1\}^{P_i} \to \{0, 1\}$
    \item<3-> Same for $i+1$, $S_{i+1}: \{0, 1\}^{P_{i+1}} \to \{0, 1\}$
    \item<4-> The edges can be similarly encoded, $E_i: \{0, 1\}^{P_i \cup P_{i+1}} \to \{0, 1\}$
    \end{itemize}
    \onslide<5->{
      \alert{The guard automaton consists of \emph{boolean functions}!}
    }
  \end{block}
  \onslide<6->{
    \begin{block}{Binary Decision Diagrams (BDD)}
      \begin{itemize}
      \item (Relatively) Efficient way of representing boolean functions.
      \item Support \emph{a lot} of operations ($\neg$, $\vee$, $\wedge$, finding sat assignment\dots)
      \end{itemize}
    \end{block}
  }
  
\end{frame}

\begin{frame}{Two `Naive' Algorithms}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \centering
      \begin{tikzpicture}[xscale=0.65, yscale=1,
        pred/.style={},
        val/.style={},
        arrow/.style={draw, -latex'}
        ]
        \newcommand\ttrue{{\color{ForestGreen}\ensuremath{T}}}
        \newcommand\tfalse{{\color{Red}\ensuremath{F}}}
        \newcommand\tsat{{\color{ForestGreen}\ensuremath{\mathrm{sat}}}}
        \newcommand\tunsat{{\color{Red}\ensuremath{\mathrm{unsat}}}}
        \node[Brown] (p0) at (0.7, 2.7) {$p_0:$};
        \node[Brown] (p1) at (-0.5, 1.5) {$p_1:$};
        \node[Brown] (p2) at (-1, 0.5) {$p_2:$};
        
        \node[pred] (v) at (3.5, 3) {$\tsat$};
        \node[pred] (v0) at (1.5, 2) {$\tsat$};
        \node[pred] (v1) at (5.5, 2) {$\tsat$};
        \node[pred] (v00) at (0.5, 1) {$\tsat$};
        \node[pred] (v01) at (2.5, 1) {$\tunsat$};
        \node[pred] (v10) at (4.5, 1) {$\tsat$};
        \node[pred] (v11) at (6.5, 1) {$\tunsat$};
        \node[val] (v000) at (0, 0) {$\tunsat$};
        \node[val] (v001) at (1, 0) {$\tsat$};
        \node[val] (v100) at (4, 0) {$\tsat$};
        \node[val] (v101) at (5, 0) {$\tsat$};

        \path (v) edge[arrow] node[above left] {\tfalse} (v0);
        \path (v) edge[arrow] node[above right] {\ttrue} (v1);
        \path (v0) edge[arrow] node[left] {\tfalse} (v00);
        \path (v0) edge[arrow] node[right] {\ttrue} (v01);
        \path (v1) edge[arrow] node[left] {\tfalse} (v10);
        \path (v1) edge[arrow] node[right] {\ttrue} (v11);
        \path (v00) edge[arrow] node[left] {\tfalse} (v000);
        \path (v00) edge[arrow] node[right] {\ttrue} (v001);
        \path (v10) edge[arrow] node[left] {\tfalse} (v100);
        \path (v10) edge[arrow] node[right] {\ttrue} (v101);
      \end{tikzpicture}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{block}{Top Down Approach}
        \begin{itemize}
        \item Visit the tree of valuations
        \item Check satisfiability at each node
        \item If unsat remove the branch
        \end{itemize}
      \end{block}
      \begin{description}[pros:]
      \item<2->[pros:] easy to add predicates
      \item<3->[cons:] lots of SMT requests
      \end{description}
    \end{column}
  \end{columns}
  \onslide<4->{
    \begin{columns}
      \begin{column}{0.5\textwidth}
        \begin{block}{Bottom Up Approach}
          \begin{itemize}
          \item Extract valuation of predicates from a model
          \item Negate this valuation and add the new clause to the solver
          \item Repeat until unsat
          \end{itemize}
        \end{block}
        \begin{description}[pros:]
        \item<5->[pros:] efficient on sparse automata
        \item<6->[cons:] big SMT requests otherwise
        \end{description}
      \end{column}
      \begin{column}{0.5\textwidth}
        \newcommand\ttrue{{\color{ForestGreen}\ensuremath{T}}}
        \newcommand\tfalse{{\color{Red}\ensuremath{F}}}
        \begin{description}[constraint:]
        \item[model:] $p_0: \tfalse, p_1: \tfalse, p_2: \ttrue$
        \item[constraint:] $\neg (\neg p_0 \wedge \neg p_1 \wedge p_2)$
        \item[model:] $p_0: \ttrue, p_1: \tfalse, p_2: \tfalse$
        \item[constraint:] $\neg (p_0 \wedge \neg p_1 \wedge \neg p_2)$
        \item[model:] $p_0: \ttrue, p_1: \tfalse, p_2: \ttrue$
        \item[constraint:] $\neg (p_0 \wedge \neg p_1 \wedge p_2)$
        \item[model:] {\color{Red} unsat}
        \end{description}
      \end{column}
    \end{columns}
  }
\end{frame}

\begin{frame}{Combining the Two}
  \begin{block}{Data Structure}
    For each unique layer $i \in \N$ maintain two BDDs:
    \begin{itemize}
    \item $\mathrm{low}_i$ \emph{under approximates} the transitions.
    \item $\mathrm{high}_i$ \emph{over approximates} them
    \end{itemize}
    Initially, $(\mathrm{low}_i, \mathrm{high}_i) = (\false, \true)$
  \end{block}

  \pause
  \begin{block}{Main Loop}
    \begin{enumerate}
    \item Find a \emph{small} valuation that satisfies $\mathrm{high}_i \wedge \neg \mathrm{low}_i$
    \item Ask the SMT solver if it is sat
    \item If it is, add the full model to $\mathrm{low}_i$ as in \thick{\color{Brown} Bottom-Up}
    \item Otherwise remove the whole \emph{cube} from $\mathrm{high}_i$ as in \thick{\color{Brown} Top-Down}
    \item Repeat until $\mathrm{high}_i = \mathrm{low}_i$
    \end{enumerate}
  \end{block}
  \pause
  
  When adding a new predicate, $\mathrm{low}_i$ needs to be reset but
  $\mathrm{high}_i$ can remain.
  \pause
  
  Is integrated in a lazy object that computes the automaton
  dynamically.
\end{frame}

\section{Conclusion}
\begin{frame}{Conclusion}
  \begin{block}{What PyLTA can do:}
    \begin{itemize}
    \item Parameterised verification for \emph{every} number of processes at once
    \item Full LTL support (both safety and liveness)
    \item Both synchronous and asynchronous algorithms
    \end{itemize}
    This means PyLTA can verify in a few seconds a large variety of
    properties on different algorithms of the literature.
  \end{block}
  
  
  \begin{block}{Future Work}
    \begin{itemize}
    \item Lazy abstraction\footnotemark[1] to improve performances
    \item Ranking functions\footnotemark[2] to verify more properties
    \item Almost sure termination of randomised algorithms
    \end{itemize}
  \end{block}
\footnotetext[1]{S. Tonetta. Abstract Model Checking without Computing the Abstraction. FM 2009}
\footnotetext[2]{M.Heismann, J. Hoenicke, J.Leike, A. Podelski. Linear Ranking for Linear Lasso Programs. ATVA 2013}
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
