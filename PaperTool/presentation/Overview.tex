\documentclass[10pt, usenames, dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{xcolor} % Pretty colors
\usepackage{tikz}
\usetikzlibrary{arrows,calc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[noend]{algorithm2e}
\usepackage{booktabs}
\usepackage{verbatimbox}
\usepackage{MyMaccros}

\newcommand\Tr{{\color{ForestGreen}T}}
\newcommand\Fa{{\color{Red}F}}

% These lines replace `else' and `do' in algorithms with `:' (shorter)
\SetKwIF{If}{ElseIf}{Else}{if}{:}{else if}{else}{endif}
\SetKwFor{For}{for}{:}{endfor}
\SetKwFor{While}{while}{:}{endw}
\DontPrintSemicolon{}

% A light gray color for background
\definecolor{NotTooWhite}{RGB}{220,220,220}

% Defining styles
\setbeamertemplate{blocks}[rounded][shadow=false]
\setbeamercolor{structure}{fg=Brown}
\setbeamercolor{background canvas}{bg=NotTooWhite}
\setbeamertemplate{footline}{\hspace*{\fill}\insertframenumber/\inserttotalframenumber\hspace*{0.3cm}}
\setbeamerfont{footline}{series=\bfseries,size=\fontsize{8}{12}\selectfont}
\setbeamertemplate{navigation symbols}{}

% For not numbering backup slides
\newcommand{\beginbackup}{
   \newcounter{framenumbervorappendix}
   \setcounter{framenumbervorappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumbervorappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumbervorappendix}} 
}

% fix spacing when writing math equation inside a block
\newcommand{\blockmathspacefix}{
  \vspace*{-\baselineskip}\setlength\belowdisplayshortskip{0pt}
}

\AtBeginSection[]
{
\begin{frame}<beamer>{Table of Contents}
\tableofcontents[currentsection,
    sectionstyle=show/shaded
]
\end{frame}
}

% Defining Tikz styles
% \tikzset{mynode/.style = []}
% \tikzset{metanode/.style = {fill, Brown!15, rounded corners}}
\tikzset{legendnode/.style = {Brown, thick, font=\bfseries}}
\tikzset{separation/.style = {NotTooWhite}}
% \tikzset{DTSstate/.style = {}}
% \tikzset{DTSedge/.style = {->, thick}}
% \tikzset{DTSlabel/.style = {font=\scriptsize}}
\tikzset{GAstate/.style = {Blue, circle, fill = Blue!20, minimum size = 0.8cm}}
\tikzset{GAedge/.style = {->, thick, Red}}
% \tikzset{posetnode/.style = {fill, black, circle}}
% \tikzset{posetedge/.style = {black}}

\title{PyLTA: A Tool for Verifying Parameterised Distributed Algorithms.}
\author{Bastien Thomas, Nathalie Bertrand, Ocan Sankur}
\institute{Univ Rennes, Inria, CNRS, IRISA, France}
\date{}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}

\section{Presentation of the Models}

\begin{frame}{Example: Flood Min Consensus Algorithm\footnote{S. Chaudhuri, M. Herlihy, N. A. Lynch, M. R. Tuttle. Tight bounds for $k$-set agreement. J. ACM, 2000}}
  \begin{columns}
    \begin{column}{0.47\textwidth}
      \begin{algorithm}[H]
        \small
        \DontPrintSemicolon{}
        \SetKwProg{Fn}{Process}{:}{}
        \Fn{$\mathrm{FloodMin}(n, t, v)$}{
          \For{$i = 0$ to $t$}{
            broadcast $v$\;
            receive $u_0, \dotsc, u_{l-1}$\;
            $v \leftarrow \min \{ u_0, \dotsc, u_{l-1}\}$\;
          }
          return $v$;
        }
      \end{algorithm}
    \end{column}%
    \begin{column}{0.53\textwidth}
      \begin{description}[$v \in \{0, 1\}:$]
      \item[$n \in \N$:] number of processes
      \item[$t \leq n$:] number of allowed crashes
      \item[$v \in \{0, 1\}$:] initial value
      \item[Output:] a common value
      \end{description}
    \end{column}
  \end{columns}%
  \pause%
  \begin{block}{}
    Crashing processes may not finish their broadcast, but:
    \begin{itemize}
    \item if all process agree initially then {\color{ForestGreen} OK}
    \item if no process crashes in an iteration then {\color{ForestGreen} OK}
    \item $t+1$ iterations $\implies$ one such round
    \end{itemize}
  \end{block}%
  \pause%
  \begin{block}{Characteristics}
    \begin{description}[Parameterised:]
    \item[Parameterised:] $n$ processes and $t$ allowed faults
    \item[Synchronous:] but asynchronous algorithms can also be
      handled
    \item[Unbounded:] each process sends unboundedly many messages
    \end{description}
  \end{block}
\end{frame}

\begin{frame}{Configurations: How LTAs represent executions.}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{algorithm}[H]
        \small
        \DontPrintSemicolon{}
        \SetKwProg{Fn}{Process}{:}{}
        \Fn{$\mathrm{FloodMin}(n, t, v)$}{
          \For{$i = 0$ to $t$}{
            broadcast $v$\;
            receive $u_0, \dotsc, u_{l-1}$\;
            $v \leftarrow \min \{ u_0, \dotsc, u_{l-1}\}$\;
          }
          return $v$;
        }
      \end{algorithm}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{block}{Defining States}
        \begin{description}[$v_0$:]
        \item[$v_0$:] has $v=0$, is not crashing
        \item[$v_1$:] has $v=1$, is not crashing
        \item[$c_0$:] has $v=0$, is crashing
        \item[$c_1$:] has $v=1$, is crashing
        \end{description}
      \end{block}
    \end{column}
  \end{columns}
  \pause
  \begin{columns}
    \begin{column}{0.47\textwidth}
      \begin{block}{Layered Configuration}
        \resizebox{\textwidth}{!}{
          \begin{tikzpicture}[
            xscale = 1.7,
            yscale=0.7,
            highlightstate/.style={draw, thick},
            fadedstate/.style={draw, gray},
            every edge/.style={draw, thick, -latex'}
            ]
            \node (val) at (1.5, -3.5) {Parameter values: $n : 5, t : 2$};
            \node (v0) at (0, 3) {$\lay=0$};
            \node (l1) at (1, 3) {$\lay=1$};
            \node (l2) at (2, 3) {$\lay=2$};
            \node (l3) at (3, 3) {$\lay=3$};

            \node[fadedstate] (s00) at (0, 1) {$v_0: 0$};
            \node[highlightstate] (c00) at (0, 2) {$c_0: 1$};
            \node[highlightstate] (s01) at (0, -1) {$v_1: 4$};
            \node[fadedstate] (c01) at (0, -2) {$c_1: 0$};

            \node[fadedstate] (s10) at (1, 1) {$v_0: 0$};
            \node[highlightstate] (c10) at (1, 2) {$c_0: 1$};
            \node[highlightstate] (s11) at (1, -1) {$v_1: 3$};
            \node[fadedstate] (c11) at (1, -2) {$c_1: 0$};

            \node[highlightstate] (s20) at (2, 1) {$v_0: 1$};
            \node[fadedstate] (c20) at (2, 2) {$c_0: 0$};
            \node[highlightstate] (s21) at (2, -1) {$v_1: 2$};
            \node[fadedstate] (c21) at (2, -2) {$c_1: 0$};

            \node[highlightstate] (s30) at (3, 1) {$v_0: 3$};
            \node[fadedstate] (c30) at (3, 2) {$c_0: 0$};
            \node[fadedstate] (s31) at (3, -1) {$v_1: 0$};
            \node[fadedstate] (c31) at (3, -2) {$c_1: 0$};

            \draw (s01.east) edge (c10.west);
            \draw (s01.east) edge node[below] {$\times 3$} (s11.west);

            \draw (s11.east) edge (s20.west);
            \draw (s11.east) edge node[below] {$\times 2$} (s21.west);

            \draw (s20.east) edge (s30.west);
            \draw (s21.east) edge node[right] {$\times 2$} (s30.west);
          \end{tikzpicture}%
        }
      \end{block}
    \end{column}%
    \begin{column}{0.53\textwidth}
      A configuration consists of:
      \begin{itemize}
      \item<3-> A \emph{valuation} of the parameters
      \item<4-> Some states arranged in \emph{Layers} (i.e. columns).
      \item<5-> The \emph{number} of processes in each states
      \item<6-> The \emph{flow} of processes between states.
      \end{itemize}
    \end{column}%
  \end{columns}%
\end{frame}

\begin{frame}{Threshold Guards}
  \begin{columns}
    \begin{column}{0.46\textwidth}
      \begin{algorithm}[H]
        \small
        \DontPrintSemicolon{}
        \SetKwProg{Fn}{Process}{:}{}
        \Fn{$\mathrm{FloodMin}(n, t, v)$}{
          \For{$i = 0$ to $t$}{
            broadcast $v$\;
            receive $u_0, \dotsc, u_{l-1}$\;
            $v \leftarrow \min \{ u_0, \dotsc, u_{l-1}\}$\;
          }
          return $v$;
        }
      \end{algorithm}
    \end{column}
    \begin{column}{0.54\textwidth}
      \resizebox{\textwidth}{!}{
        \begin{tikzpicture}[
          xscale=2,
          yscale=0.6,
          highlightstate/.style={draw, thick},
          fadedstate/.style={draw, gray},
          every edge/.style={draw, thick, -latex'}
          ]
          \node (val) at (1.5, -3.5) {Parameter values: $n : 5, t : 2$};
          \node (v0) at (0, 3) {$\lay=0$};
          \node (l1) at (1, 3) {$\lay=1$};
          \node (l2) at (2, 3) {$\lay=2$};
          \node (l3) at (3, 3) {$\lay=3$};

          \node[fadedstate] (s00) at (0, 1) {$v_0: 0$};
          \node[highlightstate] (c00) at (0, 2) {$c_0: 1$};
          \node[highlightstate] (s01) at (0, -1) {$v_1: 4$};
          \node[fadedstate] (c01) at (0, -2) {$c_1: 0$};

          \node[fadedstate] (s10) at (1, 1) {$v_0: 0$};
          \node[highlightstate] (c10) at (1, 2) {$c_0: 1$};
          \node[highlightstate] (s11) at (1, -1) {$v_1: 3$};
          \node[fadedstate] (c11) at (1, -2) {$c_1: 0$};

          \node[highlightstate] (s20) at (2, 1) {$v_0: 1$};
          \node[fadedstate] (c20) at (2, 2) {$c_0: 0$};
          \node[highlightstate] (s21) at (2, -1) {$v_1: 2$};
          \node[fadedstate] (c21) at (2, -2) {$c_1: 0$};

          \node[highlightstate] (s30) at (3, 1) {$v_0: 3$};
          \node[fadedstate] (c30) at (3, 2) {$c_0: 0$};
          \node[fadedstate] (s31) at (3, -1) {$v_1: 0$};
          \node[fadedstate] (c31) at (3, -2) {$c_1: 0$};

          \draw (s01.east) edge (c10.west);
          \draw (s01.east) edge node[below] {$\times 3$} (s11.west);

          \draw (s11.east) edge (s20.west);
          \draw (s11.east) edge node[below] {$\times 2$} (s21.west);

          \draw (s20.east) edge (s30.west);
          \draw (s21.east) edge node[right] {$\times 2$} (s30.west);
        \end{tikzpicture}%
      }
    \end{column}
  \end{columns}
  \begin{block}{When can a process move from $v_1$ to $v_0$?}
    \begin{itemize}
    \item<2-> If it has received a value $u_i = 0$
    \item<3-> Meaning \emph{another process} has $v = 0$
    \item<4-> Meaning some processes are in $v_0$ or $c_0$
    \end{itemize}
    \onslide<5->{In an LTA, this is written: $\LTAguard(v_1, v_0): v_0 + c_0 > 0$}
  \end{block}
  \onslide<6->{\alert{An edge can only be taken if the corresponding guard is satisfied.}}
\end{frame}

\section{Interacting With PyLTA}

\begin{myverbbox}{\vparam}
PARAMETERS: n, t
\end{myverbbox}
\begin{myverbbox}{\vparamrel}
PARAMETER_RELATION: t <= n
\end{myverbbox}
\begin{myverbbox}{\vlayers}
LAYERS: L, L
\end{myverbbox}
\begin{myverbbox}{\vstates}
STATES: L.v0, L.v1, L.c0, L.c1
\end{myverbbox}
\begin{myverbbox}{\vcasea}
CASE L.v0:
  IF TRUE THEN L.v0
  IF TRUE THEN L.c0
\end{myverbbox}
\begin{myverbbox}{\vcaseb}
CASE L.v1:
  IF L.v0 + L.c0 > 0 THEN L.v0
  IF L.v0 + L.c0 > 0 THEN L.c0
  IF L.v0 == 0 THEN L.v1
  IF L.v0 == 0 THEN L.c1
\end{myverbbox}
\begin{frame}[fragile]{The PyLTA Input Language}
  \resizebox{\textwidth}{!}{
    \begin{tikzpicture}[
      yscale=0.4,
      codenode/.style={anchor=north west},
      legendnode/.style={anchor=north east, ForestGreen}
      ]
      \onslide<1->{
        \node[codenode] (param) at (0, 0) {\vparam};
        \node[legendnode] (lparam) at (11, 0) {Define parameters $n$ and $t$};
      }\onslide<1->{
        \node[codenode] (paramrel) at (0, -1) {\vparamrel};
        \node[legendnode] (lparamrel) at (11, -1) {Impose $t \leq n$};
      }\onslide<2->{
        \node[codenode] (layers) at (0, -3) {\vlayers};
        \node[legendnode] (llayers) at (11, -3) {Define a single repeating layer $L$};
      }\onslide<2->{
        \node[codenode] (states) at (0, -4) {\vstates};
        \node[legendnode] (lstates) at (11, -4) {Define the states in layer $L$};
      }\onslide<3->{
        \node[codenode] (case1) at (0, -6) {\vcaseb};
        \node[legendnode] (lcase1) at (11, -6) {Define the guards for $v_1$};
      }\onslide<4->{
        \node[codenode] (case0) at (0, -12) {\vcasea};
        \node[legendnode] (lcase0) at (11, -12) {Define the guards for $v_0$};
      }
    \end{tikzpicture}
  }
\end{frame}

\begin{myverbbox}{\vvalidity}
WITH
  L.one0: L.v0 + L.c0 > 0
  L.ini: L.v0 + L.v1 + L.c0 + L.c1 == n
VERIFY: L.ini & ! L.one0 -> ! F L.one0
\end{myverbbox}
\begin{frame}[fragile]{Properties of LTAs}
  \begin{columns}
    \begin{column}{0.53\textwidth}
      \resizebox{\textwidth}{!}{
        \begin{tikzpicture}[
          xscale=2,
          yscale=0.6,
          highlightstate/.style={draw, thick},
          fadedstate/.style={draw, gray},
          every edge/.style={draw, thick, -latex'},
          truepred/.style={draw, ForestGreen, fill=ForestGreen!20},
          falsepred/.style={draw, Red, fill=Red!20}
          ]
          \node (val) at (1.5, 4) {Parameter values: $n : 5, t : 3$};
          \node (v0) at (0, 3) {$\lay=0$};
          \node (l1) at (1, 3) {$\lay=1$};
          \node (l2) at (2, 3) {$\lay=2$};
          \node (l3) at (3, 3) {$\lay=3$};

          \node[fadedstate] (s00) at (0, 1) {$v_0: 0$};
          \node[highlightstate] (c00) at (0, 2) {$c_0: 1$};
          \node[highlightstate] (s01) at (0, -0.5) {$v_1: 4$};
          \node[fadedstate] (c01) at (0, -1.5) {$c_1: 0$};

          \node[fadedstate] (s10) at (1, 1) {$v_0: 0$};
          \node[highlightstate] (c10) at (1, 2) {$c_0: 1$};
          \node[highlightstate] (s11) at (1, -0.5) {$v_1: 3$};
          \node[fadedstate] (c11) at (1, -1.5) {$c_1: 0$};

          \node[fadedstate] (s20) at (2, 1) {$v_0: 0$};
          \node[fadedstate] (c20) at (2, 2) {$c_0: 0$};
          \node[highlightstate] (s21) at (2, -0.5) {$v_1: 3$};
          \node[fadedstate] (c21) at (2, -1.5) {$c_1: 0$};

          \node[fadedstate] (s30) at (3, 1) {$v_0: 0$};
          \node[fadedstate] (c30) at (3, 2) {$c_0: 0$};
          \node[highlightstate] (s31) at (3, -0.5) {$v_1: 2$};
          \node[highlightstate] (c31) at (3, -1.5) {$c_1: 1$};

          \draw (s01.east) edge (c10.west);
          \draw (s01.east) edge node[below] {$\times 3$} (s11.west);

          \draw (s11.east) edge node[below] {$\times 3$} (s21.west);

          \draw (s21.east) edge node[above] {$\times 2$} (s31.west);
          \draw (s21.east) edge (c31.west);

          \onslide<3->{
            \node[anchor=east] (one0) at (-0.2, -3.5) {$\mathrm{one}_0$:};
            \node[truepred] (p0) at (0, -3.5) {$\true$};
            \node[truepred] (p1) at (1, -3.5) {$\true$};
            \node[falsepred] (p2) at (2, -3.5) {$\false$};
            \node[falsepred] (p3) at (3, -3.5) {$\false$};
            \node[anchor=east] (ini) at (-0.2, -4.5) {$\mathrm{ini}$:};
            \node[truepred] (p0) at (0, -4.5) {$\true$};
            \node[falsepred] (p1) at (1, -4.5) {$\false$};
            \node[falsepred] (p2) at (2, -4.5) {$\false$};
            \node[falsepred] (p3) at (3, -4.5) {$\false$};
          }
        \end{tikzpicture}%
      }
    \end{column}
    \begin{column}{0.47\textwidth}
      \begin{block}<2->{Predicate Definitions:}
        \begin{description}[$\mathrm{one}_0:$]
        \item[$\mathrm{one}_0:$] $v_0 + c_0 > 0$
        \item[$\mathrm{ini}:$] $v_0 + c_0 + v_1 + c_1 = n$
        \end{description}
      \end{block}
      \onslide<4->{\alert{Properties of LTAs are temporal formulas
          over a set of predicates.}}
    \end{column}
  \end{columns}
  \begin{block}<5->{Example: If no process has $v=0$ then no process will:}
    \[
      \mathrm{ini} \wedge \neg \mathrm{one}_0 \implies \neg \F \mathrm{one}_0
    \]
    \onslide<6->{
      \vvalidity
    }
  \end{block}
  \onslide<7->{PyLTA immediately verifies this property}
\end{frame}

\begin{myverbbox}{\vtermination}
WITH
  L.ini: L.v0 + L.v1 + L.c0 + L.c1 == n
  L.fair: L.v0 + L.v1 >= n - t
  L.decided: L.v0 + L.c0 == 0 | L.v1 + L.c1 == 0
VERIFY: L.ini & G L.fair -> F L.decided
\end{myverbbox}
\begin{myverbbox}{\vterminationb}
WITH
  L.ini: L.v0 + L.v1 + L.c0 + L.c1 == n
  L.fair: L.v0 + L.v1 >= n - t
  L.clean: L.c0 + L.c1 == 0
  L.decided: L.v0 + L.c0 == 0 | L.v1 + L.c1 == 0
VERIFY: L.ini & G L.fair & F L.clean -> F L.decided
\end{myverbbox}
\begin{frame}[fragile]{A More Complex Example: Liveness}
  \begin{block}{Termination}
    \vtermination
  \end{block}
  \pause
  \alert{PyLTA fails to prove or disprove this property.}
  \pause
  \begin{block}{Termination (under assumption)}
    \vterminationb
  \end{block}
  \alert{But it succeeds if we assume one round without crashes.}
\end{frame}

\section{Conclusion}
\begin{frame}{Conclusion}
  \begin{block}{What PyLTA can do:}
    \begin{itemize}
    \item Parameterised verification for \emph{every} number of processes at once
    \item Full LTL support (both safety and liveness)
    \item Both synchronous and asynchronous algorithms
    \end{itemize}
    This means PyLTA can verify in a few seconds a large variety of
    properties on different algorithms of the literature.
  \end{block}
  
  
  \begin{block}{Future Work}
    \begin{itemize}
    \item Lazy abstraction\footnotemark[1] to improve performances
    \item Ranking functions\footnotemark[2] to verify more properties
    \item Almost sure termination of randomised algorithms
    \end{itemize}
  \end{block}
\footnotetext[1]{S. Tonetta. Abstract Model Checking without Computing the Abstraction. FM 2009}
\footnotetext[2]{M.Heismann, J. Hoenicke, J.Leike, A. Podelski. Linear Ranking for Linear Lasso Programs. ATVA 2013}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
