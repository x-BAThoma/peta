\section{Modeling Distributed Algorithms}%
\label{sec: concepts}

In order to illustrate the capabilities of PyLTA, we use the Phase
King algorithm (Algorithm~\ref{alg: phase king})~\cite{BG-mst93}. In
general, the algorithms that can be handled by PyLTA exhibit the
following characteristics:
\begin{enumerate}
\item They are \textbf{parameterized}: in Algorithm~\ref{alg: phase
    king}, $n$ denotes the number of processes and $t$ a bound on the
  number of \emph{Byzantine} faults. PyLTA verifies the algorithm for
  \emph{all} the valuations of these parameters at once.

\item They can exchange \textbf{messages in an unbounded} domain: the
  indices $2i$ and $2i + 1$ in Algorithm~\ref{alg: phase king} are not
  bounded by a constant.

\item They can be \textbf{synchronous} or \textbf{asynchronous} but
  must ensure \emph{communication closure}: sent and received messages
  are tagged with indices ($2i$ and $2i+1$ in Algorithm~\ref{alg:
    phase king}) that can only increase with time. As noted
  in~\cite{DDMW-cav19}, communication closure appears both in
  synchronous and asynchronous algorithms in the literature.
  
\item The algorithms should use \emph{threshold conditions}. This
  means that the conditions in branches on the algorithms should be
  arithmetic formulas comparing \emph{numbers} of received messages
  and the values of parameters (see line~\ref{line: phase king
    branch}).
\end{enumerate}

Under these conditions, algorithms can be encoded in an LTA.  The last
two conditions can often be worked around. For example, we will show
along this article how Algorithm~\ref{alg: phase king} can be verified
despite the fact that the condition on line~\ref{line: phase king
  choice} is not ameanable to counter abstraction  as it
  uses the identity of processes which is lost in the abstraction.

\begin{algorithm}[h]
  \small
  \DontPrintSemicolon{}
  \SetKwProg{Fn}{Process}{:}{}
  \Fn{$\mathrm{PhaseKing}(n, t, \mathrm{id}, v)$}{
    \KwData{
      $n$ processes,
      $t < \frac{n}{4}$ Byzantine faults,
      $\mathrm{id} \in \{0 \dotso n-1\}$,
      $v \in \{0, 1\}$.
    }
    \For(\tcp*[f]{Start of layer $\lay=2i$}){$i = 0$ to $t$}{
      broadcast $(2i, v)$ \tcp*[f]{State $a_v$, $v \in \{0, 1\}$}\label{line: phase king a}\;
      $n_0 \leftarrow$ number of messages $(2i, 0)$ received\;
      $n_1 \leftarrow$ number of messages $(2i, 1)$ received\tcp*[f]{Start of layer $\lay=2i+1$}\label{line: phase king end layer 0}\;
      \uIf(\tcp*[f]{Current process is king}){$i == \mathrm{id}$\label{line: phase king choice}}{
        \lIf{$n_0 \geq n_1$ }{
          $v \leftarrow 0$ \tcp*[f]{State $k_0$}\label{line: phase king k0}
        }\lElse{
          $v \leftarrow 1$ \tcp*[f]{State $k_1$}\label{line: phase king k1}
        }
        broadcast $(2i + 1, v)$\;
      }
      \lElseIf{$n_0 > \frac{n}{2} + t$\label{line: phase king branch}}{
        $v \leftarrow 0$ \tcp*[f]{State $b_0$} \label{line: phase king b0}
      }\lElseIf{$n_1 > \frac{n}{2} + t$}{
        $v \leftarrow 1$ \tcp*[f]{State $b_1$} \label{line: phase king b1}
      }\lElse{
        $v \leftarrow v'$ where $(2i+1, v')$ is the king's message \tcp*[f]{State $b_?$}\label{line: phase king bu}
      }
    }
    return $v$;
  }
  \caption{The Phase King algorithm~\cite{BG-mst93} is a synchronous
    algorithm that solves binary consensus under $t < \frac{n}{4}$
    Byzantine faults. It executes $t{+}1$ rounds, and each round
    $i \in \{0 \dotso t\}$ is further decomposed into two layers (for
    round~$i$, the layers are named $2i$ and $2i{+}1$). In layer $2i$,
    the processes broadcast their preferences $v$, and in layer
    $2i{+}1$, they update $v$ either to the majority if it is strong
    enough, or to the preference of the process with id $i$, which is
    the king of the round~$i$.}\label{alg: phase king}
\end{algorithm}
  
  %\paragraph{Phase King Algorithm.}
Algorithm~\ref{alg: phase king} uses the parameters~$n$, and~$t$ with
the condition $t < \frac{n}{4}$.  We introduce an additional
paramter~$f\leq t$ which is the actual number of faulty processes: the
algorithm does not have access to~$f$, but it is used during
verification.  
%The algorithm is \emph{communication closed}: the
%indices~$2i$ and~$2i+1$ that tag the messages increase at each rounds.
Communication closure yields a layered structure of our models: a \emph{layer} indexed
by $\lay \in \N$ models the portion of the program that deals with
messages tagged with $\lay$. In Algorithm~\ref{alg: phase king}, the
layer $\lay = 2i$ corresponds to lines~\ref{line: phase king
  a}-\ref{line: phase king end layer 0}, while layer $\lay = 2i+1$
corresponds to lines~\ref{line: phase king choice}-\ref{line: phase
  king bu}.

We use \emph{counter abstraction} to model executions of the
algorithm, meaning that we define a counter storing the number of processes
at each state of the algorithm. Here, our approach differs from other works on threshold
automata because we count the number of processes that \emph{have
  been} through the state instead of those that are
\emph{currently} in it. It follows that the number of messages $m$
sent during the execution can be accurately deduced from these counter values 
as the number of processes at states where messages $m$ have been sent.
The downside of counter abstraction is that the identities of the
processes are lost. Notably, the condition on line~\ref{line: phase
  king choice} needs to be abstracted with a non deterministic
choice.

\begin{figure}[h]
  \centering
  %\resizebox{\textwidth}{!}{
    \begin{tikzpicture}[xscale = 1,yscale=0.5, every node/.style={}]
      \node (val) at (4, -3) {Parameter values: $n : 5, t : 1, f : 1$};
      \node (l0) at (0, 3) {$\lay=0$};
      \node (l1) at (2, 3) {$\lay=1$};
      \node (l2) at (4, 3) {$\lay=2$};
      \node (l3) at (6, 3) {$\lay=3$};
      \node (l4) at (8, 3) {$\lay=4$};
      \node[gray] (ldots) at (9, 3) {$\cdots$};
      \node[gray] (dots0) at (9, 1) {$\cdots$};
      \node[gray] (dots1) at (9, -1) {$\cdots$};

      \node[draw] (a00) at (0, -1) {$a_0: 2$};
      \node[draw] (a01) at (0, 1) {$a_1: 2$};

      \node[draw, gray] (k10) at (2, -2) {$k_0: 0$};
      \node[draw, gray] (b10) at (2, -1) {$b_0: 0$};
      \node[draw] (b1u) at (2, 0) {$b_?: 4$};
      \node[draw, gray] (b11) at (2, 1) {$b_1: 0$};
      \node[draw, gray] (k11) at (2, 2) {$k_1: 0$};

      \node[draw] (a20) at (4, -1) {$a_0: 1$};
      \node[draw] (a21) at (4, 1) {$a_1: 3$};

      \node[draw, gray] (k30) at (6, -2) {$k_0: 0$};
      \node[draw, gray] (b30) at (6, -1) {$b_0: 0$};
      \node[draw] (b3u) at (6, 0) {$b_?: 2$};
      \node[draw] (b31) at (6, 1) {$b_1: 1$};
      \node[draw] (k31) at (6, 2) {$k_1: 1$};

      \node[draw, gray] (a40) at (8, -1) {$a_0: 0$};
      \node[draw] (a41) at (8, 1) {$a_1: 2$};

      \draw[-latex'] (a00) edge node[below] {$\times 2$} (b1u);
      \draw[-latex'] (a01) edge node[above] {$\times 2$} (b1u);

      \draw[-latex'] (b1u) edge (a20);
      \draw[-latex'] (b1u) edge node[above] {$\times 3$} (a21);

      \draw[-latex'] (a20) edge (b3u);
      \draw[-latex'] (a21) edge (b3u);
      \draw[-latex'] (a21) edge (b31);
      \draw[-latex'] (a21) edge (k31);

      \draw[-latex'] (k31) edge (a41);
      \draw[-latex'] (b31) edge (a41);
      
    \end{tikzpicture}%
    \caption{A configuration of the Phase King algorithm
      (Algorithm~\ref{alg: phase king}).}
  \label{fig: config phase king}
\end{figure}

\paragraph{Configurations.} PyLTA verifies properties on
all reachable \emph{configurations}. A configuration can be
interpreted as a record of events that occured during an
execution. An example is depicted in Fig.~\ref{fig: config phase king}
which we now explain.

The configuration contains an instantiation of the parameter values
(given on the bottom of the figure).  Moreover, for each layer index,
it specifies the number of \emph{correct} (i.e. non-faulty) processes
that were at a given state at that layer; as well as the number of
correct processes that moved from one state to another between
consecutive layers.

In Fig.~\ref{fig: config phase king}, initially, 2 correct processes
are at state~$a_1$, and $2$ are at~$a_0$, for a parameter
valuation~$n=5,t=1,f=1$.  Recall that layers $2i$ and~$2i+1$
correspond to round~$i$, and that the meaning of the states are given
in Algorithm~\ref{alg: phase king}; in particular, $a_x$ is the first
line of an iteration where variable~$v$ has value~$x$.  All 4 correct
processes go to~$b_{\text{?}}$ at layer~$1$, which means that the
Byzantine process was king at round~$0$.  Then three of them go to
$a_1$ at layer~$3$, and one of them goes to~$a_0$, etc.  This models
the situation where the Byzantine process sent a message
$(2\times 0 + 1, 1)$ to the latter process but $(2\times 0+1, 0)$ to
the others. In the next layer, a correct process is king with value
$1$ (state $k_1$), and one correct process has received a majority of
value~$1$ (state $b_1$), but not all correct processes have arrived to
layer $4$ yet.  This configurations thus represents a finite prefix of
an execution. When needed, LTL fairness assumptions can ensure that we
only consider infinite configurations.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
