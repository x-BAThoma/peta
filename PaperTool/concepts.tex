\section{Parameterized Distributed Algorithms}%
\label{sec: concepts}
In this paper, we present a tool for the formal verification of
distributed algorithms with arbitrary numbers of processes with
Byzantine or crash faults. We focus on algorithms that exhibits a
\emph{layered} structure. This approach is similar to the one done in
\cite{EF-scp82,DDMW-cav19}, it supposes that every messages sent by
the algorithms are labeled with an index $\lay \in \N$. The
communications performed by any process during any execution should
concern messages with non decreasing values of $\lay$. More
explicitly, a process should start by sending messages with
$\lay = 0$, then receive messages with $\lay = 0$ before moving on to
$\lay = 1$ and all the other values in order.

% As noted in \cite{DDMW-cav19}, the labeling of the messages is often
% implicit in the code of the algorithms, but in this paper, we will
% always write it explicitly.

Such a structure is naturally enforced for \emph{synchronous}
algorithms such as~\cite{BG-mst93} \todo{cite more} but it is also
present in many asynchronous ones such as~\cite{BenOr-podc83}
\todo{cite more}. Our method will be able to handle both cases.

\paragraph{Running Example: The Phase King Algorithm.}
\begin{algorithm}[h]
  \small
  \DontPrintSemicolon{}
  \SetKwProg{Fn}{Process}{:}{}
  \Fn{$\mathrm{PhaseKing}(n, t, \mathrm{id}, v)$}{
    \KwData{
      $n$ processes,
      $t < \frac{n}{4}$ Byzantine faults,
      $\mathrm{id} \in \{0 \dotso n-1\}$,
      $v \in \{0, 1\}$.
    }
    \For{$i = 0$ to $t$}{
      %\tcc{Messages with $\lay = 2i$.}
      broadcast $(2i, v)$ \tcp*[h]{Start of layer $\lay=2i$}\;
      receive all the messages $(2i, \_)$\;
      $n_0 \leftarrow$ number of messages $(2i, 0)$ received\;
      $n_1 \leftarrow$ number of messages $(2i, 1)$ received\;
      %\tcc{beginning of layer $2i+1$}
      \uIf(\tcp*[h]{Current process is king}){$i == \mathrm{id}$\label{line: phase king choice}}{
        \lIf{$n_0 \geq n_1$ }{
          %\tcc{Messages with $\lay = 2i+1$.}
          broadcast $(2i + 1, 0)$ \tcp*[h]{Start of layer $\lay=2i+1$}
        }\lElse{
          broadcast $(2i + 1, 1)$
        }
      }
      \lIf{$n_0 > \frac{n}{2} + t$\label{line: phase king branch}}{
        $v \leftarrow 0$
      }\lElseIf{$n_1 > \frac{n}{2} + t$}{
        $v \leftarrow 1$
      }\lElse{
        $v \leftarrow v'$ where $(2i+1, v')$ is the king's message
      }
    }
    return $v$;
  }
  \caption{The Phase King algorithm~\cite{BG-mst93}, is a
    \emph{synchronous} algorithm that solves binary consensus under
    $t < \frac{n}{4}$ Byzantine faults. It executes $t{+}1$ rounds.
    In round $i \in \{0 \dotso t\}$, the local value $v$ of each
    process is updated either according to the majority, or to the
    preferred value of the process with id $i$ (the King process).}%
  \label{alg: phase king}
    
\end{algorithm}

We will illustrate our tool's input language and its capabilities on
the Phase King algorithm (Algorithm~\ref{alg: phase king}) by Berman, Garay
and Perry~\cite{BG-mst93}. 
This algorithm solves synchronous consensus for $n$ processes under at most
$t < \frac{n}{4}$ Byzantine faults.
Each process starts with an initial value~$v$, and the objective is that
non-faulty processes agree on a unique value.
An execution of this algorithm consists of $t+1$ \emph{rounds},
in which different processes are
designated as king. The correction of the algorithm relies on the
property that in at least one round a non-Byzantine process is king.

During an execution of the body of the loop of Algorithm~\ref{alg:
  phase king}, a process will broadcast messages labeled with $2i$ 
then receive messages with the same label  (layer $2i$). If the process is the king
of the round it will then broadcast a message labeled with $2i+1$,
otherwise it will skip this step and proceed to receive messages
labeled with $2i+1$ (layer $2i+1$). 
% This algorithm is therefore \emph{layered} and
% we will call layer $\lay \in \N$ the portion of executions that start
% with the first message labeled $\lay$ sent or received and ends just
% before the first message with a greater label appears (some layers may
% be empty).

% \bt{I renounced showing the beginning and end of layers in the
%   algorithm as these get split by the different branches. I think it's
%   clearer to say that being layered is a property that must hold for
%   the executions of the algorithm}

% In the literature the notion of rounds is sometimes used instead of
% layers. But in Algorithm~\ref{alg: phase king}, we find it clearer to
% have round $i$ denote the $i+1$-th execution of the body of the loop
% and layer $2i$ and $2i+1$ being the smaller subdivision.

% For each process and each round, we keep track of their value of $v$,
% whether the process is king of the round and in this case, the value it
% broadcast (which may be different from its value $v$). This leads to
% a total of $6$ states for each process and each round.
% \todo[inline]{Insert a precise definition of a layer (currently not clear)}

% Each round is further divided into \emph{layers}.
% In fact, the broadcast value can only be set after all the values of
% $v$ are known. A round is therefore further decomposed into two
% parts we call \emph{layers}: the first layer includes 
% the first broadcast and all instructions that precede,
% while the rest is the second layer.
% In the literature, the notion
% of round is sometimes, but not always, used to name this subdivision
% instead.

% \bt{Move this somewhere else?}  \os{I don't think this paragraph is even necessary}
% For a set $X$, we denote with $\PA(X)$
% the set of Linear arithmetic formulas with free variables in $X$. For
% a formula $\varphi \in \PA(X)$ and a valuation $\xi: X \to \Z$,
% $\xi \models \varphi$ means that $\varphi$ holds when replacing every
% variable $x \in X$ with the value $\xi(x) \in \Z$. In this paper,
% valuations will only take values in $\N$, but the notations remain the
% same. 

\paragraph{Modeling Formalism.}
Layered threshold automata (LTA) are an expressive formalism used to
model distributed algorithms. These consist in an automata-based
description describing the joint behaviors of a distributed algorithm
with parameters such as the number of processes and the maximal number
of faulty processes (respectively, $n$ and~$t$ here). An LTA
associates to each $\lay \in \N$ a set of \emph{states} that forms a
\emph{layer}. To each such state is associated an integer variables
that counts the number of processes that went through this
state. Transitions between states of successive layers are then
guarded by linear arithmetic formulas over these variables as well as
the parameters. The formal definition is given in the following.

For a set $X$, we denote with $\PA(X)$
the set of linear arithmetic formulas with free variables in $X$.
\begin{definition}
  A layered threshold automaton (LTA) is a tuple
  $\LTAObj = \left(\LTAR, {\left(\LTAS_{\lay}\right)}_{\lay \in \N},
    {\left(\LTAguard_\lay\right)}_{\lay \in \N} \right)$ with:
  \begin{itemize}
  \item $\LTAR$ a finite set of \emph{parameters}.
  \item for $\lay \in \N$, $\LTAS_{\lay}$ is a finite set called
    \emph{layer} whose elements are \emph{states}.
  \item for $\lay \in \N$,
    $\LTAguard_{\lay}: \LTAS_{\lay} \times \LTAS_{\lay+1} \to
    \PA\left(\LTAR \cup \LTAS_{\lay}\right)$ associates a linear
    arithmetic \emph{guard} to each pair of states in successive
    layers. The free variables of these guards can either be
    \emph{parameters} of $\LTAR$ or \emph{states} of $\LTAS_{\lay}$.
  \end{itemize}
\end{definition}

The semantic of LTAs is based on \emph{counter abstraction} meaning
that instead of assigning states to processes, we assign a number of
processes for each state. LTAs differ from other TAs \todo{ref} in the
sense that they do not only count the processes that are
\emph{currently} in a given state but rather count the processes that
\emph{have been through} the state at some point. These counters
therefore only increase and when a message is sent in a given state,
the state counter stores accurately the number of messages that have
been sent at this location.

Consider the line~\ref{line: phase king branch} of Algorithm~\ref{alg:
  phase king}, the condition $n_0 > \frac{n}{2} + t$ references the
number of messages $(2i, 0)$ received by the process, as well as the
values of some parameters. The number of messages that can be received by the process depends on 


Intuitively, the different states of a layer $\LTAS_{\lay}$
corresponds to different local states that a process can be in when it
sends its message labeled $\lay$.

The semantic of an LTA is given by \emph{configurations} that define
the values of the parameters in $\LTAR$ as well as the number of
processes that went through each state $\LTAs$ of each layer
$\LTAS_{\lay}$ (a configuration is therefore an infinite
valuation). Initially, every such counter would be $0$ except for the
very first layer $\LTAS_0$ which would encode the initial valuation
of the processes. As time elapses, other layers get populated as
processes go through further states.

We consider \emph{cyclic} LTAs where both
${\left(\LTAS_{\lay}\right)}_{\lay \in \N}$ and
${\left(\LTAguard_{\lay}\right)}_{\lay \in \N}$ repeat with a
\emph{period} $\per > 0$, meaning that for any $\lay \in \N$,
$\LTAS_{\lay} = \LTAS_{\lay+\per}$ and
$\LTAguard_{\lay} = \LTAguard_{\lay + \per}$.  
% Many distributed
% algorithms have either this property or have a few prefix layer before
% the cycle. It is easy enough to extend the algorithms to this later
% case, but we will keep the pure cycle here for the sake of simplicity.

% \os{LTAs have been used for formally verifying ....}  \bt{LTAs have
%   only been introduced as is in the Concur article, so I don't really
%   know what to write here.}

\begin{figure}[h]
  \centering
  \begin{subfigure}{0.37\textwidth}
    \resizebox{\textwidth}{!}{
      \begin{tikzpicture}[scale = 0.8, every node/.style={}]
        \node (l0) at (0, 3) {$\lay = 0$};
        \node (l1) at (2, 3) {$\lay = 1$};
        \node (l2) at (4, 3) {$\lay = 2$};

        
        \node (a0) at (0, -1) {$a_0$};
        \node (a1) at (0, 1) {$a_1$};

        \node (k0) at (2, -2) {$k_0$};
        \node (b0) at (2, -1) {$b_0$};
        \node (bu) at (2, 0) {$b_?$};
        \node (b1) at (2, 1) {$b_1$};
        \node (k1) at (2, 2) {$k_1$};

        \node (a20) at (4, -1) {$a_0$};
        \node (a21) at (4, 1) {$a_1$};

        \path[draw] (a0.east) -- (1, -1);
        \path[draw] (a1.east) -- (1, 1);
        \path[draw] (1, -2) -- (1, 2);
        \path[draw, ->] (1, -2) -- (k0.west);
        \path[draw, ->] (1, -1) -- (b0.west);
        \path[draw, ->] (1, 0) -- (bu.west);
        \path[draw, ->] (1, 1) -- (b1.west);
        \path[draw, ->] (1, 2) -- (k1.west);

        \path[draw, ->] (k0) -- (a20);
        \path[draw, ->] (b0) -- (a20);
        \path[draw, ->] (bu) -- (a20);
        \path[draw, ->] (bu) -- (a21);
        \path[draw, ->] (b1) -- (a21);
        \path[draw, ->] (k1) -- (a21);
      \end{tikzpicture}%
    }%
  \end{subfigure}\quad%
  \begin{subfigure}{0.55\textwidth}
    Parameters $\LTAR = \{n, t, f\}$
    
    For $x, y \in \{0, 1\}$:
    \begin{align*}
      \LTAguard_0(a_x, k_y) &: 2 (a_y + f) \geq n \\
      \LTAguard_0(a_x, b_y) &: 2 (a_y + f) > n + 2t \\
      \LTAguard_0(a_x, b_?) &: 2 a_0 \leq n + 2t \wedge 2 a_1 \leq n + 2t \\
      \LTAguard_1(k_x, a_x) &: \true \\
      \LTAguard_1(b_x, a_x) &: \true \\
      \LTAguard_1(b_?, a_x) &: k_{1-x} = 0
    \end{align*}
    All other guards are set to $\false$.
  \end{subfigure}
  \caption{The LTA for Phase King algorithm. Only the first three
    layers are represented as they repeat with period $2$. 
    % Two states are linked when their guard is not $\false$.
    }%
  \label{fig: lta phase king}
\end{figure}

\begin{figure}[h]
  \centering
  %\resizebox{\textwidth}{!}{
    \begin{tikzpicture}[xscale = 1.2,yscale=0.8, every node/.style={}]
      \node (val) at (4, -3) {Parameter values: $n = 5, t = 1, f = 1$};
      \node (l0) at (0, 3) {$\lay=0$};
      \node (l1) at (2, 3) {$\lay=1$};
      \node (l2) at (4, 3) {$\lay=2$};
      \node (l3) at (6, 3) {$\lay=3$};
      \node (l4) at (8, 3) {$\lay=4$};
      \node[gray] (ldots) at (9, 3) {$\cdots$};
      \node[gray] (dots0) at (9, 1) {$\cdots$};
      \node[gray] (dots1) at (9, -1) {$\cdots$};

      \node[draw] (a00) at (0, -1) {$a_0: 2$};
      \node[draw] (a01) at (0, 1) {$a_1: 2$};

      \node[draw, gray] (k10) at (2, -2) {$k_0: 0$};
      \node[draw, gray] (b10) at (2, -1) {$b_0: 0$};
      \node[draw] (b1u) at (2, 0) {$b_?: 4$};
      \node[draw, gray] (b11) at (2, 1) {$b_1: 0$};
      \node[draw, gray] (k11) at (2, 2) {$k_1: 0$};

      \node[draw] (a20) at (4, -1) {$a_0: 1$};
      \node[draw] (a21) at (4, 1) {$a_1: 3$};

      \node[draw, gray] (k30) at (6, -2) {$k_0: 0$};
      \node[draw, gray] (b30) at (6, -1) {$b_0: 0$};
      \node[draw] (b3u) at (6, 0) {$b_?: 2$};
      \node[draw] (b31) at (6, 1) {$b_1: 1$};
      \node[draw] (k31) at (6, 2) {$k_1: 1$};

      \node[draw, gray] (a40) at (8, -1) {$a_0: 0$};
      \node[draw] (a41) at (8, 1) {$a_1: 2$};

      \draw[->] (a00) edge node[below] {$\times 2$} (b1u);
      \draw[->] (a01) edge node[above] {$\times 2$} (b1u);

      \draw[->] (b1u) edge (a20);
      \draw[->] (b1u) edge node[above] {$\times 3$} (a21);

      \draw[->] (a20) edge (b3u);
      \draw[->] (a21) edge (b3u);
      \draw[->] (a21) edge (b31);
      \draw[->] (a21) edge (k31);

      \draw[->] (k31) edge (a41);
      \draw[->] (b31) edge (a41);
      
    \end{tikzpicture}%
  \caption{A configuration of the LTA of Figure~\ref{fig: lta phase king}}
  \label{fig: config phase king}
\end{figure}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:

% LocalWords:  Berman LTAs LTL liveness
