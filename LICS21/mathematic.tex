\section{A Fistful of Domain Theory}%
\label{sec:domain theory}
\subsection{Mathematical Preliminaries}%
\label{subsec:notations}
%==================================
% For $X$ and $Y$ two sets, we denote with $X^Y$ the set of functions
% from $Y$ to $X$. \todo{Necessary?}
%The set of natural (non negative) integers will be written $\N$.
This section presents mathematical notions as well as notations that
are used throughout the paper. In particular, it introduces partially
ordered sets and Scott topology. The interested reader is referred
to~\cite{AJ-hlcs95} for an thorough introduction to domain theory.

{\bf Sets and multisets.} A \emph{multiset} over a set $X$ is an
element of $\N^X$. Addition and inclusion over multisets are defined
in a natural way. For $\xi, \xi' \in \N^X$ two multisets,
$\xi + \xi' \in \N^X$ is the multiset such that for every $x \in X$,
$\left(\xi + \xi'\right)(x) = \xi(x) + \xi'(x)$. We write
$\xi \sqsubseteq \xi'$ if for every $x \in X$, $\xi(x) \leq
\xi'(x)$. Standard sets can be seen as special cases of multisets with
the canonical bijection between the set of subsets of $X$ ($2^X$) and
the set of functions from $X$ to $\{0, 1\}$.

{\bf Sequences.} For $X$ a set and $n \in \N$ a natural number, a
sequence of elements of $X$ of length $n$ is some
$u \in X^{\{0, \dotsc, n-1\}}$. Its length is $\wlen{u} = n$ and
  for $i < n$, $u(i) \in X$ denotes the letter at index $i$.
$X^* = \bigcup_{n \in \N} X^{\{0, \dotsc, n-1\}}$ (resp.
$X^+ = \bigcup_{n > 0} X^{\{0, \dotsc, n-1\}}$) denotes the set of all
\emph{finite} (resp.\ finite and non-empty) sequences of elements of
$X$. Moreover, $\closure{X^*} = X^* \cup X^\N$ is the set of finite or
\emph{infinite} sequences of $X$. For $u \in X^*$ a finite sequence
and $v \in \closure{X^*}$ a finite or infinite sequence, we write
$u \concat v$ for the \emph{concatenation} of $u$ and $v$. For $u$ and
$w$ two sequences, we write $u \pref w$ and say that $u$ is a
\emph{prefix} of $w$ if either $w$ is finite and there exists
$v \in \closure{X^*}$ such that $u \concat v = w$ or $u = w$. For $w$
a sequence and $i \leq \wlen{w}$, $w_i$ is the prefix $w$ of length $i$.

{\bf Closures and bounds for partially ordered sets.} Let
$(X, \myLeq)$ be a partially ordered set, and $\xi \subset X$. The
\emph{upward-closure} of $\xi$ is
$\upClos \xi = \{ x \in X \mid \exists x' \in \xi, x' \myLeq x\}$, and
$\xi$ is \emph{upward-closed} if $\upClos \xi = \xi$. Dually, one
defines the \emph{downward-closure} $\loClos \xi$ and
\emph{downward-closed} sets. An element $x \in X$ is an
\emph{upper-bound} of $\xi$ if for any element $x' \in \xi$,
$x' \myLeq x$. We write $\upBound(\xi)$ for the set of upper-bounds of
$\xi$. If it exists (it is then unique), the \emph{greatest} element
of $\xi$ is $x \in X$ such that $x \in \xi$ and $x \in \upBound(\xi)$.
Dually, one defines the notion of \emph{least} element by reversing
the order. If it exists, the \emph{least upper bound} of $\xi$ is the
least element of $\upBound(\xi)$, and we denote it by $\lub
\xi$. Finally $\xi$ is \emph{directed} if it is non-empty and if for
every two elements $x, x' \in \xi$,
$\upBound(\{x, x'\}) \cap \xi \neq \emptyset$; intuitively, any finite
subset of $\xi$ has an upper-bound in $\xi$. An interesting particular
case of directed case are completely ordered sets which are called
\emph{chains} in this context.

{\bf Directed Complete Partially ordered sets (DCPO).} A DCPO is a
partially ordered set $(X, \myLeq)$ such that any directed subset
$\xi \subset X$ has a (unique) least upper bound. These partially
ordered sets are particularly important in semantics of programming
languages. % Simple examples of DCPO are any set of the
% trivial order ($x \myLeq y$ iff $x = y$),
% $\closure{\N} = \N \cup \{\infty\}$ with the usual order (note however
% that $\N$, with usual order, is not a DCPO) and any finite poset.

{\bf The Scott Topology on DCPO.} Directed complete partial orders are
naturally equipped with the Scott topology. A subset $\xi$ of a DCPO
$(X, \myLeq)$ is \emph{Scott-closed} if it is \emph{downward-closed}
and if for any directed subset $\xi' \subset \xi$,
$\lub \xi' \in \xi$. A subset is \emph{Scott-open} if its complement
in $X$ is Scott-closed. Functions that are continuous for the Scott
topology are called \emph{Scott-continuous}. A function $f: X \to Y$
is \emph{monotonous} if for any $x, x' \in X$, if $x \myLeq x'$ then
$f(x) \myLeq f(x')$. A Scott-continuous function is always
monotonous. A function $f: X \to Y$ is Scott-continuous if and only if
for any directed subset $\xi \subset X$,
$f(\lub(\xi)) = \lub(f(\xi))$. In this paper, a \emph{partial}
function $f: X \to Y$ is called \emph{Scott-continuous} if its domain
$\dom(f)$ is Scott-closed and if for any directed subset
$\xi \subset \dom(f)$, $f(\lub \xi) = \lub f(\xi)$.
% We give a few more notable examples of DCPOs, also to illustrate
% Scott-continuity.
% \begin{itemize}
% \item Any finite poset is a DCPO\@.
% \item Any trivially ordered set ($x \myLeq y$ iff $x = y$) is a
%   DCPO\@.
% \item A cartesian product of DCPO is a DCPO and the projections are
%   Scott-continuous.
% \item The set of Scott-continuous functions between DCPOs $X$ and $Y$
%   is denoted by $[X \to Y]$. It is itself a DCPO when ordered
%   pointwise ($f \myLeq g$ if $\forall x \in X, f(x) \myLeq
%   f(y)$). Again, the projections (for $x \in X$,
%   $\pi_x: f \mapsto f(x)$) are Scott-continuous.
% %\item $\closure{\N} = \N \cup \{\infty\}$ with the usual order forms a
% %  DCPO (note that $\N$ alone is not a DCPO).
% \item The set $\closure{A^*}$ of finite or infinite sequences over an
%   alphabet $A$ ordered by the prefix ordering form a DCPO\@. A
%   morphism $\varphi: A^* \to B^*$ between two free monoids can be
%   extended uniquely into a Scott-continuous function
%   $\closure{\varphi}: \closure{A^*} \to \closure{B^*}$. For example,
%   the length function $\wlen{\cdot}: \closure{A^*} \to \closure{\N}$,
%   $w \mapsto \wlen{w}$ can be defined this way.
% \end{itemize}

%==================================================================%
\subsection{Partially Ordered  Transition Systems}%
\label{subsec:POTS}
Building on domain theory, this section introduces a generic model for
distributed transition systems, that will capture the semantics of
distributed algorithms. An ordering naturally appears on sets of sent
messages --that can only grow-- and the asynchrony requires the order
to be partial only.

\begin{definition}
  A \emph{partially ordered transition system} (POTS) is a
  tuple $\POTSObj = \left( \POTSX, \myLeq, \POTSA\right)$ where:
  \begin{itemize}
  \item $(\POTSX, \myLeq)$ forms a DCPO\@.
  \item $\POTSA$ is a set of partial functions, called \emph{actions},
    from $X$ to itself and such that for every $\POTSa \in \POTSA$ and
    every $\POTSx \in \dom(\POTSa)$, $\POTSx \myLeq
    \POTSa(\POTSx)$.
  \end{itemize}
\end{definition}

\begin{definition}
  A \emph{schedule} is a (finite or infinite) sequence of
  \emph{actions}: $\POTSsched = {\left(\POTSa_t\right)}_{t < T}$, with
  $T \in \closure{\N}$. A schedule
  $\POTSsched = {\left(\POTSa_t\right)}_{t < T}$ is \emph{applicable}
  at $\POTSx \in \POTSX$ if there exists a sequence
  ${\left( \POTSx_t \right)}_{t < T{+}1}$ with
%  \begin{itemize}
%  \item
  $\POTSx_0 = \POTSx$, and 
  % \item F
  for every $t < T$, $\POTSx_t \in \dom(\POTSa_t)$ and
    $\POTSa_t(\POTSx_t) = \POTSx_{t{+}1}$.
%  \end{itemize}
  In this case, we write $\POTSconfigs(\POTSx, \POTSsched)$ for the
  sequence ${\left( \POTSx_t \right)}_{t < T{+}1}$, and
  $\POTSx \POTSstar \POTSsched$ for
  $\lub \left\{ \POTSx_t \mid t < T{+}1 \right\}$.
\end{definition}

The above definition uses the convention that $\infty + 1 =
\infty$. Note that if $\sigma$ is applicable at $x$, then the sequence
${\left( \POTSx_t \right)}_{t < T+1}$ is unique. Moreover, the least
upper bound $\lub \left\{ \POTSx_t \mid t < T+1 \right\}$ exists
because for any $t < T$, $\POTSx_t \myLeq \POTSx_{t+1}$ and
$\left\{ \POTSx_t \mid t < T+1 \right\}$ is therefore a chain.  When
$\POTSsched = {\left(\POTSa_t\right)}_{t < T}$ is finite,
$\POTSx \POTSstar \POTSsched = \POTSx \POTSstar \POTSa_0 \POTSstar
\dotsb \POTSstar \POTSa_{T-1}$ denotes the last element of the
monotonous sequence $\POTSconfigs(\POTSx, \POTSsched)$. In particular,
for $\POTSa \in \POTSA$ and $\POTSx \in \dom(\POTSa)$,
$\POTSx \POTSstar \POTSa = \POTSa(\POTSx)$.  When
$\POTSsched_t \in \POTSA^t$ is defined as the prefix of length $t$ of
$\POTSsched$, $\POTSx_t = \POTSx \POTSstar \POTSsched_t$ and it
follows:
$\POTSx \POTSstar \POTSsched = \lub \{\POTSx \POTSstar \POTSsched_t
\mid t < T, t \in \N\}$.

The following lemma will be useful throughout the paper:
\begin{restatable}{lemma}{lemmaContinuityStar}\label{lemma: continuity star}
  For $\POTSx \in \POTSX$, the set $\POTSapplicable{\POTSx}$ of
  schedules applicable at $\POTSx$ is Scott-closed for the prefix
  ordering and the function:
  $[ \POTSx \POTSstar \_ ]: \POTSapplicable{\POTSx} \to \POTSX$ is
  Scott-continuous.
\end{restatable}


\begin{definition}\label{def: abstraction}
  An \emph{abstraction} between POTS
  $\POTSObj = \left( \POTSX, \myLeq, \POTSA\right)$ and
  $\POTSObj' = \left( \POTSX', \myLeq, \POTSA'\right)$ consists of
  %two functions $\POTSabX$ and $\POTSabA$
  \begin{itemize}
  \item a \emph{set abstraction} $\POTSabX: \POTSX \to \POTSX'$
    which is a Scott-continuous function;
  \item a \emph{monoid abstraction}
    $\POTSabA: \POTSA^* \to {\POTSA'}^*$ which is a monoid morphism
    (with slight abuse of notation, $\POTSabA$ also denotes its
    Scott-continuous extension
    $\closure{\POTSA^*} \to \closure{{\POTSA'}^*}$);
    % \item Additionally, for any $\POTSa \in \POTSA$ and any
    %   $\POTSx \in \dom(\POTSa)$,
    %   $\POTSabA(\POTSa) \in {\POTSA'}^*$
    %   should be applicable at $\POTSabX(\POTSx) \in \POTSX'$ and
    %   $\POTSabX(\POTSx \POTSstar \POTSa) = \POTSabX(\POTSX)
    %   \POTSstar \POTSabA(\POTSa)$.
  \end{itemize}
  both such that for every $\POTSa \in \POTSA$ and every
  $\POTSx \in \dom(\POTSa)$, $\POTSabA(\POTSa) \in {\POTSA'}^*$ is
  applicable at $\POTSabX(\POTSx) \in \POTSX'$ and
  $\POTSabX(\POTSx \POTSstar \POTSa) = \POTSabX(\POTSx)
  \POTSstar \POTSabA(\POTSa)$.
\end{definition}

The last condition of the definition of abstraction translates into
the commutativity of the diagram in
Figure~\ref{fig:POTSabstraction}. The soundness of the abstraction for
any (possibly infinite) schedule is stated in the following
proposition and illustrated on Figure~\ref{fig:POTSabstractionstar}.

\begin{figure}[htbp]
   \begin{subfigure}{0.45\textwidth}
    \centering
  \begin{tikzpicture}[scale=2]
    \node (X) at (0, 1) {\(
        \dom(\POTSa) \subset \POTSX
      \)};
    \node (Xa) at (2, 1) {$\POTSX$};
    \node (X') at (0, 0) {\(
      \dom(\POTSabA(a)) \subset \POTSX'
      \)};
    \node (Xa') at (2, 0) {$\POTSX'$};

    \draw[->] (X) edge node[above] {$\POTSa$} (Xa);
    \draw[->] (X) edge node[left] {$\POTSabX$} (X');
    \draw[->] (Xa) edge node[right] {$\POTSabX$} (Xa');
    \draw[->] (X') edge node[below] {$\POTSabA(\POTSa)$} (Xa');
  \end{tikzpicture}
  \caption{% When $(\POTSabX,\POTSabA)$ forms an abstraction between
    % the POTS $\left( \POTSX, \myLeq, \POTSA\right)$ and
    % $\left( \POTSX', \myLeq, \POTSA'\right)$, then the above
    By Definition~\ref{def: abstraction} diagram commutes for any
    action $\POTSa \in \POTSA$.}%
  \label{fig:POTSabstraction}
\end{subfigure}
\quad
  \begin{subfigure}{0.47\textwidth}
    \centering
    \begin{tikzpicture}[scale=2]
    \node (X0) at (0, 1) {$\POTSX$};
    \node (X1) at (1, 1) {$\POTSX$};
    \node (X2) at (2, 1) {$\cdots$};
    \node (X3) at (3, 1) {$\POTSX$};
    \node (X0') at (0, 0) {$\POTSX'$};
    \node (X1') at (1, 0) {$\POTSX'$};
    \node (X2') at (2, 0) {$\cdots$};
    \node (X3') at (3, 0) {$\POTSX'$};

    \draw[->] (X0) edge node[above] {$\POTSa_0$} (X1);
    \draw[->] (X1) edge node[above] {$\POTSa_1$} (X2);
    \draw[->] (X0') edge node[above] {$\POTSabA(\POTSa_0)$} (X1');
    \draw[->] (X1') edge node[above] {$\POTSabA(\POTSa_1)$} (X2');

    \draw[->] (X0) edge node[left] {$\POTSabX$} (X0');
    \draw[->] (X1) edge node[left] {$\POTSabX$} (X1');
    \draw[->] (X3) edge node[left] {$\POTSabX$} (X3');

    \draw[->] (X0) edge[bend left] node[above] {$\POTSsched$} (X3);
    \draw[->] (X0') edge[bend right] node[above] {$\POTSabA(\POTSsched)$} (X3');
  \end{tikzpicture}
  \caption{By Proposition~\ref{prop: correction abstraction} diagram commutes for any schedule $\POTSsched$.
    %When $(\POTSabX,\POTSabA)$ is an abstraction, the above diagram commutes for any schedule $\POTSsched$, 
    %as stated in Proposition~\ref{prop: correction abstraction}.
  }%
    \label{fig:POTSabstractionstar}
  \end{subfigure}
  \caption{$(\POTSabX,\POTSabA)$ forms an abstraction between
    the POTS $\left( \POTSX, \myLeq, \POTSA\right)$ and
    $\left( \POTSX', \myLeq, \POTSA'\right)$.}
  \label{fig:POTSabs}
\end{figure}


\begin{restatable}{proposition}{propCorrectionAbstraction}\label{prop: correction abstraction}
  Let $(\POTSabX, \POTSabA)$ be an abstraction between
  $\POTSObj = \left( \POTSX, \myLeq, \POTSA\right)$ and
  $\POTSObj' = \left( \POTSX', \myLeq, \POTSA'\right)$,
  $\POTSx \in \POTSX$ be an element, and
  $\POTSsched \in \closure{\POTSA^*}$ a schedule. If $\POTSsched$ is
  applicable at $\POTSx$, then $\POTSabA(\POTSsched)$ is applicable at
  $\POTSabX(\POTSx)$ and
  $\POTSabX(\POTSx \POTSstar \POTSsched) = \POTSabX(\POTSx) \POTSstar
  \POTSabA(\POTSsched)$.
\end{restatable}

% \begin{figure}[htbp]
%   \centering
%   \begin{tikzpicture}[scale=2]
%     \node (X0) at (0, 1) {$\POTSX$};
%     \node (X1) at (1, 1) {$\POTSX$};
%     \node (X2) at (2, 1) {$\cdots$};
%     \node (X3) at (3, 1) {$\POTSX$};
%     \node (X0') at (0, 0) {$\POTSX'$};
%     \node (X1') at (1, 0) {$\POTSX'$};
%     \node (X2') at (2, 0) {$\cdots$};
%     \node (X3') at (3, 0) {$\POTSX'$};

%     \draw[->] (X0) edge node[above] {$\POTSa_0$} (X1);
%     \draw[->] (X1) edge node[above] {$\POTSa_1$} (X2);
%     \draw[->] (X0') edge node[above] {$\POTSabA(\POTSa_0)$} (X1');
%     \draw[->] (X1') edge node[above] {$\POTSabA(\POTSa_1)$} (X2');

%     \draw[->] (X0) edge node[left] {$\POTSabX$} (X0');
%     \draw[->] (X1) edge node[left] {$\POTSabX$} (X1');
%     \draw[->] (X3) edge node[left] {$\POTSabX$} (X3');

%     \draw[->] (X0) edge[bend left] node[above] {$\POTSsched$} (X3);
%     \draw[->] (X0') edge[bend right] node[above] {$\POTSabA(\POTSsched)$} (X3');
%   \end{tikzpicture}
%   \caption{When $(\POTSabX,\POTSabA)$ is an abstraction, the above diagram commutes for any schedule $\POTSsched$, 
%     as stated in Proposition~\ref{prop: correction abstraction}.
%     }%
%   \label{fig:POTSabstractionstar}
% \end{figure}

The proof of this proposition % (postponed to
% Appendix~\ref{subsec:POTSComplements}) 
is by transfinite induction on
the length of schedules: showing that the result holds for finite
schedules is easy, and continuity arguments (such as Lemma~\ref{lemma:
  continuity star}) are then used to extend to infinite schedules.

% For any POTS $\POTSObj = \left( \POTSX, \myLeq, \POTSA\right)$, one
% can naturally define a POTS
% $\POTSObjseq = \left( \closure{\POTSX^*}, \pref, \POTSAseq \right)$
% over sequences of elements of $X$ where $\pref$ denotes the prefix
% ordering on $\closure{\POTSX^*}$. With each $\POTSa \in \POTSA$, one
% associates $\POTSaseq \in \POTSAseq$ whose domain comprises all
% \emph{finite} sequences $x_0, \dotsc, x_{T-1}$ such that
% $x_{T-1} \in \dom(\POTSa)$, defined by
% $\POTSaseq(x_0, \dotsc, x_{T-1}) = x_0, \dotsc, x_{T-1},
% \POTSa(x_{T-1})$.

% \begin{proposition}
%   The following functions define an abstraction between $\POTSObjseq$
%   and $\POTSObj$:
%   \begin{itemize}
%   \item $\POTSabXseq: \closure{\POTSX^*} \to \POTSX$ with
%     $\POTSabXseq\left( {(\POTSx_t)}_{t < T} \right) = \lub \left\{
%       \POTSx_t \middle\mid t < T \right\}$; and
%   \item $\POTSabAseq: \POTSAseq \to \POTSA$ with
%     $\POTSabAseq(\POTSaseq) = \POTSa$.
%   \end{itemize}

%   % There exists a morphism from $\POTSObjseq$ and $\POTSObj$ whose set
%   % abstraction is $\POTSabXseq: \closure{\POTSX^*} \to \POTSX$ with
%   % $\POTSabXseq\left( {(\POTSx_t)}_{t < T} \right) = \lub \left\{
%   %   \POTSx_t \middle\mid t < T \right\}$ and whose monoid abstraction
%   % $\POTSabAseq: \POTSAseq \to \POTSA$ sends any $\POTSaseq$ to the
%   % corresponding $\POTSa$.
% \end{proposition}
% \begin{proof}
%   \begin{itemize}
%   \item We first show that $\POTSabXseq$ is Scott-continuous. Observe
%     that $\POTSabXseq: \closure{\POTSX^*} \to \POTSX$ is
%     monotonous. Indeed, consider
%     ${(\POTSx_t)}_{t < T} \pref {(\POTSx_t)}_{t < T'} \in
%     \closure{\POTSX^*}$, with $T \leq T'$. Then,
%     $\{ x_t \mid t < T\} \subset \{ x_t \mid t < T'\}$ and,
%     $\POTSabXseq(\POTSXset) \myLeq \lub \POTSXset'$.

%     Therefore, for $\zeta \subset \closure{\POTSX^*}$ directed,
%     $\lub \POTSabXseq(\zeta) \myLeq \POTSabXseq(\lub \zeta)$.

%     Moreover, consider ${(\POTSx_t)}_{t < T} = \lub \zeta$ and set
%     $\POTSx = \POTSabXseq({(\POTSx_t)}_{t < T})$. Then for any
%     $u < T$, there exists ${(\POTSx_t)}_{t < T'} \in \zeta$ with
%     $u < T'$ and therefore,
%     $\POTSx_u \myLeq \POTSabXseq({(\POTSx_t)}_{t < T'}) \myLeq \lub
%     \POTSabXseq(\zeta)$.

%     Hence,
%     $\POTSabXseq(\lub \zeta) = \lub \{\POTSx_u \mid u < T \} \myLeq
%     \lub \POTSabXseq(\zeta)$.

%     Finally,
%     $\POTSabXseq {(\POTSx_t)}_{t < T} = \lub {(\POTSx_t)}_{t < T'}$
%     and $\POTSabXseq$ is Scott-continuous.
    
%   \item We now prove that $\POTSabAseq$ is a morphism monoid. Consider
%     ${(\POTSx_t)}_{t < T} \in \closure{\POTSX^*}$ and define
%     $\POTSx = \POTSabXseq({(\POTSx_t)}_{t < T})$. Consider
%     $\POTSa \in \POTSA$ and the corresponding
%     $\POTSaseq \in \POTSAseq$. Suppose that
%     ${(\POTSx_t)}_{t < T} \in \dom(\POTSaseq)$. In particular, this
%     implies $T < \infty$ and therefore $\POTSx = \POTSx_{T-1}$. Then
%     $\POTSabXseq\left({(\POTSx_t)}_{t < T} \POTSstar \POTSaseq \right)
%     = \POTSabXseq\left(x_0, \dotsc, x_{T-1}, \POTSa(x_{T-1})\right) =
%     \POTSa(x_{T-1}) = \POTSabXseq({(\POTSx_t)}_{t < T}) \POTSstar
%     \POTSabAseq(\POTSa)$.
%   \end{itemize}
% \end{proof}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
