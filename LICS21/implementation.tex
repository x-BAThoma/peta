\section{Guard Automata towards Practical Implementation}%
\label{sec:implem}
While Theorem~\ref{thm: carac reachable} suffices to verify
\emph{finite} LTA through the counter abstraction, it falls short at
capturing infinite models that arise for instance from round-based
algorithms. This section introduces guard automata as a finite-state
abstraction which is sound, yet, unsurprisingly, not complete in
general and may introduce spurious
counterexamples. %However, the transformation remains correct in the
%sense that properties verified by the abstract model
% will also hold on the original one.

\subsection{Cyclic LTA}%
\label{sec:implem/CLTA}

% The theory developed so far is purely theoretical and is not
% concerned with how the infinite object introduced are represented in
% memory. In order to propose some practical implementations, some
% additional assumptions needs to be made. Hence, the LTA used from here
% on will consists in a finite sequence of layers that is repeated
% indefinitely.

Towards algorithmic considerations and practical implementations, the
rest of the paper focuses on round-based distributed algorithms, which
can be captured by cyclic LTA\@. Intuitively, a cyclic LTA is used to
model an LTA that repeats a finite series of layers indefinitely. For
$\per \in \N_{>0}$, a \emph{$\per$-cyclic LTA} ($\per$-CLTA) is a
tuple $\CLTAObj = \left(\LTAR, \CLTAS, \CLTAguard\right)$ where:
\begin{itemize}
\item $\LTAR$ is a \emph{finite} set of parameters.
\item $\CLTAS$ is a \emph{finite} set of states partitioned into
  $\per$ layers
  $\CLTAS = \CLTASlay{0} \cup \dotsb \cup \CLTASlay{\per - 1}$.
\item $\CLTAguard: \CLTAS^2 \to \PA(\LTAR \cup \CLTAS)$ is a finite
  set of guards such that for $\lay < \per$,
  $\CLTAs \in \CLTASlay{\lay}$ and $\CLTAs' \in \CLTAS$,
  $\CLTAguard(\CLTAs, \CLTAs') \in \PA(\LTAR \cup \CLTASlay{\lay})$
  and if $\CLTAs' \notin \CLTASlay{\lay + 1 \bmod \per}$, then
  $\CLTAguard(\CLTAs, \CLTAs') = \false$.
\end{itemize}
Unfolding a $\per$-CLTA yields an infinite-state acyclic LTA
$\Uncycle\left(\LTAR, \CLTAS, \CLTAguard\right)$. Formally
$\Uncycle\left(\LTAR, \CLTAS, \CLTAguard\right)= \left(\LTAR, \LTAS,
  \LTAguard\right)$ with:
\begin{itemize}
\item
  $\LTAS = \{ (\CLTAs, \lay) \mid \lay \in \N, \CLTAs \in \CLTASlay{\lay
    \bmod \per}\}$
\item For $\lay \in \N$, $\CLTAs \in \CLTASlay{\lay \bmod \per}$ and
  $\CLTAs' \in \CLTASlay{\lay + 1 \bmod \per}$,
  $\LTAguard\left((\CLTAs, \lay), (\CLTAs', \lay+1)\right) =
  \CLTAguard(\CLTAs, \CLTAs')[\CLTAs'' \leftarrow (\CLTAs'', \lay)
  \text{ for } \CLTAs'' \in \CLTASlay{\lay \bmod \per}]$ meaning that
  any free variable $\CLTAs'' \in \CLTAS$ that appears in
  $\CLTAguard(\CLTAs, \CLTAs')$ gets replaced with $(\CLTAs'', \lay)$.
  In any other case, $\LTAguard$ is $\false$.
\end{itemize}

\subsection{Guard Automaton}

From the guard abstraction, one can construct a finite-state
automaton that represents the set of reachable configurations of a
cyclic LTA\@.

Let $\CLTAObj = \left(\LTAR, \CLTAS, \CLTAguard\right)$ be a
$\per$-CLTA equipped with a \emph{finite} set of guards expressed in
Presburger arithmetic:
$\CLTAG = \bigcup_{\lay < \per} \CLTAGlay{\lay}$ such that for
$\lay < \per$, $\CLTAGlay{\lay} \in \PA(\CLTASlay{\lay} \cup
\LTAR)$. In practice, $\CLTAG$ will include all guards appearing in
the LTA, as well as the events that need to be observed.

A CLTA can be unfolded into an infinite-state LTA, by concatenating
copies of $\CLTAObj$. %  When unfolding a CLTA, copies of the layers are
% produced, 
In order for the guard abstraction to be formally defined, copies of
the guards in $\CLTAG$ for each new layer are required.  For
$\lay \in \N$ a layer index and
$\CLTAg \in \CLTAGlay{\lay \bmod \per}$ a guard,
$\LTAGunfold_{\lay}(\CLTAg) = \CLTAg[\CLTAs \leftarrow (\CLTAs, \lay)
\text{ for } \CLTAs \in \CLTASlay{\lay \bmod \per}]$ denotes the guard
obtained by replacing every free occurrence of a variable
$\CLTAs \in \CLTASlay{\lay \bmod \per}$ in $\CLTAg$ by
$(\CLTAs, \lay)$. The converse folding operation is defined by:
$\LTAGfold_{\lay}(\LTAg) = \LTAg[(\CLTAs, \lay) \leftarrow \CLTAs,
\text{ for } \CLTAs \in \CLTASlay{\lay \bmod \per}]$. Finally,
$\LTAG _{\lay}= \LTAGunfold\left(\CLTAGlay{\lay \bmod \per}\right)$ is
the set of guards at layer $\lay$ and
$\LTAG = \bigcup_{\lay \in \N} \LTAG_{\lay}$ the set of all guards.

The guard abstraction maps every configuration of $\Uncycle(\CLTAObj)$
to a set of guards that hold in that configuration. Formally,
$\LTAeval_{\LTAG}: \LTAC \to 2^{\LTAG}$. A set of guards
$\LTAgamma \in 2^{\LTAG}$ can be represented with the sequence
$\LTAgamma_0 \LTAgamma_1 \dotso$, where for $\lay \in \N$,
$\gamma_{\lay} = \gamma \cap \LTAG_{\lay}$. $\LTAGfold(\LTAgamma)$
then denotes the sequence
$\LTAGfold_0(\LTAgamma_0) \concat \LTAGfold_1(\LTAgamma_1) \concat
\dotsb \in {\left(2^{\CLTAG}\right)}^{\omega}$ and $\LTAGunfold$ is
the converse operation that applies $\LTAGunfold_{\lay}$ to the
elements of layer $\lay$ in the sequence. Doing so, a configuration
$\LTAc \in \LTAC$ defines a (possibly infinite) word
$\CLTAgammalay{0} \CLTAgammalay{1} \dotso$ over the \emph{finite}
alphabet $\Sigma =  \bigcup_{\lay < \per} 2^{\CLTAGlay{\lay}}$ as represented in
Figure~\ref{fig:ConfigToWord}.
\begin{figure}[htbp]
  \centering
  \resizebox{.9\textwidth}{!}{
    \begin{tikzpicture}
      \node (LTAC) at (0, 0.5) {
        \(
        \LTAc \in \LTAC
        \)
      };

      \node (LTAG) at (6, 0) {
        \(
        \begin{aligned}
          \LTAgamma_0 &\subset \LTAG_0 \\
          \LTAgamma_1 &\subset \LTAG_1 \\
          \LTAgamma_2 &\subset \LTAG_2 \\
          &\vdots \\
          \LTAgamma_{\per - 1} &\subset \LTAG_{\per - 1} \\
          \LTAgamma_{\per} &\subset \LTAG_{\per} \\
          \LTAgamma_{\per + 1} &\subset \LTAG_{\per + 1} \\
          &\vdots
        \end{aligned}
        \)
      };

      \node (CLTAG) at (14, 0) {
        \(
        \begin{aligned}
          \CLTAgammalay{0} &\subset \CLTAGlay{0} \\
          \CLTAgammalay{1} &\subset \CLTAGlay{1} \\
          \CLTAgammalay{2} &\subset \CLTAGlay{2} \\
          &\vdots \\
          \CLTAgammalay{\per - 1} &\subset \CLTAGlay{\per - 1} \\
          \CLTAgammalay{\per} &\subset \CLTAGlay{0} \\
          \CLTAgammalay{\per + 1} &\subset \CLTAGlay{1} \\
          &\vdots
        \end{aligned}
        \)
      };

      \path[->, thick] (LTAC.east) edge node[above] {
          $\LTAeval_{\LTAG}: \LTAC \to 2^{\LTAG} \approx \prod_{\lay \in \N} 2^{\LTAG_\lay}$
      }
      (LTAC.east -| LTAG.west);

      \path[->, thick] (LTAG.30) edge node[above] {
          $\LTAGfold: \prod_{\lay \in \N} 2^{\LTAG_\lay} \to \prod_{\lay \in \N} 2^{\CLTAGlay{\lay \bmod \per}}$
      }
      (LTAG.30 -| CLTAG.west);

      \path[->, thick] (CLTAG.180) edge node[below] {
          $\LTAGunfold: \prod_{\lay \in \N} 2^{\CLTAGlay{\lay \bmod \per}} \to \prod_{\lay \in \N} 2^{\LTAG_\lay}$
      }
      (CLTAG.180 -| LTAG.east);
    \end{tikzpicture}
  }%
  \caption{From a configuration to a word over the finite alphabet of the guard automaton.}%
  \label{fig:ConfigToWord}
\end{figure}
% Let us then define $\Sigma = \bigcup_{\lay < \per} 2^{\CLTAGlay{\lay}}$.

For $\lay < \per$ a layer index, $\CLTAgamma \in 2^{\CLTAGlay{\lay}}$
and $\CLTAgamma' \in 2^{\CLTAGlay{\lay + 1 \bmod \per}}$ guard
valuations of layer $\ell$ and the next layer, one can use an SMT
solver to check whether $\CLTAgamma'$ is a successor
$\CLTAgamma$. Precisely, the SMT query asks for the existence of
$x \in \N^{\CLTASlay{\lay}}$,
$y \in \N^{\CLTASlay{\lay + 1 \bmod \per}}$ and
$e \in \N^{\CLTASlay{\lay} \times \CLTASlay{\lay + 1 \bmod \N}}$ such
that the valuation of guards~\eqref{eq:flowCond}, flow
condition~\eqref{eq:flowCond2} and counter coherence
\eqref{eq:counterCohehrence} are verified.
\begin{equation}\label{eq:flowCond}
    x \models \bigwedge_{\CLTAg \in \CLTAgamma} \CLTAg \wedge
    \bigwedge_{\CLTAg \in \CLTAGlay{\lay} \setminus \CLTAgamma} \neg
    \CLTAg \quad \quad \quad 
    y \models \bigwedge_{\CLTAg' \in \CLTAgamma'} \CLTAg' \wedge
    \bigwedge_{\CLTAg' \in \CLTAGlay{\lay + 1 \bmod \per} \setminus
      \CLTAgamma'} \neg \CLTAg'
  \end{equation}
  \vspace{-.5cm}
  \begin{equation}\label{eq:flowCond2}
    e, x \models \bigwedge_{\CLTAs \in \CLTASlay{\lay}} \CLTAs \geq
    \sum_{\CLTAs' \in \CLTASlay{\lay + 1 \bmod \per}}
    \left[\CLTAs, \CLTAs'\right]\quad \quad \quad  e, y \models \bigwedge_{\CLTAs' \in \CLTASlay{\lay+1 \bmod \per}} \sum_{\CLTAs
      \in \CLTASlay{\lay}} \left[\CLTAs, \CLTAs'\right] = \CLTAs'
  \end{equation}
    \vspace{-.5cm} 
\begin{equation}\label{eq:counterCohehrence}
  e, x \models \bigwedge_{(\CLTAs, \CLTAs') \in \CLTASlay{\lay}
    \times \CLTASlay{\lay + 1 \bmod
      \N}} \left[\CLTAs, \CLTAs'\right] > 0 \implies \CLTAguard(\CLTAs, \CLTAs')\\
\end{equation}

The guard automaton is a finite automaton whose language
\emph{overapproximates} the set of reachable configurations. It bears
similarities with de Bruijn graphs~\cite{debruijn46} used \emph{e.g.}
in bioinformatics. If
$\GAE_{\lay} \subset 2^{\CLTAGlay{\lay}} \times 2^{\CLTAGlay{\lay + 1
    \bmod \per}}$ denotes the set of all pairs
$\CLTAgamma, \CLTAgamma'$ that verify conditions \eqref{eq:flowCond}
and and \eqref{eq:counterCohehrence}, one can build the set
$\GAE = \bigcup_{\lay < \per} \GAE_{\lay}$.

\begin{definition}
The \emph{guard automaton}
%\footnote{The guard automaton is in fact a subgraph of the de Bruijn
% graph of order $2$.}
of $\CLTAObj$ is
$\GuardAutomaton(\CLTAObj) = \left(\Sigma, \GAE, 2^{\CLTAGlay{0}},
  \GAsrc, \GAdest, \GAlabel\right)$ where:
\begin{itemize}
\item $\Sigma$ is both the alphabet and the set of states.
\item $2^{\CLTAGlay{0}} \subset \Sigma$ is the set of initial states.
\item $\GAE \subset \Sigma^2$ defined above is the set of edges,
  equipped with $\GAsrc: \GAE \to \Sigma$ (resp.
  $\GAdest: \GAE \to \Sigma$) that defines the \emph{source state}
  (resp. \emph{destination state}) of every edge, and
  $\GAlabel: \GAE \to \Sigma$ associates a \emph{label} to each edge
  defined by $\GAlabel(\CLTAgamma, \CLTAgamma') = \CLTAgamma$.  
% \item $\GAsrc: \GAE \to \Sigma$ associates a \emph{source state} to
%   each edges, it is defined as the projection
%   $\GAsrc(\CLTAgamma, \CLTAgamma') = \CLTAgamma$ ($\Sigma$ is seen as
%   the set of states here).
% \item $\GAdest: \GAE \to \Sigma$ associates a \emph{destination state}
%   to each edges,it is defined as the projection
%   $\GAdest(\CLTAgamma, \CLTAgamma') = \CLTAgamma'$.
% \item $\GAlabel: \GAE \to \Sigma$ associates a \emph{label} to each
%   edge (this time, $\Sigma$ is seen as the alphabet), it is defined as
%   the projection: $\GAlabel(\CLTAgamma, \CLTAgamma') = \CLTAgamma$.
\end{itemize}  
\end{definition}
An infinite run ${\left( \GAe_{\lay} \right)}_{\lay < \infty}$ of the
guard automaton defines a word
$\GAword\left( {\left( \GAe_{\lay} \right)}_{\lay < \infty} \right) =
\GAlabel(\GAe_0) \concat \GAlabel(\GAe_1) \concat \dotsb$, and
$\GAlang(\GuardAutomaton(\CLTAObj)) \subset \Sigma^\omega$ denotes the
language of $\GuardAutomaton(\CLTAObj)$.  
% Given an infinite run ${\left( \GAe_{\lay} \right)}_{\lay < \infty}$
% of the Guard Automaton (meaning that
% $\GAsrc(\GAe_{0}) \in 2^{\CLTAGlay{0}}$ and $\forall \lay \in \N$,
% $\GAdest(\GAe_{\lay}) = \GAsrc(\GAe_{\lay + 1})$), a word is defined
% with
% $\GAword\left( {\left( \GAe_{\lay} \right)}_{\lay < \infty} \right) =
% \GAsrc(\GAe_0) \concat \GAlabel(\GAe_0) \concat \GAlabel(\GAe_1)
% \concat \dotsb$. The language of infinite words
% $\GAlang(\GuardAutomaton(\CLTAObj)) \subset \Sigma^\omega$ then
% denotes the language of all such infinite words generated by
% $\GuardAutomaton(\CLTAObj)$. Then the following theorem holds:


\begin{example}
  Algorithm~\ref{alg:PhaseKing} can be described by the following CLTA
  with $\per=1$. The parameters are $\LTAR = \{n, t, f\}$ where
  % $n$ and $t$ corresponds to the actual parameters of the and
  $f$ denotes the actual number of Byzantine faults. % This algorithm
  % will have $\per = 1$, the set of 
  States are $\CLTAS = \{v_0, k_0, v_1, k_1\}$. The guards here only
  depend on the next value of $v$. For instance:
  \begin{multline*}
    \DTSguard(\_, v_0) = (v_0 + k_0 + v_1 + k_1 + f = n) \\
    \wedge \Bigl( \bigl(2(v_0 + k_0 + f) > n + 2t\bigr) \vee
    \Bigl((2v_0 + 2k_0 \leq n+2t) \wedge (2v_1 + 2k_1 \leq n+2t)
    \wedge (k_1 = 0) \Bigr)\Bigr).
  \end{multline*}
  Also, $\DTSguard(\_, k_0) = \DTSguard(\_, v_0)$ and
  $ \DTSguard(\_, v_1) = \DTSguard(\_, k_1)$ is defined symmetrically.
  % \[
  %   \DTSguard(\_, v_0) = (v_0 + k_0 + v_1 + k_1 + f = n) \quad \wedge
  %   (A \vee (B \wedge C \wedge D))
  % \]
  % where $A \equiv 2(v_0 + k_0 + f) > n + 2t$ (line 8),
  % $B \equiv 2v_0 + 2k_0 \leq n+2t$ (else branch of line 7)
  % $C \equiv 2v_1 + 2k_1 \leq n+2t$ (else branch of line 9) and
  % $D \equiv k_1 = 0$ (King chooses 1 or Byzantine king).
  % \begin{align*}
  %   \DTSguard(\_, v_0) &: (v_0 + k_0 + v_1 + k_1 + f = n) 
  %   \quad \wedge (A \vee (B \wedge C \wedge D)) &\text{with:} \\
  %   A &: 2(v_0 + k_0 + f) > n + 2t & \text{line 8} \\
  %   B &: 2v_0 + 2k_0 \leq n+2t & \text{else branch of line 7}\\
  %   C &: 2v_1 + 2k_1 \leq n+2t & \text{else branch of line 9} \\
  %   D &: k_1 = 0 & \text{either king chooses 1 or byzantine king}
  % \end{align*}
%
%  Then, 

  A configuration $\LTAc$ of the unfolded LTA is depicted bottom-right
  of Figure~\ref{fig:SuccessiveAbstractions}, where the array contains
  the valuation $\LTAkappa(\LTAc)$ and the arrows represent the
  flow. For example $\LTAkappa(\LTAc)(v_1, 0) = 2$,
  $\LTAflow(\LTAc)((v_0, 0), (k_1, 1)) = 1$ and
  $\LTAflow(\LTAc)((v_0, 0), (v_0, 1)) = 0$.
  
  The guard abstraction transforms $\LTAc$ into the guard
  configuration bottom-left of
  Figure~\ref{fig:SuccessiveAbstractions}. Here, we chose the set of
  guards $\CLTAG$ to consist of $\LTAs > 0$ for each
  $\LTAs \in \CLTAS$ and of the guards of the
  LTA. %This is only an heuristic choice,
  %other possibilities may be considered.
  The alphabet $\Sigma$ contains \emph{e.g.},
  $(T\cdot T\cdot\cdot\cdot\cdot\cdot T)$. SMT queries determine
  whether two letters may appear successively, in order to build the
  guard automaton.
  % This only
  % requires modeling two layers of a configuration and is therefore
  % finite query.
  For instance, according to the first two layers of
  $\LTAeval_{\LTAG}(\LTAc)$, $(T\cdot T\cdot\cdot\cdot\cdot\cdot T)$
  can be followed by $(T\cdot TT\cdot\cdot\cdot\cdot T)$. There will
  therefore be a transition between these two states in the guard
  automaton.

  
  
  % Put together, these conditions can help build an \emph{automaton}
  % whose infinite words represents guards configurations.
  % An interesting liveness property is the following:
  % \begin{align*}
  %   \varphi &= \left(\Sigma^{\omega} \setminus \mathrm{fair}\right) \cup \mathrm{live} & \text{with:} \\
  %   \mathrm{fair} &= \Sigma^* \concat \left[ k_0 > 0 \vee k_1 > 0\right] \concat \Sigma^{\N} &\text{King is correct at a layer}\\
  %           & \quad \cap {\left[k_0 + k_1 \leq 1\right]}^\N & \text{ at most one king per layer} \\
  %           &\quad \cap {\left[v_0 + k_0 + v_1 + k_1 = n\right]}^{\omega} &\text{all process move through every layer} \\
  %   \mathrm{live} &= \Sigma^* \concat {[v_0 = 0 \wedge k_0 = 0]}^\omega &\text{decide 1}\\
  %           & \quad \cup \Sigma^* \concat {[v_1 = 0 \wedge k_1 = 0]}^\omega &\text{decide 0}
  % \end{align*}
  % By checking that the language of the guard automaton is included in
  % $\varphi$, one can check the corresponding liveness properties.
\end{example}

\begin{restatable}{theorem}{thmGAcorrect}\label{thm: GA correctness}
  Let $\LTAc \in \LTAC$ be a configuration of $\Uncycle(\CLTAObj)$ and
  $\LTAeval_{\LTAG}(\LTAc) \in 2^{\LTAG}$ its guard abstraction. If
  $\LTAc$ is counter-coherent, then
  $\LTAGfold\left(\LTAeval_{\LTAG}(\LTAc)\right) \in
  \GAlang(\GuardAutomaton(\CLTAObj))$.
\end{restatable}
By soundness of the guard automaton construction, a property which
holds on configurations that correspond to runs of
$\GuardAutomaton(\CLTAObj)$ also holds on the configurations of
$\Uncycle(\CLTAObj)$. A simple verification procedure thus consists in
checking that $\GAlang(\GuardAutomaton(\CLTAObj))$ is included in a
given language of correct configurations. At a first glance, it might
seem that only safety properties can be checked. However, the guard
automaton also represents configurations reachable by infinite
schedules, making the verification of liveness properties feasible.

\begin{figure}[htbp]
  \begin{subfigure}{0.47\textwidth}
    \centering
    \begin{tikzpicture}[xscale=0.8, yscale=0.7,
      state/.style={draw, minimum width=1cm, minimum height=0.6cm}]
      \node[state] (v0) at (0, 1) {$v_0$};
      \node[state] (v1) at (0, -1) {$v_1$};

      \node[state] (k00) at (3, 2.5) {$k_{0, 0}$};
      \node[state] (k01) at (3, 1.5) {$k_{0, 1}$};
      \node[state] (p0) at (3, 0.5) {$p_0$};
      \node[state] (p1) at (3, -0.5) {$p_1$};
      \node[state] (k10) at (3, -1.5) {$k_{1, 0}$};
      \node[state] (k11) at (3, -2.5) {$k_{1, 1}$};

      \node[state] (vp0) at (6, 1) {$v_0$};
      \node[state] (vp1) at (6, -1) {$v_1$};

      \draw (v0.east) -- (2, 1);
      \draw (2, 2.5) -- (2, 0.5);
      \draw[->] (2, 2.5) -- (k00.west);
      \draw[->] (2, 1.5) -- (k01.west);
      \draw[->] (2, 0.5) -- (p0.west);

      \draw (v1.east) -- (2, -1);
      \draw (2, -2.5) -- (2, -0.5);
      \draw[->] (2, -0.5) -- (p1.west);
      \draw[->] (2, -1.5) -- (k10.west);
      \draw[->] (2, -2.5) -- (k11.west);


      \draw (k00.east) -- (4, 2.5);
      \draw (k01.east) -- (4, 1.5);
      \draw (p0.east) -- (4, 0.5);
      \draw (p1.east) -- (4, -0.5);
      \draw (k10.east) -- (4, -1.5);
      \draw (k11.east) -- (4, -2.5);
      \draw (4, -2.5) -- (4, 2.5);
      \draw[->] (4, 1) -- (vp0.west);
      \draw[->] (4, -1) -- (vp1.west);
    \end{tikzpicture}
  \end{subfigure}
  \begin{subfigure}{0.47\textwidth}\centering
    $\DTSP = \{n, t, f\}$ and for $x, y \in \{0, 1\}$:
    \begin{align*}
      \LTAguard(v_x, p_x) &= \true \\
      \LTAguard(v_x, k_{x, y}) &= \left[ 2(v_y + f) \geq n \right] \\
      \LTAguard(\_, v_x) &=\left[ 2(p_x + k_{x, 0} + k_{x, 1}) > n + 2t \right] \vee \\
                          & \quad \left[
                            \begin{aligned}
                              2(p_0 + k_{0, 0} + k_{0, 1}) &\leq n+2t \wedge \\
                              2(p_1 + k_{1, 0} + k_{1, 1}) &\leq n+2t \wedge \\
                              k_{0, x} + k_{1, x} &= 0
                            \end{aligned}
                                                    \right]
    \end{align*}
  \end{subfigure}%
  \caption{\label{fig:LTA real PhaseKing} A 2-CLTA for the Phase King
    algorithm with non-deterministic choice of the
    king. %For $x, y \in \{0, 1\}$,
    A process in  $k_{x, y}$ is king of the current round, its
    current value is $x$ and it thinks the majority is $y$.}
\end{figure}
\begin{example}
  For presentation purposes, Algorithm~\ref{alg:PhaseKing} is an
  overly simplified version of the Phase King
  algorithm~\cite{BG-mst93}. The latter can be faithfully encoded by
  the 2-CLTA $\CLTAObj$ of Figure~\ref{fig:LTA real PhaseKing}, where
  the updated value when there is no clear majority is not the king's
  value, but rather the majority of the values received by the
  king. Each round consists of two layers of communication, a first in
  which each process broadcasts its value, and a second in which the
  king broadcasts what it thinks is the majority. The set of guards at
  the first layer is $\CLTAGlay{0} = \left\{v_0 > 0, v_1 > 0\right\}$
  and at the second layer $\CLTAGlay{1}$ consists of
  $k_{0, 0} {+} k_{1, 0} > 0$,\, $k_{0, 1} {+} k_{1, 1} > 0$,\,
  $p_0 {+} k_{0, 0} {+} k_{0, 1} > 0$,\,
  $p_1 {+} k_{1, 0} {+} k_{1, 1} > 0$,\,
  $2(k_{0, 0} {+} k_{0, 1} {+} p_0 {+} f) > n {+} 2t$ and
  $2(k_{1, 0} {+} k_{1, 1} {+} p_1 {+} f) > n {+} 2t$.
  % \begin{align*}
  %   \CLTAGlay{1} &= \left\{
  %              \begin{aligned}
                 % k_{0, 0} + k_{1, 0} &> 0 \\
                 % k_{0, 1} + k_{1, 1} &> 0 \\
                 % p_0 + k_{0, 0} + k_{0, 1} &> 0 \\
                 % p_1 + k_{1, 0} + k_{1, 1} &> 0 \\
                 % 2(k_{0, 0} + k_{0, 1} + p_0 + f) &> n + 2t \\
                 % 2(k_{1, 0} + k_{1, 1} + p_1 + f) &> n + 2t \\
  %              \end{aligned}
  %              \right\}
  % \end{align*}

  Restricting to valuations with
  $\sum_{\LTAs \in \LTAS_{\lay}} \LTAs {+} f = n$ (fairness) and
  $k_{0, 0} {+} k_{0, 1} {+} k_{1, 0} {+} k_{1, 1} \leq 1$ (at most
  one king), the resulting guard automaton has 3~states in even layers
  and 11~in odd layers. Writing $[\mathit{formula}]$ for the set of
  letters in $2^{\CLTAG}$ for which $\mathit{formula}$ holds, one can
  show:
  \begin{align}
    \GAlang(\GuardAutomaton(\CLTAObj)) &\subset {[\neg(k_{0, 0} + k_{1, 0} > 0) \wedge \neg(k_{0, 1} + k_{1, 1} > 0)]}^{\omega}\label{align: fairness} \\
                                       & \quad \cup \Sigma^* [(k_{0, 0} + k_{1, 0} > 0) \vee (k_{0, 1} + k_{1, 1} > 0)] {[\neg (p_0 + k_{0, 0} + k_{0, 1} > 0)]}^\omega \label{align: case 0}\\
                                         & \quad \cup \Sigma^* [(k_{0, 0} + k_{1, 0} > 0) \vee (k_{0, 1} + k_{1, 1} > 0)] {[\neg (p_1 + k_{1, 0} + k_{1, 1} > 0)]}^\omega \enspace. \label{align: case 1}
  \end{align}
  Therefore, either every chosen king is Byzantine~(\ref{align:
    fairness}), or all processes agree on a value after a
  non-Byzantine king is chosen~(\ref{align: case 0} or~\ref{align:
    case 1}).
\end{example}


In general, although is it sound, the guard automaton construction is
not complete: the language may contain words that correspond to no
configuration of the LTA\@. As usual for incomplete methods,
heuristics can be used to remove some spurious counterexamples. % More
% details on incompleteness and refinements can be found in
% Appendix~\ref{app: implem}.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End: