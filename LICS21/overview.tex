\section{Overview}

\nb{here give an overview of the contribution on an example (reliable
  broadcast for finite case, and maybe phase-King or phase-Queen for
infinite case)}

\subsection{Finite case --- Reliable broadcast}

\paragraph*{Layered threshold automata}
Examples of threshold automata are given in
Figures~\ref{fig:reliable-bc} and~\ref{fig:reliable-bc-layered}. They
ressemble standard automata, yet the edges are labelled with
constraints that relate the number of processes in states and the
values of the parameters. For instance, $v_1 \geq t{+}1{-}f$ expresses
that the number of processes in state $v_1$ is larger than or equal to
a threshold, where $t$ is the maximal number of faulty processes and
$f$ the actual number of faulty processes in the current execution.

Both threshold automata from Figures~\ref{fig:reliable-bc}
and~\ref{fig:reliable-bc-layered} model the reliable broadcast
algorithm. Initially, each process has value $0$ or $1$, represented
by the states $v_0$ and $v_1$ being initial states. A process can
leave $v_0$ if the guard $v_1 \geq t{+}1{-}f$ evaluates to true. The
two threshold automata however differ because the first one is not
layered, whereas the second one is. The states of a layered threshold
automata can be partionned into layers so that every edge move from
one layer to the next. Our methodology exploits the layered
assumption. \nb{put a forward ref to explanation why layers are
  needed}
\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}
    \tikzstyle{rond5}=[draw,circle,minimum size=5mm,inner sep=1pt]
%      \path[use as bounding box] (0,.8) -- (5,-1.3);
    \draw (0,0) node[rond5] (v0) {$\vphantom{tg}{v_0}$};
    \draw[-latex] (0,-.7) -- (v0);
    \draw (2.5,0) node[rond5] (v1) {} node {$\vphantom{tg}{v_1}$};
    \draw[-latex] (2.5,-.7) -- (v1);
    \draw (5,0) node[rond5] (acc) {} node {$\vphantom{tg}\mathsf{acc}$};
    \draw[-latex'] (v0) -- (v1) node[sloped,above,midway] {$v_1 \geq t{+}1{-}f$};
    \draw[-latex'] (v1) -- (acc) node[sloped,below,midway] {$v_1 \geq n{-}t{-}f$};
\end{tikzpicture}  
  \caption{Threshold automaton for reliable broadcast.}
  \label{fig:reliable-bc}
\end{figure}


\begin{figure}[htbp]
  \centering
  \begin{tikzpicture}
    \tikzstyle{rond5}=[draw,circle,minimum size=5mm,inner sep=1pt]
%      \path[use as bounding box] (0,.8) -- (5,-1.3);
    \draw (0,1) node[rond5] (v0) {$\vphantom{tg}v_0$};
    \draw[-latex] (0,.3) -- (v0);
    \draw (0,-1) node[rond5] (v1) {$\vphantom{tg}v_1$};
    \draw[-latex] (0,-1.7) -- (v1);
    \draw (2.5,0) node[rond5] (e1) {} node {$\vphantom{tg}s_1$};
    \draw (5,0) node[rond5] (acc) {} node {$\vphantom{tg}\mathsf{acc}$};
    \draw[-latex'] (v0) -- (e1) node[sloped,above,midway] {$v_1 \geq t{+}1{-}f$};
    \draw[-latex'] (v1) -- (e1);
    \draw[-latex'] (e1) -- (acc) node[sloped,below,midway] {$s_1 \geq n{-}t{-}f$};
\end{tikzpicture}  
  \caption{Layered threshold automaton for reliable broadcast.}
  \label{fig:reliable-bc-layered}
\end{figure}

\paragraph*{Configurations}
\nb{explain that a ``configuration'' of the counter system contains
  the number of processes in each state \emph{at each layer}; thus it
  is a compact representation of an execution (without order of
  transitions though), not only of its last configuration}

\paragraph*{Formulas}
We use formulas to describe several types of constraints. These
formulas use variables for each state ($\statevar{s}$ for state $s$)
as well as for pairs of states ($\edgevar{s}{s'}$ for states $s$ to
$s'$ if $\DTSguard{s,s'} \neq \mathsf{false}$).

First, we write \emph{flow constraints} $\phi_{\mathsf{flow}}$ expressing that (1) the number
of processes in a state is bounded by the number of processes along
edges that leave that state, and (2) the number of processes in a
state is equal to the number of processes along edges that enter that
state. On the layered reliable broadcast example (see
Figure~\ref{fig:reliable-bc-layered}), the flow equations are the
following: $\edgevar{v_0}{s_1} \leq \statevar{v_0}$,
$\edgevar{v_1}{s_1} \leq \statevar{v_1}$ and
$\edgevar{s_1}{\mathsf{acc}}$ for the first kind and
$\statevar{s_1} = \edgevar{v_0}{s_1} + \edgevar{v_1}{s_1}$ and
$\statevar{\mathsf{acc}}= \edgevar{s_1}{\mathsf{acc}}$ for the second
kind.

Second, we write \emph{fairness constraints} $\phi_{\mathsf{fair}}$ expressing that enabled
edges cannot be avoided forever. More precisely, for every edge, if
its guard evaluates to true, then the number of processes in its
source state is equal to the number of processes along edges that
leave this source state. Observe that for these formulas, the guards
we use do not involve faulty processes. On our running example
$\statevar{v_1} \geq t{+}1 \implies \statevar{v_0} =
\edgevar{v_0}{s_1}$,
$\statevar{s_1} \geq n{-}t \implies \statevar{s_1}
=\edgevar{s_1}{\mathsf{acc}}$, and
$\statevar{v_1} = \edgevar{v_1}{s_1}$.

Third, we write \emph{enabledness constraints} $\phi_{\mathsf{enab}}$expressing that, in
order to be fired, and edge must be enabled. Formally, if the number
of processes along an edge is positive, then the guard is
satisfied. For the layered reliable broadcast, this translates as:
$\edgevar{v_0}{s_1} >0 \implies \statevar{v_1} \geq t{+}1{-}f$ and
$\edgevar{s_1}{\mathsf{acc}} >0 \implies \statevar{s_1} \geq
n{-}t{-}f$.

Then, we write \emph{initial constraints} $\phi_{\mathsf{init}}$ to represent how the
algorithm is initialized. On our example,
$\statevar{v_0}+\statevar{v_1}=n$ means that all correct processes
start in state $v_0$ or $v_1$.

At this point, we thus have
$\phi_{\mathsf{sys}} = \phi_{\mathsf{flow}} \wedge
\phi_{\mathsf{fair}} \wedge \phi_{\mathsf{enab}} \wedge
\phi_{\mathsf{init}}$ that describes constraints on executions of the
asynchronous distributed algorithm. One can check whether the formula
$\phi_{\mathsf{sys}}$ is satisfiable, that is, whether the distributed
system admits an execution. 

Last but not least, we write \emph{specification constraints}
corresponding to the properties we wish to prove. More precisely, we
write formulas that encode the negation of the properties of
interest. For instance, on the layered reliable broadcast,
$\statevar{v_1}=0 \wedge \statevar{\mathsf{acc}} >0$ expresses that no
process starts in $v_1$ and yet some process accepts, which is the
negation of validity. For each property $\mathsf{prop}$, if
$\phi_{\mathsf{prop}}$ denotes the corresponding constraint, it
suffices to check that $\phi_{\mathsf{prop}}$ is satisfiable and
$\phi_{\mathsf{sys}} \wedge \phi_{\mathsf{prop}}$ is unsatisfiable, in
order to prove that $\mathsf{prop}$ holds on all executions of the
system.


\subsection{Infinite case --- Phase-king algorithm}

The Phase-king algorithm is a synchronous consensus algorithm tolerant
to Byzantine faults under the resilience condition
$n > 3t \wedge t \geq f$~\cite{BGP-focs89}. It proceeds in rounds, and
in each round one process acts as the King. Since the number of rounds
before termination is not \emph{a priori} bounded, and typically
depends on the parameters valuation, the layered threshold automaton
representing it must have a denumerable number of states. Namely,
there are four states for each layer~$r$: $k_0^r$ and $k_1^r$ to
encode that a process is King and its value is $0$ or $1$,
respectively, and $v_0^r$, $v_1^r$ encoding that a process is not
King, and its value is $0$ or $1$, respectively. The layered
threshold automaton  is defined by the following guards:
\begin{itemize}
\item
  $\DTSguard(\_^r,v_0^{r{+}1}) = \Bigl[ \bigl(v_0^r + k_0^r + f \geq
  \frac{n}{2} + t\bigr) \vee \bigl(k_0^r + k_1^r =0 \wedge v_1^r +
  k_1^r < \frac{n}{2} +t \bigr) \vee \bigl(k_0^r \geq 1\bigr) \Bigr]
  \wedge \sum_{s} s^r = n{-}f$;
\item $\DTSguard(\_^r,v_1^{r{+}1})$ is symmetric;
\item $\DTSguard(\_^r,k_0^{r{+}1}) = \DTSguard(\_^r,v_0^{r{+}1})$;
\item  $\DTSguard(\_^r,k_1^{r{+}1}) = \DTSguard(\_^r,v_1^{r{+}1})$;
  \item otherwise $\DTSguard(\_^{r},\_^{r'})=\mathsf{false}$
\end{itemize}



\begin{table}
  \begin{center}
    \caption{Contraints for the Phase-king algorithm}
    \label{tab:PK-formulas}

    \begin{tabular}{ll}
      \tbh{Type} & \tbh{Formula}  \\
      Init & $\sum_{s} s^0 = n$\\
            \hline
      Flow  & $\statevar{s^r} \leq \sum_{t}\edgevar{s^r}{t^{r{+}1}}$\\
       & $\statevar{t^{r+1}} = \sum_{s} \edgevar{s^r}{t^{r{+}1}}$\\
      \hline
      Fairness & \\
      \hline
      King assumption 1 & $\forall r, K_0^r + K_1^r \leq 1$\\
      King assumption 2 & $\exists r, K_0^r + K_1^r \geq 1$\\
            \hline
      Agreement & \\
      Validity & \\
      Termination & \\
    \end{tabular}
  \end{center}
\end{table}
Although our focus is asynchronous algorithms, synchronous algorithms
such as Phase-king, can be handled: it suffices to encode synchrony as
a constraint that no process can move to layer~$r{+}1$ unless all
correct processes are in layer~$r$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
