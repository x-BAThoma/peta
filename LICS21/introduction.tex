\section{Introduction}%
\label{sec:intro}
Under the umbrella of \emph{parameterized verification}, the
verification of systems formed of an arbitrary number of agents
executing the same code, has attracted quite some attention in the
recent years, see for instance~\cite{Esparza-stacs14,BJKKRVW-book15}.
Application examples range from distributed algorithms (\emph{e.g.},
for clock synchronization~\cite{ST-tacas17} or robot
coordination~\cite{SSPT-fmcad17}), cache-coherence
protocols~\cite{MM-charme01,AAGRZ-fmcad15}, to chemical or biological
systems~\cite{BH-ic15}. In all cases, the systems are designed to
operate correctly independently of the number of agents.

More specifically, \emph{distributed algorithms} are central to
various emblematic applications, including telecommunications,
scientific computing, and Blockchain. Automatically proving the
correctness of distributed algorithms is a particularly relevant, as
stated by Lamport: ``Model-checking algorithms prior to submitting
them for publication should become the
norm''~\cite{Lamport-disc06}. The task, that the verification
community has started to address, is quite challenging, since it aims
at validating at once all instances of the algorithm for arbitrarily
many processes.




Distributed algorithms with \emph{threshold guards} are omni-present
in solutions for consensus and agreement problems.  Typically, these
guards also are parameterized, \emph{e.g.}, if the number of processes
in a distributed system is $n$, then it is natural to require that
certain actions are taken only if a majority of processes is ready to
do so; this results in a parameterized threshold expression of $n/2$.
Due to Blockchain and other current applications these kinds of
distributed algorithm enjoy recent attention from the algorithm design
community as well as the verification community.  the algorithm design
community has been studying them for a long time, (see \emph{e.g.},
\cite{BrachaT85}) and typically provides hand-written proofs based on
mathematical models without formal semantics.

For computer-aided verification the first challenge is to develop
appropriate modeling formalisms that maintain all behaviors of the
original algorithms on the one hand, and on the other hand are
abstract and succinct to allow for efficient verification.  Several
approaches towards efficient verification have recently been proposed.


The threshold automata framework~\cite{KVW-ic17} targets asynchronous
distributed algorithms with threshold guards and reductions (similar
to~\cite{Lipton75,EF-scp82}) have been used to show that SMT-based
bounded model checking is complete~\cite{KLVW17:POPL}.  Later this
framework was generalized and generalizations were analyzed regarding
decidability~\cite{KKW18:concur}, and complexity
\cite{BEL:atva20}. The current paper also targets threshold
distributed algorithms, yet eventually provides an even coarser
abstraction to represent their behaviors, thus reducing the overall
verification complexity. Moreover, the semantics of distributed
algorithms and the soundness of the abstraction rely on domain theory
concepts, thus providing a solid mathematical framework to our
work. Last but not least, our approach can handle infinite
  behaviours, in contrast to the threshold automata framework.
       
  The logical fragment of the IVy toolset has also been shown to allow
  to model threshold guards by axiomising their semantics as quorum
  systems \cite{BerkovitsLLPS19}.  For instance, the reason for
  waiting for quorums of more than $n/2$ messages is that any two such
  quorums must intersect at one sender.  IVy allows to express these
  quorum axioms and reduce verification to decidable
  fragments. Similar intuitions underlie verification results in the
  heard-of model (HO model)~\cite{Charron-BostS09}. This computational
  model for distributed algorithms already targets a high level of
  abstractions that are sound for communication closed distributed
  algorithms~\cite{Chaouch-SaadCM09}.  Here a consensus logic was
  introduced in~\cite{DHVWZ14} that could be used for deductive
  verification and cut-off results where provided in \cite{MaricSB17}
  that reduce the parameterized verification problem to small finite
  instances. Compared to this line of work, the distributed algorithms
  we target share some similarities with these round-based
  communication closed models. Recently, a threshold automata
  framework for round-based algorithms was introduced that also uses a
  small counterexample property for verification
  in~\cite{StoilkovskaKWZ19}. In contrast, we use domain theory, and
  particularly Scott continuity to be able to reason on infinite
  behaviors and thus to capture algorithms that do not necessarily
  terminate.

Other less related verification frameworks also target distributed
algorithms with quite different techniques such as event
B~\cite{Mery-ictac19}, array systems \cite{AlbertiGP16} or logic
and automata theory~\cite{ABG-ic18}.

\paragraph*{Contributions}
Using basic domain theory concepts, we provide a rigorous framework to
model and verify (asynchronous) distributed algorithms. Our
methodology applies to distributed algorithms that are structured in
\emph{layers} (that can be seen as a fine-grain notion of rounds), and
may consist of countably many layers, thus capturing round-based
distributed algorithms (with no \emph{a priori} bound on the number of
rounds).
\begin{itemize}
\item In Section~\ref{sec:domain theory}, we define partially ordered
  transition systems, which serve to express the semantics our models.
\item Section~\ref{sec:setting} introduces the low-level model of layered distributed
  systems to represent threshold based distributed algorithms. The
  state-space of layered distributed systems being infinite (and
  even not necessarily finitely representable), we provide several
  abstraction steps, up to a so-called guard abstraction. The
  soundness of each step is justified by the Scott-continuity of the
  corresponding abstraction. Some steps are also complete, and thus do
  not introduce spurious behaviors. 
\item Finally, towards practical verification, we define in
  Section~\ref{sec:implem} the guard automaton, a finite-state
  abstraction of (cyclic) layered distributed systems. It
  overapproximates the set of infinite behaviors of distributed
  algorithms, and thus enabling the verification of safety as well as
  liveness properties. Its construction can be automated with the help
  of an SMT solver, paving the way to the automated verification of
  round-based threshold distributed algorithms.
\end{itemize}
% Due to space limitations, missing proofs and details can be found in
% the companion HAL report~\cite{hal-concur21}.
