\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{maccros}

\theoremstyle{plain}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}

\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}

\title{Adding Randomisation to DTS and Guard Automata}
\author{Bastien Thomas}
\date{}
\begin{document}
\maketitle

\section{Probability Distributions and Domains}

In this section, we consider a \emph{continuous} DCPO $(\DTX, \myLeq)$
equipped with the Scott topology $\Scott(\DTX)$. A probability
distribution over $\DTX$ is a mapping
$\DTdistr: \Scott(\DTX) \to [0, 1]$ that verifies:
\begin{description}
\item[strictness:] $\DTdistr(\emptyset) = 0$
\item[monotonicity:] If $\DTxi_1 \subseteq \DTxi_2$ then $\DTdistr(\DTxi_1) \leq \DTdistr(\DTxi_2)$
\item[modularity:]
  $\DTdistr(\DTxi_1 \cup \DTxi_2) + \DTdistr(\DTxi_1 \cap \DTxi_2) =
  \DTdistr(\DTxi_1) + \DTdistr(\DTxi_2)$
\item[continuity:] For ${\langle \DTxi_j \rangle}_{j \in J}$ a directed
  subset of $(\Scott(\DTX), \subseteq)$,
  $\DTdistr(\bigcup_{j \in J} \DTxi_j) = \sup_{j \in J}
  \DTdistr(\DTxi_j)$.
\item[normalisation:] $\DTdistr(\DTX) = 1$
\end{description}

The following theorem (proven by~\cite{MED-ents98}) justifies the
appelation \emph{probability distribution}. Note that this result only
hold for \emph{continuous} DCPO\@.

\begin{theorem}\label{thm: extension supp distr}
  Every probability distribution over $\DTX$ has a unique extension to
  a measure on the Borel $\sigma$-algebra of $(\DTX, \Scott(\DTX))$.
\end{theorem}

For two such probability distribution $\DTdistr$ and $\DTdistr'$, we
write $\DTdistr \myLeq \DTdistr'$ if for any
$\DTxi \in \Scott(\DTX)$, $\distr(\DTxi) \leq \distr(\DTxi')$,
then $\left(\distr(\DTX), \myLeq\right)$ is a DCPO\@.

For $\DTx \in \DTX$, we define the point distribution on $\DTx$
$\pointdistr{\DTx}$ such that $\pointdistr{\DTx}(\DTxi) = 1$ if
$x \in \xi$ and $0$ otherwise.


\section{Partially Ordered Markov Decision Processes}

The natural settings for modelling both the randomisation and the non
determinism that occu in randomised distributed algorithm are Markov
Decision Processes (MDP). Simple algorithms can be modelled as a
finite state MDP, where classical techniques can be used to perform
modelisation. However, the state space of our algorithms are typically
infinite, meaning that a lot of classical results cannot be
applied. We will however have access to strong hypothesis involving
partial orders that we will present in this section.

\begin{definition}
  A Markov Decision Process (MDP) consists of a tuple
  $\MDPO = \left(\MDPS, \MDPA, {\left( \MDPp{\MDPa} \right)}_{\MDPa
      \in \MDPA}\right)$ where:
  \begin{itemize}
  \item $\MDPS$ is a countable set of \emph{states}
  \item $\MDPA$ is a countable set of \emph{actions}
  \item For $\MDPa \in \MDPA$,
    $\MDPp{\MDPa}: \MDPS \times \MDPS \to \R$ is such that for any
    $\MDPs \in \MDPS$,
    $\MDPp{\MDPa}\left(\MDPs, \_\right): \MDPS \to \R$ forms a
    \emph{discrete} probability distribution. Intuitively,
    $\MDPp{\MDPa}\left(\MDPs, \MDPs'\right)$ denote the probability of
    going from a state $\MDPs$ to a state $\MDPs'$ under an action
    $\MDPa$.
  \end{itemize}

  Suppose that $\MDPS$ has a partial ordering $(\MDPS, \MDPleq)$. In
  this case, $\MDPO$ itself is said to be partially ordered if
  whenever $\MDPp{\MDPa}\left(\MDPs, \MDPs'\right) > 0$, then
  $\MDPs \MDPleq \MDPs'$.
\end{definition}

In the following, $\incr\left(\MDPS^+\right) \subset \MDPS^+$ will
denote the set of non-empty monotonous sequences over $\MDPS$, meaning
that $\MDPs_0 \dotso \MDPs_n \in \MDPS^+$ if and only if for any
$i < n$, $\MDPs_i \MDPleq \MDPs_{i+1}$. Similarly,
$\incr\left(\MDPS^\omega\right) \subset \MDPS^\omega$ denote
monotonous infinite sequences.

\begin{definition}
  An \emph{adversary} over a partially ordered MDP is a function
  $\MDPadv: \incr\left(\MDPS^+\right) \to \distr\left(\MDPA\right)$.

  Consider such an adversary $\MDPadv$ and an initial distribution of
  state $\MDPSdistr \in \distr\left(\MDPS\right)$.

  Define ${\left(\MDPSvar{\MDPadv}{\MDPSdistr}{n}\right)}_{n \in \N}$,
  a sequence of random variables over $\MDPS$, and
  ${\left(\MDPAvar{\MDPadv}{\MDPSdistr}{n}\right)}_{n \in \N}$,
  another one over $\MDPA \cup \{\MDPstop\}$. For a finite monotonous
  sequence
  ${\left( \MDPs_i \right)}_{i \leq n + 1} \in
  \incr\left(\MDPS^{n+1}\right)$ and $\MDPa \in \MDPA$, the
  distribution of the variables follow the relations:
  \begin{align*}
    \ProbaCond{ \MDPAvar{\MDPadv}{\MDPSdistr}{n} = \MDPa }{ \forall i \leq n, \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_i } &= \MDPadv(\MDPs_0 \dotso \MDPs_n)(\MDPa) \\
    \ProbaCond{ \MDPSvar{\MDPadv}{\MDPSdistr}{n+1} = \MDPs_{n+1} }{ \MDPSvar{\MDPadv}{\MDPSdistr}{n} = \MDPs_{n} \wedge \MDPAvar{\MDPadv}{\MDPSdistr}{n} = \MDPa } &= \MDPp{\MDPa}\left(\MDPs_n, \MDPs_{n+1}\right)
  \end{align*}
\end{definition}

The idea behind this definition is that the adversary can choose which
actions to play (or to stop) depending on the history of the play, and
a random player chooses the outcome of the probabilistic actions
depending only on the current state and the chosen actions.

\begin{definition}
  Suppose that $\left(\MDPS, \MDPleq\right)$ forms a countable basis
  of a continuous DCPO $\left(\closure{\MDPS}, \MDPleq\right)$. For
  $\MDPadv$ an adversary and $\MDPSdistr$ an initial distribution
  define the random variable
  $\MDPlast{\MDPadv}{\MDPSdistr}$ over $\closure{\MDPS}$ with:
  \[ \MDPlast{\MDPadv}{\MDPSdistr} = \lub_{n \in \N} \MDPSvar{\MDPadv}{\MDPSdistr}{n} \]
\end{definition}

On a finite partially ordered MDP, the random variable
$\MDPlast{\MDPadv}{\MDPSdistr}$ would encode the distribution of the
final state of the MDP under the adversary $\MDPadv$. Using the notion
of continuous DCPO, we extends this definition to some infinite MDP\@.

\begin{definition}
  An adversary
  $\MDPadv: \incr\left(\MDPS^+\right) \to \distr\left(\MDPA\right)$ is
  qualified as:
  \begin{description}
  \item[Stutter-Invariant] if repetitions in the input do not affect
    the result. Formally, if for any
    $u, v \in \incr\left(\MDPS^*\right)$ and $\MDPs \in \MDPS$ such
    that $u \concat \MDPs \concat v \in \incr\left(\MDPS^+\right)$,
    $\MDPadv\left( u \concat \MDPs \concat \MDPs \concat v \right)$.
  \item[Memoryless] if its output distribution depends only on the
    last letter of its input. More precisely if for any
    $u \concat \MDPs \in \incr\left(\MDPS^+\right)$,
    $\MDPadv\left(u \concat \MDPs\right) =
    \MDPadv\left(\MDPs\right)$. The input set of a memoryless
    adversary can therefore be restricted to $\MDPS$.

    In particular, any memoryless adversary is stutter-invariant.
  \item[Deterministic] if its output is a point distribution. The
    output set of a deterministic adversary can therefore be
    restricted to $\MDPA$.
  \end{description}
  Hence, an adversary that is both memoryless and deterministic can be
  encoded by a function $\MDPS \to \MDPA$.
\end{definition}

\begin{proposition}
  For any \emph{stutter-invariant} adversary
  $\MDPadv: \incr\left(\MDPS^+\right) \to \distr\left(\MDPA\right)$
  and initial distribution $\MDPSdistr \in \distr(\MDPS)$, there
  exists a memoryless adversary
  $\MDPadv': \MDPS \to \distr\left(\MDPA\right)$ such that
  $\MDPlast{\MDPadv}{\MDPSdistr}$ and $\MDPlast{\MDPadv'}{\MDPSdistr}$
  follow the same distributions.
\end{proposition}
\begin{proof}
  We define $\MDPadv'$ such that:
  \[
    \MDPadv'(\MDPs)(\MDPa) = \ProbaCond{
      \exists i \in \N,
      \begin{gathered}
        \forall j < i, \MDPSvar{\MDPadv}{\MDPSdistr}{j} \MDPsleq \MDPs \\
        \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs \\
        \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa
      \end{gathered}
    }{
      \exists j \in \N, \MDPSvar{\MDPadv}{\MDPSdistr}{j} = \MDPs
    }
  \]

  The idea behind this definition is that we want the probability of
  $\MDPadv'$ taking the action $\MDPa$ in state $\MDPs$ to be the same
  as the probability for $\MDPadv$ to take if we only knew that it has
  reached $\MDPs$ for the first time. This means that we ignore the
  outputs of $\MDPadv$ when it stutters on a state, which explains why
  we need the stutter-invariance of $\MDPadv$.

  The function $\MDPadv'(\MDPs)$ is indeed a probability distribution
  over $\MDPA$ because the family of events
  ${\left( E_{\MDPa} \right)}_{\MDPa \in \MDPA}$ defined with

  \[
    E_{\MDPa} = \left(\exists i \in \N,
      \begin{gathered}
        \forall j < i, \MDPSvar{\MDPadv}{\MDPSdistr}{j} \MDPsleq \MDPs \\
        \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs \\
        \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa
      \end{gathered}
    \right)
  \]

  forms a partition of the event
  $\left(\exists j \in \N, \MDPSvar{\MDPadv}{\MDPSdistr}{j} =
    \MDPs\right)$.
  
  For a Scott-open set $O \in \Scott\left( \closure{\MDPS} \right)$,
  $\Proba{\MDPlast{\MDPadv}{\MDPSdistr} \in O} = \Proba{\exists n \in
    \N, \MDPSvar{\MDPadv}{\MDPSdistr}{n} \in O}$. The same holds for
  adversary $\MDPadv'$. Moreover, because $O$ is an upper set, the
  sequence of events
  ${\left(\MDPSvar{\MDPadv'}{\MDPSdistr}{n} \in O\right)}_{n \in \N}$
  is increasing, meaning that:

  \[
    \Proba{\exists n \in \N, \MDPSvar{\MDPadv'}{\MDPSdistr}{n} \in O}
    = \sup_{n \in \N} \Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n} \in O}
  \]

  For $n = 0$,
  $\Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{0} \in O} = \MDPSdistr(O)$.
  
  For $n \in \N$, we have:
  \[
    \Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n+1} \in O}
    = \Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n} \in O} +
      \Proba{
      \begin{gathered}
        \MDPSvar{\MDPadv'}{\MDPSdistr}{n+1} \in O \\
        \MDPSvar{\MDPadv'}{\MDPSdistr}{n} \notin O
      \end{gathered}
    }
  \]

  and for $\MDPs_n, \MDPs_{n+1} \in \MDPS$ and $\MDPa_n \in \MDPA$,

  \begin{align*}
    &\Proba{
      \MDPSvar{\MDPadv'}{\MDPSdistr}{n} = \MDPs_n \wedge
      \MDPAvar{\MDPadv'}{\MDPSdistr}{n} = \MDPa_n \wedge
      \MDPSvar{\MDPadv'}{\MDPSdistr}{n+1} = \MDPs_{n+1} 
    } \\
    &\quad = \Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n} = \MDPs_n} \times \MDPadv'(\MDPs_n)(\MDPa_n) \times \MDPp{\MDPa_n}(\MDPs_n, \MDPs_{n+1}) \\
    &\quad = \frac{\Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n} = \MDPs_n} \times \MDPp{\MDPa_n}(\MDPs_n, \MDPs_{n+1}) \times \Proba{
      \exists i \in \N,
      \begin{gathered}
        \forall j < i, \MDPSvar{\MDPadv}{\MDPSdistr}{j} \MDPsleq \MDPs_n \\
        \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_n \\
        \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa_n
      \end{gathered}
    }
    }{
      \Proba{\exists j \in \N, \MDPSvar{\MDPadv}{\MDPSdistr}{j} = \MDPs_n}
    } \\
    &\quad = \frac{\Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n} = \MDPs_n}}{\Proba{\exists j \in \N, \MDPSvar{\MDPadv}{\MDPSdistr}{j} = \MDPs_n}
      }
      \times \MDPp{\MDPa_n}(\MDPs_n, \MDPs_{n+1}) \times \sum_{i \in \N} \Proba{
      \begin{gathered}
        \forall j < i, \MDPSvar{\MDPadv}{\MDPSdistr}{j} \MDPsleq \MDPs_n \\
        \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_n \\
        \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa_n
      \end{gathered}
    } \\
    &\quad =
      \frac{\Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n} = \MDPs_n}}{
      \Proba{\exists j \in \N, \MDPSvar{\MDPadv}{\MDPSdistr}{j} = \MDPs_n}}
      \times \sum_{i \in \N}
      \ProbaCond{\MDPSvar{\MDPadv}{\MDPSdistr}{i+1} = \MDPs_{n+1}}{
      \begin{gathered}
        \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_n \\
        \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa_n
      \end{gathered}
    } \times \Proba{
    \begin{gathered}
      \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_n \\
        \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa_n
      \end{gathered}
    } \times \ProbaCond{
    \forall j < i, \MDPSvar{\MDPadv}{\MDPSdistr}{j} \MDPsleq \MDPs_n
    }{
    \begin{gathered}
      \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_n \\
      \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa_n
    \end{gathered}
    } \\
    &\quad =
      \frac{\Proba{\MDPSvar{\MDPadv'}{\MDPSdistr}{n} = \MDPs_n}}{
      \Proba{\exists j \in \N, \MDPSvar{\MDPadv}{\MDPSdistr}{j} = \MDPs_n}}
      \times \sum_{i \in \N}
      \Proba{
      \begin{gathered}
        \MDPSvar{\MDPadv}{\MDPSdistr}{i+1} = \MDPs_{n+1} \\
        \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_n \\
        \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa_n
      \end{gathered}
    } \times \ProbaCond{
    \forall j < i, \MDPSvar{\MDPadv}{\MDPSdistr}{j} \MDPsleq \MDPs_n
    }{
    \begin{gathered}
      \MDPSvar{\MDPadv}{\MDPSdistr}{i} = \MDPs_n \\
      \MDPAvar{\MDPadv}{\MDPSdistr}{i} = \MDPa_n
    \end{gathered}
    } \\
  \end{align*}
  
\end{proof}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
