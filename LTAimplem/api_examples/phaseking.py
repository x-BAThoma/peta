import lta
import formulas as fm
from guard_automaton import GuardAutomaton

def var(x):
    return fm.Term(x)

phase_king = lta.CLTA(2)

n = phase_king.add_parameter('n')
t = phase_king.add_parameter('t')
f = phase_king.add_parameter('f')

v0 = phase_king.add_state(0, 'v0')
v1 = phase_king.add_state(0, 'v1')

k00 = phase_king.add_state(1, 'p0k0')
k01 = phase_king.add_state(1, 'p0k1')
p0 = phase_king.add_state(1, 'p0')
p1 = phase_king.add_state(1, 'p1')
k10 = phase_king.add_state(1, 'p1k0')
k11 = phase_king.add_state(1, 'p1k1')

# First Layer
phase_king.set_guard(0, v0, p0, fm.TRUE)
phase_king.set_guard(
    0, v0, k00,
    fm.And(
        var(v0) + var(v1) + var(f) == var(n),
        2*(var(v0) + var(f)) >= var(n)
    )
)
phase_king.set_guard(
    0, v0, k01,
    fm.And(
        var(v0) + var(v1) + var(f) == var(n),
        2*(var(v1) + var(f)) >= var(n)
    )
)
phase_king.set_guard(0, v1, p1, fm.TRUE)
phase_king.set_guard(
    0, v1, k10,
    fm.And(
        var(v0) + var(v1) + var(f) == var(n),
        2*(var(v0) + var(f)) >= var(n)
    )
)
phase_king.set_guard(
    0, v1, k11,
    fm.And(
        var(v0) + var(v1) + var(f) == var(n),
        2*(var(v1) + var(f)) >= var(n)
    )
)

# Second Layer
all_p0 = var(p0) + var(k00) + var(k01)
all_p1 = var(p1) + var(k10) + var(k11)
all_k0 = var(k00) + var(k10)
all_k1 = var(k01) + var(k11)

full = var(k00) + var(k01) + var(p0) + var(p1) + var(k10) + var(k11) + var(f) == var(n)
for src in [k00, k01, p0, p1, k10, k11]:
    maj0 = 2*all_p0 + 2*var(f) > var(n) + 2*var(t)
    king0 = fm.And(
        all_p0 <= var(n) + 2*var(t),
        all_p1 <= var(n) + 2*var(t),
        all_k1 == var(0)
    )
    phase_king.set_guard(
        1, src, v0,
        fm.And(
            full,
            fm.Or(
                maj0,
                king0
            )
        )
    )
    maj1 = 2*all_p1 + 2*var(f) > var(n) + 2*var(t)
    king1 = fm.And(
        all_p0 <= var(n) + 2*var(t),
        all_p1 <= var(n) + 2*var(t),
        all_k0 == var(0)
    )
    phase_king.set_guard(
        1, src, v1,
        fm.And(
            full,
            fm.Or(
                maj1,
                king1
            )
        )
    )

ga = GuardAutomaton(phase_king)
ga.add_parametric_relation(2*var(t) < n)
ga.add_parametric_relation(var(f) <= var(t))
ga.add_parametric_relation(var(t) > 0)

ga.add_assertion(0, var(v0) + var(v1) + f <= var(n))
ga.add_assertion(1, all_p0 + all_p1 + f <= var(n))

ga.add_proposition(0, "layer 0 full", var(v0) + var(v1) + var(f) >= var(n))
ga.add_initial_cond({"layer 0 full": True})

ga.add_proposition(0, "v0 > 0", var(v0) > var(0))
ga.add_proposition(0, "v1 > 0", var(v1) > var(0))

ga.add_proposition(1, "layer 1 full", all_p0 + all_p1 + var(f) >= var(n))
ga.add_proposition(1, "k0 > 0", all_k0 > var(0))
ga.add_proposition(1, "k1 > 0", all_k1 > var(0))
ga.add_proposition(1, "p0 > 0", all_p0 > var(0))
ga.add_proposition(1, "p1 > 0", all_p1 > var(0))
ga.add_proposition(1, "majority 0", 2*all_p0 + 2*var(f) > var(n) + 2*var(t))
ga.add_proposition(1, "majority 1", 2*all_p1 + 2*var(f) > var(n) + 2*var(t))

adj_list = ga.build_adjacence_list()

for state, successors in adj_list:
    print("state: " + ", ".join([str(s) for s in successors]))
