""" Provide the Ben-Or lta """
import lta
import formulas as fm
from guard_automaton import GuardAutomaton
from lta_encoder_itp import LTAEncoderITP

def var(x):
    return x.term()

ben_or = lta.CLTA(3)
n = ben_or.add_parameter('n')
t = ben_or.add_parameter('t')

v0 = ben_or.add_state(0, 'v0')
v1 = ben_or.add_state(0, 'v1')

p0 = ben_or.add_state(1, 'p0')
pi = ben_or.add_state(1, 'p?')
p1 = ben_or.add_state(1, 'p1')

r0 = ben_or.add_state(2, 'r0')
ri = ben_or.add_state(2, 'r?')
r1 = ben_or.add_state(2, 'r1')
d0 = ben_or.add_state(2, 'd0')
d1 = ben_or.add_state(2, 'd1')

for src in [v0, v1]:
    ben_or.set_guard(0, src, p0, 2*var(v0) > var(n))
    ben_or.set_guard(0, src, p1, 2*var(v1) > var(n))
    ben_or.set_guard(
        0, src, pi,
        fm.And(
            var(v0) + var(v1) >= var(n) - var(t),
            2*var(v0) >= var(n) - 2*var(t),
            2*var(v1) >= var(n) - 2*var(t)
        )
    )

for src in [p0, pi, p1]:
    ben_or.set_guard(1, src, d0, var(p0) > t)
    ben_or.set_guard(1, src, d1, var(p1) > t)
    ben_or.set_guard(
        1, src, r0,
        fm.And(
            var(p0) + var(pi) >= var(n) - var(t),
            var(pi) >= var(n) - 2*var(t),
            var(p0) > 0
        )
    )
    ben_or.set_guard(
        1, src, r1,
        fm.And(
            var(p1) + var(pi) >= var(n) - var(t),
            var(pi) >= var(n) - 2*var(t),
            var(p1) > 0
        )
    )
    ben_or.set_guard(
        1, src, ri,
        var(pi) >= var(n) - var(t)
    )

ben_or.set_guard(2, r0, v0, fm.TRUE)
ben_or.set_guard(2, r1, v1, fm.TRUE)
ben_or.set_guard(2, ri, v0, fm.TRUE)
ben_or.set_guard(2, ri, v1, fm.TRUE)

ga = GuardAutomaton(ben_or)

ga.add_parametric_relation(2 * var(t) < var(n))
#ga.add_parametric_relation(var(t) > 0)

ga.add_assertion(0, var(v0) + var(v1) <= n)
ga.add_assertion(1, var(p0) + var(pi) + var(p1) <= n)
ga.add_assertion(2, var(d0) + var(r0) + var(ri) + var(r1) + var(d1) <= n)

ga.add_proposition(0, "v0 > 0", var(v0) > 0)
ga.add_proposition(0, "v1 > 0", var(v1) > 0)
ga.add_proposition(0, "v0 + v1 >= n - t", var(v0) + var(v1) >= var(n) - var(t))

ga.add_proposition(1, "p0 > 0", var(p0) > 0)
ga.add_proposition(1, "pi > 0", var(pi) > 0)
ga.add_proposition(1, "p1 > 0", var(p1) > 0)
ga.add_proposition(1, "p0 + pi + p1 >= n - t", var(p0) + var(pi) + var(p1) >= var(n) - var(t))

ga.add_proposition(2, "d0 > 0", var(d0) > 0)
ga.add_proposition(2, "r0 > 0", var(r0) > 0)
ga.add_proposition(2, "ri > 0", var(ri) > 0)
ga.add_proposition(2, "r1 > 0", var(r1) > 0)
ga.add_proposition(2, "d1 > 0", var(d1) > 0)
ga.add_proposition(2, "d0 + r0 + ri + r1 + d1 >= n - t", var(d0) + var(r0) + var(ri) + var(r1) + var(d1) >= var(n) - var(t))
adj_list = ga.build_adjacence_list()
safe = True
for state in adj_list.keys():
    clayer, val = ga.get_state_val(state)
    if clayer == 2:
        if val["d0 > 0"] and val["d1 > 0"]:
            safe = False
print("Safe: {}".format(safe))

###############################################################

ga_itp = LTAEncoderITP(ben_or, 3)
ga_itp.add_assertion(None, 2 * var(t) < var(n))
ga_itp.add_assertion(0, var(v0) + var(v1) <= n)
ga_itp.add_proposition(0, "v0 > 0", var(v0) > 0)
ga_itp.add_proposition(2, "d0 > 0", var(d0) > 0)

valuation = ga_itp.get_empty_valuation()
valuation[0]["v0 > 0"] = False
valuation[2]["d0 > 0"] = True
interpolants = ga_itp.compute_interpolants(valuation)
print(interpolants)
print(ga_itp.converter.from_py_smt(interpolants[0]))
print(ga_itp.converter.from_py_smt(interpolants[1]))
print(ga_itp.converter.from_py_smt(interpolants[2]))
