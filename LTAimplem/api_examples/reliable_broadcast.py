""" Provide the reliable broadcast lta """
import lta
import formulas as fm
from lta_encoder import LTAEncoder

def var(x):
    return fm.Term(x)

rel_broadcast = lta.CLTA(3)
n = rel_broadcast.add_parameter("n")
t = rel_broadcast.add_parameter("t")
f = rel_broadcast.add_parameter("f")

v0  = rel_broadcast.add_state(0, "v0")
v1  = rel_broadcast.add_state(0, "v1")
p   = rel_broadcast.add_state(1, "p")
acc = rel_broadcast.add_state(2, "acc")

rel_broadcast.set_guard(0, v0, p, var(v1) + var(f) > var(t))
rel_broadcast.set_guard(0, v1, p, fm.TRUE)
rel_broadcast.set_guard(1, p, acc, var(p) + var(f) >= var(n) - var(t))

ga = LTAEncoder(rel_broadcast, 3)

# Setup parametric relations
ga.add_assertion(None, 3*var(t) < var(n))
ga.add_assertion(None, var(f) <= var(t))

# Setup initial conditions
ga.add_assertion(0, var(v0) + var(v1) + var(f) == var(n))

# Interesting events
ga.add_proposition(0, "v1 > 0", var(v1) > 0)
ga.add_proposition(0, "v1 >= n - f", var(v1) >= var(n) - var(f))
ga.add_proposition(2, "acc > 0", var(acc) > 0)
ga.add_proposition(2, "acc >= n - f", var(acc) >= var(n) - var(f))

vals = ga.get_empty_valuation()
vals[2]["acc >= n - f"] = True
print("Sanity check: {}".format(ga.check_valuation(vals)))
vals[0]["v1 > 0"] = False
print("Safety: {}".format(not(ga.check_valuation(vals))))

# Fairness conditions
edge_vp1 = rel_broadcast.get_edge_variable(0, v1, p)
edge_vp0 = rel_broadcast.get_edge_variable(0, v0, p)
edge_pacc = rel_broadcast.get_edge_variable(1, p, acc)
ga.add_assertion(0, var(v1) == var(edge_vp1))
ga.add_assertion(
    0,
    fm.Or(
        fm.Not(var(v1) > var(t)),
        var(v0) == var(edge_vp0)
    )
)
ga.add_assertion(
    1,
    fm.Or(
        fm.Not(var(p) >= var(n) - var(t)),
        var(p) == var(edge_pacc)
    )
)

# Liveness
vals = ga.get_empty_valuation()
vals[0]["v1 >= n - f"] = True
print("Liveness Sanity Check: {}".format(ga.check_valuation(vals)))
vals[2]["acc >= n - f"] = False
print("Liveness: {}".format(not(ga.check_valuation(vals))))

print(ga.complete_valuation({}))
