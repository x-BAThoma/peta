from valuation_manager import ValManager

class Automaton:
    
    def __init__(self):
        self.props = ValManager()
        self.edges = []
        self.initial_state = None
        self.final_states = set()

    def copy(self):
        res = Automaton()
        res.props = self.props.copy()
        res.edges = self.edges.copy()
        res.initial_state = self.initial_state
        res.final_states = self.final_states.copy()
        return res
        
    def add_proposition(self, name):
        self.props.add_proposition(name)

    def add_state(self):
        state = len(self.edges)
        self.edges.append({})
        return state

    def add_edge(self, src, dest, val_edge):
        self.edges[src][dest] = val_edge

    def set_initial(self, state):
        self.initial_states = state

    def set_final(self, state):
        self.final_states.add(state)
        
    def union(self, other):
        assert self.initial_state is not None
        assert other.initial_state is not None
        result = Automaton()
        pair_to_state = {}
        state_to_pair = []
        s0 = result.add_state()
        assert s0 == len(state_to_pair)
        state_to_pair.append((self.initial_state, other.initial_state))
        pair_to_state[(self.initial_state, other.initial_state)] = s0
        stack = [()
