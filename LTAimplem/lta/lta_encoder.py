from pysmt.shortcuts import Solver, GT, GE, Equals, Plus
from pysmt.shortcuts import Symbol, And, Not, Implies, Iff, Int
from pysmt.typing import BOOL

from formulas_converter import FormulaConverter
from all_sat import all_sat

class LTAEncoder:
    """
    A class that uses pysmt to compute some finite parts of a consistent
    guard configuration of a given LTA.
    """
    def __init__(self, lta, length):
        self.lta = lta
        self.length = length
        self.propositions = [{} for layer in range(length)]
        self.converter = FormulaConverter()
        self.solver = Solver(name="msat")

        self.solver.add_assertion(
            self.no_negative_parameters()
        )
        for layer in range(self.length):
            self.solver.add_assertion(
                self.no_negative_states(layer)
            )
            self.solver.add_assertion(
                self.no_negative_edges(layer)
            )
            self.solver.add_assertion(
                self.guard_coherence(layer)
            )
            self.solver.add_assertion(
                self.out_flow_coherence(layer)
            )
        for layer in range(self.length - 1):
            self.solver.add_assertion(
                self.in_flow_coherence(layer)
            )
        
    def no_negative_parameters(self):
        """ Add conditions p >= 0 for every parameters of the lta """
        formulas = []
        for param in self.lta.get_parameters():
            smt_param = self.converter.convert_parameter(param)
            formulas.append(GE(smt_param, Int(0)))
        return And(formulas)

    def no_negative_states(self, layer):
        """ Add condition s >= 0 for every state s of the input layer """
        layer_index = self.lta.get_layer_index(layer)
        formulas = []
        for state in self.lta.get_states(layer_index):
            smt_state = self.converter.convert_state(state, layer)
            formulas.append(GE(smt_state, Int(0)))
        return And(formulas)

    def no_negative_edges(self, layer):
        """ Add condition e >= 0 for every edge e between the input layer and the next """
        layer_index = self.lta.get_layer_index(layer)
        formulas = []
        for src in self.lta.get_states(layer_index):
            for dest in self.lta.get_successors(layer_index, src):
                edge = self.lta.get_edge_variable(layer_index, src, dest)
                smt_edge = self.converter.convert_edge(edge, layer)
                formulas.append(GE(smt_edge, Int(0)))
        return And(formulas)

    def guard_coherence(self, layer):
        """ Add conditions (e > 0) -> guard(e) for every edges e
        in the input layer
        """
        layer_index = self.lta.get_layer_index(layer)
        formulas = []
        for src in self.lta.get_states(layer_index):
            for dest in self.lta.get_successors(layer_index, src):
                guard = self.lta.get_guard(layer_index, src, dest)
                edge = self.lta.get_edge_variable(layer_index, src, dest)
                smt_edge = self.converter.convert_edge(edge, layer)
                smt_guard = self.converter.convert_formula(guard, layer)
                condition = Implies(
                    GT(smt_edge, Int(0)),
                    smt_guard
                )
                formulas.append(condition)
        return And(formulas)

    def out_flow_coherence(self, layer):
        """ Add condition s >= sum_{s' in S_{layer + 1}} e(s, s') for all
        state s in the input layer """
        layer_index = self.lta.get_layer_index(layer)
        formulas = []
        for src in self.lta.get_states(layer_index):
            smt_src = self.converter.convert_state(src, layer)
            out_edges = []
            for dest in self.lta.get_successors(layer_index, src):
                edge = self.lta.get_edge_variable(layer_index, src, dest)
                out_edges.append(self.converter.convert_edge(edge, layer))
            if out_edges:
                condition = GE(smt_src, Plus(*out_edges))
                formulas.append(condition)
        return And(formulas)

    def in_flow_coherence(self, layer):
        """ Add condition sum_{s in S_{layer}} e(s, s') = s' for
        all state s' in the layer (layer + 1) """
        layer_index = self.lta.get_layer_index(layer)
        next_index = self.lta.get_layer_index(layer + 1)
        # We first compute the incoming edges for each state at layer next_index
        in_edges = {s: [] for s in self.lta.get_states(next_index)}
        for src in self.lta.get_states(layer_index):
            for dest in self.lta.get_successors(layer_index, src):
                edge = self.lta.get_edge_variable(layer_index, src, dest)
                smt_edge = self.converter.convert_edge(edge, layer)
                in_edges[dest].append(smt_edge)
        # Then we build the formula
        formulas = []
        for dest, edges in in_edges.items():
            if edges:
                smt_dest = self.converter.convert_state(dest, layer + 1)
                condition = Equals(Plus(*edges), smt_dest)
                formulas.append(condition)
        return And(formulas)

    def formula_test(self, layer, formula):
        """ Tests if the input formula matches the input layer """
        if layer is None:
            if not self.lta.check_guard(None, formula):
                return False
            return True
        layer_index = self.lta.get_layer_index(layer)
        if not self.lta.check_guard(layer_index, formula):
            return False
        return True

    def add_assertion(self, layer, formula):
        """ Adds an assertion at the given layer. Can be used to impose
        fairness conditions for example. layer can be None if the assertion
        is a parametric relation. """
        if not self.formula_test(layer, formula):
            raise ValueError("{} contains unsupported variables".format(formula))
        smt_formula = self.converter.convert_formula(formula, layer)
        self.solver.add_assertion(smt_formula)

    def get_prop_handle(self, layer, name):
        """ Return a pysmt boolean variable used as a shorthand for
        the full proposition corresponding to the input name """
        layer_index = self.lta.get_layer_index(layer)
        if name not in self.propositions[layer_index]:
            raise KeyError("No proposition {} in layer {}".format(name, layer))
        handle_name = "H{}({})".format(layer, name)
        return Symbol(handle_name, BOOL)

    def get_proposition_setup(self, layer, name, prop):
        """ Return the formula to add to the solver in order
        to link the input prop with its handle """
        if not self.formula_test(layer, prop):
            raise ValueError("{} contains unsupported variables".format(prop))
        prop_handle = self.get_prop_handle(layer, name)
        smt_prop = self.converter.convert_formula(prop, layer)
        return Iff(prop_handle, smt_prop)

    def add_proposition(self, layer, name, prop):
        if name in self.propositions[layer]:
            raise KeyError("Already a proposition named {} in layer {}".format(name, layer))
        self.propositions[layer][name] = prop
        self.solver.add_assertion(self.get_proposition_setup(layer, name, prop))

    def get_empty_valuation(self):
        """ Return an empty valuation of the propositions of the abstraction """
        return [{} for layer in range(self.length)]

    def val_to_formula(self, valuation):
        """ Utility function for converting a valuation into a pysmt clause """
        result = []
        for layer, l_valuation in enumerate(valuation):
            for name, value in l_valuation.items():
                prop_handle = self.get_prop_handle(layer, name)
                if value:
                    result.append(prop_handle)
                else:
                    result.append(Not(prop_handle))
        return result

    def check_valuation(self, valuation):
        """ Check if the input valuation is feasible or not """
        assumptions = self.val_to_formula(valuation)
        return self.solver.solve(assumptions)

    def get_model(self):
        py_smt_model = self.solver.get_model()
        result = self.get_empty_valuation()
        for layer in range(self.length):
            layer_index = self.lta.get_layer_index(layer)
            for name in self.propositions[layer_index]:
                handle = self.get_prop_handle(layer, name)
                result[layer][name] = py_smt_model.get_py_value(handle)
        return result

    def complete_valuation(self, valuation):
        """ Given a feasible partial valuation, enumerates all the
        complete valuation that extends it. It is possible to restrict
        the querry to a sequence of layers by specifying the optional
        parameters start and length """
        # We convert the arguments to py_smt
        assumptions = self.val_to_formula(valuation)
        variables = []
        for layer in range(self.length):
            for name in self.propositions[layer].keys():
                variables.append(self.get_prop_handle(layer, name))
        # We get the result
        smt_valuations = all_sat(self.solver, variables, assumptions)
        # We convert the results back to our conventions
        result = []
        for smt_val in smt_valuations:
            val = self.get_empty_valuation()
            for layer in range(self.length):
                for name in self.propositions[layer].keys():
                    smt_name = self.get_prop_handle(layer, name)
                    val[layer][name] = smt_val[smt_name]
            result.append(val)
        return result
