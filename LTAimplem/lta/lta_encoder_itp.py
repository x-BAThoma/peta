"""
Enhance the normal guard abstraction by adding interpolation
mechanisms in order to produce new guards
"""

from pysmt.shortcuts import GT, GE, Equals, Plus
from pysmt.shortcuts import Solver, Symbol, Not, And, Implies, Iff, Int
from pysmt.typing import BOOL

from mathsat import * # Safe because every function starts with msat_
import formulas_converter as fc

from lta_encoder import LTAEncoder
from formulas_converter import FormulaConverter

class LTAEncoderITP(LTAEncoder):
    """
    A version of the guard abstraction that supports interpolation.
    (see method compute_interpolants).
    """
    def __init__(self, lta, length):
        self.lta = lta
        self.length = length
        self.propositions = [{} for layer in range(length)]
        self.converter = FormulaConverter()
        self.solver = Solver(name="msat", solver_options={"interpolation": "true"})

        env = self.solver.msat_env()
        self.itp_groups = []
        self.setup_itp_groups()

        # We put the parameters constraints in the first group
        # Maybe this can be changed later
        _ = msat_set_itp_group(env, self.itp_groups[0])
        self.solver.add_assertion(
            self.no_negative_parameters()
        )
        # We add all the model constraints in their respective layers.
        for layer in range(self.length):
            _ = msat_set_itp_group(env, self.itp_groups[layer])
            self.solver.add_assertion(
                self.no_negative_states(layer)
            )
            self.solver.add_assertion(
                self.no_negative_edges(layer)
            )
            self.solver.add_assertion(
                self.guard_coherence(layer)
            )
            self.solver.add_assertion(
                self.out_flow_coherence(layer)
            )
        for layer in range(self.length - 1):
            # _ = msat_set_itp_group(env, self.itp_groups[layer + 1])
            _ = msat_set_itp_group(env, self.itp_groups[layer])
            self.solver.add_assertion(
                self.in_flow_coherence(layer)
            )

    def setup_itp_groups(self):
        assert not self.itp_groups
        env = self.solver.msat_env()
        # We creat one itp group per layer
        for layer in range(self.length):
            group = msat_create_itp_group(env)
            assert group >= 0 # -1 means a mathsat error
            self.itp_groups.append(group)
            
    def add_assertion(self, layer, assertion):
        env = self.solver.msat_env()
        if layer is None:
            _ = msat_set_itp_group(env, self.itp_groups[0])
        else:
            _ = msat_set_itp_group(env, self.itp_groups[layer])
        super().add_assertion(layer, assertion)

    def add_proposition(self, layer, name, prop):
        env = self.solver.msat_env()
        _ = msat_set_itp_group(env, self.itp_groups[layer])
        super().add_proposition(layer, name, prop)

    def compute_interpolants(self, valuation, start=0, length=None):
        if length is None:
            length = self.length
        self.solver.push()

        env = self.solver.msat_env()
        # Add all the constraints of the valuation
        for layer in range(start, start + length):
            l_valuation = valuation[layer]
            _ = msat_set_itp_group(env, self.itp_groups[layer])
            for name, value in l_valuation.items():
                handle = self.get_prop_handle(layer, name)
                if value:
                    self.solver.add_assertion(handle)
                else:
                    self.solver.add_assertion(Not(handle))

        # Compute the sequence interpolants
        interpolants = {}
        for layer in range(start, start + length):
            _ = msat_set_itp_group(env, self.itp_groups[layer])
            if self.solver.solve():
                raise ValueError("Conditions are SAT")
            env = self.solver.msat_env()
            groupA = [self.itp_groups[i] for i in range(start, layer + 1)]
            # We obtain an interpolant as a mathsat formula,
            # we need to convert it back
            itp_msat = msat_get_interpolant(env, groupA)
            itp_pysmt = self.solver.converter.back(itp_msat)
            interpolants[layer] = itp_pysmt
            self.solver.add_assertion(itp_pysmt)
        return interpolants
        
