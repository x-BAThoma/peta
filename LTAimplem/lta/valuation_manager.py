
class ValManager:
    char_true = 't'
    char_false = 'f'
    char_unknown = '_'

    def __init__(self):
        self.prop_names = []
        self.prop_indices = {}

    def copy(self):
        res = ValManager()
        res.prop_names = self.prop_names.copy()
        res.prop_indices = self.prop_indices.copy()
        return res

    def add_proposition(self, name):
        if name in self.prop_indices:
            raise KeyError("Already a prop named: {}".format(name))
        index = len(self.prop_names)
        self.prop_names.append(name)
        self.prop_indices[index] = name

    def get_props(self):
        return self.prop_names

    def val_to_repr(self, valuation):
        str_repr = ""
        for name in self.prop_names:
            if name in valuation:
                if valuation[name]:
                    str_repr += self.char_true
                else:
                    str_repr += self.char_false
            else:
                str_repr += self.char_unknown
        return str_repr

    def repr_to_val(self, str_repr):
        valuation = {}
        for name, val_c in zip(self.prop_names, str_repr):
            if val_c == self.char_true:
                valuation[name] = True
            elif val_c == self.char_false:
                valuation[name] = False
        return valuation
