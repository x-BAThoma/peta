"""
Provide classes for manipulating linear arithmetic formulas over
string variables.

To define a new term, use Term("variable_name") or Term(integer), then
use the standard operators +, -, * to define more complex terms (no
need to call Term(_) for each integer used).  Finally, use the
standard comparisons <, <=, >, >=, ==, != to define propositional
Formulas.

The classes Not, And and Or can then be used to combine the formulas
together.  And, and Or can accept any number of argument.
"""

from collections import namedtuple
from gmpy2 import mpz

class Term:
    """Create and manipulate linear combinations of variables.

    A class used to represent linear combinations of variables.  The
    constructor can only build integer constants or a Term containing
    a single variable.  Use the overloaded methods +, -, * to build
    more complex Terms.

    :param arg: If it is an int or mpz, then create the corresponding
        integer constant, else it is treated as a variable.

    :raises TypeError: When the input is not hashable
    """

    def __init__(self, arg):
        self.constant = mpz(0)
        self.factors = {}
        if isinstance(arg, int):
            self.constant = mpz(arg)
        elif isinstance(arg, mpz):
            self.constant = arg
        elif isinstance(arg, Term):
            self.constant = arg.constant
            self.factors = arg.factors
        else:
            try:
                self.factors[arg] = mpz(1)
            except TypeError:
                raise TypeError(
                    "Cannot convert {} to Term, only hashable can be variables".format(arg)
                )

    def clean(self):
        """Removes 0 factors from the Term.
        """
        self.factors = {v: f for v, f in self.factors.items() if f != mpz(0)}

    def is_constant(self):
        """Check if the Term is an integer constant.

        :returns: 'True` if the Term is an integer constant, 'False`
                  otherwise.
        """
        self.clean()
        return not self.factors

    def copy(self):
        """Return a copy of the Term.

        :returns: A copy of the Term.
        """
        res = Term(0)
        res.constant = self.constant
        res.factors = self.factors.copy()
        return res

    def get_variables(self):
        """Return the set of variables that appear in the Term.

        :returns: a set containing the variables of the Term.
        """
        
        return set(self.factors.keys())

    def __str__(self):
        self.clean()
        res = ""
        for var, factor in self.factors.items():
            assert factor != 0
            if not res:          # The first factor should be treated separately
                if factor == 1:
                    res = str(var)
                elif factor == -1:
                    res = "-{}".format(var)
                else:
                    res = "{} {}".format(factor, var)
            else:
                if factor == 1:
                    res = "{} + {}".format(res, var)
                elif factor > 0:
                    res = "{} + {} {}".format(res, factor, var)
                elif factor == -1:
                    res = "{} - {}".format(res, var)
                else:
                    assert factor < -1
                    res = "{} - {} {}".format(res, abs(factor), var)
        if not res:
            assert not self.factors
            return str(self.constant)
        if self.constant == 0:
            return res
        if self.constant > 0:
            return "{} + {}".format(res, self.constant)
        assert self.constant < 0
        return "{} - {}".format(res, abs(self.constant))

    def __iadd__(self, other):
        other_term = Term(other)
        self.constant += other_term.constant
        for var, factor in other_term.factors.items():
            self.factors[var] = self.factors.get(var, 0) + factor
        return self

    def __add__(self, other):
        """Create a Term representing self + other.

        :param other: anything that can be converted into a Term

        :returns: a Term representing self + other
        """
        
        res = self.copy()
        return res.__iadd__(other)

    def __radd__(self, other):
        return self.__add__(other)
    
    def __imul__(self, arg):
        if isinstance(arg, Term):
            if not arg.is_constant():
                raise TypeError(
                    "Cannot multiply a Term by {}, only constant integer are supported".format(arg)
                )
            arg = arg.constant
        if not (isinstance(arg, int) or isinstance(arg, mpz)):
            raise TypeError(
                "Cannot multiply a Term by {}, only constant integer are supported".format(arg)
            )
        self.constant *= arg
        for var in self.factors.keys():
            self.factors[var] *= arg
        return self

    def __mul__(self, arg):
        """Create a Term representing arg*self.

        :param arg: either an int, mpz or a constant Term.

        :returns: a Term representing arg*self
        """
        res = self.copy()
        return res.__imul__(arg)

    def __rmul__(self, arg):
        return self.__mul__(arg)

    def __neg__(self):
        """Create a Term representing -self

        :returns:  a Term representing -self
        """
        
        return self.__mul__(-1)

    def __isub__(self, other):
        other_term = Term(other)
        self.constant -= other_term.constant
        for var, factor in other_term.factors.items():
            self.factors[var] = self.factors.get(var, 0) - factor
        return self

    def __sub__(self, other):
        """Create a Term representing self - other.

        :param other: anything that can be converted into a Term

        :returns: a Term representing self - other
        """
        res = self.copy()
        return res.__isub__(other)

    def __rsub__(self, other):
        res = self.__neg__()
        return res.__iadd__(other)

    def __le__(self, other):
        """Return a formula encoding self <= other.

        :param other: Anything that can be converted ito a Term

        :returns: a Formula object encoding self <= other
        """
        other_term = Term(other)
        return Proposition(self, LE, other_term)

    def __lt__(self, other):
        """Return a formula encoding self < other.

        :param other: Anything that can be converted ito a Term

        :returns: a Formula object encoding self < other
        """
        other_term = Term(other)
        return Proposition(self, LT, other_term)

    def __eq__(self, other):
        """Return a formula encoding self == other.

        :param other: Anything that can be converted ito a Term

        :returns: a Formula object encoding self == other
        """
        other_term = Term(other)
        return Proposition(self, EQ, other_term)

    def __gt__(self, other):
        """Return a formula encoding self > other.

        :param other: Anything that can be converted ito a Term

        :returns: a Formula object encoding self > other
        """
        other_term = Term(other)
        return Proposition(self, GT, other_term)

    def __ge__(self, other):
        """Return a formula encoding self >= other.

        :param other: Anything that can be converted ito a Term

        :returns: a Formula object encoding self >= other
        """
        other_term = Term(other)
        return Proposition(self, GE, other_term)

    def __ne__(self, other):
        """Return a formula encoding self != other.

        :param other: Anything that can be converted ito a Term

        :returns: a Formula object encoding self != other
        """
        other_term = Term(other)
        return Proposition(self, NE, other_term)

class Formula:
    """
    An abstract base class used to represent a formula tree
    """

    def accept_visitor(self, formula_visitor):
        """Call the methods of formula_visitor on the Formula.

        This is a way of making nice pattern matching of formulas, The
        object formula_visitor must contain the following functions
        that will be called in the appropriate cases:

            - on_boolean_variable(formula): this method is called when
              the Formula is a BooleanVariable

            - on_proposition(formula): this method is called when the
              Formula is a Proposition

            - on_not(formula): this method is called when the Formula
              is a negation (class Not)

            - on_and(formula): this method is called when the Formula
              is a conjunction (class And)

            - on_or(formula): this method is called when the Formula
              is a disjunction (class Or)

            - default(formula): this method is used in any other case

        :param formula_visitor: a FormulaVisitor object

        :returns: the output of the corresponding visitor method on
                  self
        """
        return formula_visitor.default(self)

    def get_variables(self):
        """Return a set of all the variables that appear in the
        formula
        """

FormulaVisitor = namedtuple(
    "FormulaVisitor",
    [
        "on_boolean_variable",
        "on_proposition",
        "on_not",
        "on_and",
        "on_or",
        "on_implies",
        "on_iff",
        "default"
    ]
)

class BinaryOp:
    """
    A parent class for encoding binary operators.
    """
    symbol = None
    def accept_visitor(self, binary_op_visitor):
        """Call the methods of binary_op_visitor on the BinaryOp.

        This is a way of making nice pattern matching of BinaryOp, The
        object binary_op_visitor must contain the following functions
        that will be called in the appropriate cases:

            - on_gt(binary_op): this method is called when the
              Operator is >

            - on_ge(binary_op): this method is called when the
              Operator is >=

            - on_eq(binary_op): this method is called when the
              Operator is ==

            - on_ne(binary_op): this method is called when the
              Operator is !=

            - on_le(binary_op): this method is called when the
              Operator is <=

            - on_lt(binary_op): this method is called when the
              Operator is <

            - default(binary_op): this method is used in any other
              case.

        :param binary_op_visitor: a BinaryOpVisitor object

        :returns: the output of the corresponding visitor method on
                  self
        """
        return binary_op_visitor.default(self)

BinaryOpVisitor = namedtuple(
    "BinaryOpVisitor",
    [
        "on_gt",
        "on_ge",
        "on_eq",
        "on_ne",
        "on_le",
        "on_lt",
        "default"
    ]
)

class _GT(BinaryOp):
    """
    Encode the binary operator >
    """
    symbol = ">"
    def accept_visitor(self, binary_op_visitor):
        return binary_op_visitor.on_gt(self)
GT = _GT()

class _GE(BinaryOp):
    """
    Encode the binary operator >=
    """
    symbol = ">="
    def accept_visitor(self, binary_op_visitor):
        return binary_op_visitor.on_ge(self)
GE = _GE()

class _EQ(BinaryOp):
    """
    Encode the binary operator ==
    """
    symbol = "="
    def accept_visitor(self, binary_op_visitor):
        return binary_op_visitor.on_eq(self)
EQ = _EQ()

class _NE(BinaryOp):
    """
    Encode the binary operator !=
    """
    symbol = "!="
    def accept_visitor(self, binary_op_visitor):
        return binary_op_visitor.on_ne(self)
NE = _NE()

class _LE(BinaryOp):
    """
    Encode the binary operator <=
    """
    symbol = "<="
    def accept_visitor(self, binary_op_visitor):
        return binary_op_visitor.on_le(self)
LE = _LE()

class _LT(BinaryOp):
    """
    Encode the binary operator <
    """
    symbol = "<"
    def accept_visitor(self, binary_op_visitor):
        return binary_op_visitor.on_lt(self)
LT = _LT()

class BooleanVariable(Formula):
    """
    Encode a named boolean variable

    :param name: The name of the variable
    """
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return self.name

    get_variables = None

    def accept_visitor(self, formula_visitor):
        return formula_visitor.on_boolean_variable(self)

class Proposition(Formula):
    """Parent class encoding a proposition of the form left_term op
    right_term.

    :param left_term: a Term representing the left side of the
        proposition
    :param operator: one of GT, GE, LT, LE, EQ, NE
    :param right_term: a Term representing the right side of the
        proposition
    """
    def __init__(self, left_term, operator, right_term):
        assert isinstance(left_term, Term)
        assert isinstance(operator, BinaryOp)
        assert isinstance(right_term, Term)
        self.left_term = left_term
        self.operator = operator
        self.right_term = right_term

    def __str__(self):
        return "{} {} {}".format(
            self.left_term,
            self.operator.symbol,
            self.right_term
        )

    def get_variables(self):
        left_variables = self.left_term.get_variables()
        right_variables = self.right_term.get_variables()
        return left_variables | right_variables

    def accept_visitor(self, formula_visitor):
        return formula_visitor.on_proposition(self)


    
class Not(Formula):
    """
    Encode the negation of a Formula.

    :param formula: the Formula that should be negated
    """
    def __init__(self, formula):
        """ Return another Formula encoding not(formula) """
        self.son = formula

    def __str__(self):
        return "Not({})".format(self.son)

    def get_variables(self):
        return self.son.get_variables()

    def accept_visitor(self, formula_visitor):
        return formula_visitor.on_not(self)

class And(Formula):
    """
    Encode the conjunction of a list of Formulas.

    The constructor takes a variable number of arguments.  In case you
    want the conjunction of a single enumerator list, use And(*list).

    :param args: The formulas you want the conjunction of
    """
    def __init__(self, *args):
        self.sons = list(args)

    def __str__(self):
        str_sons = [str(son) for son in self.sons]
        return "(" + " /\\ ".join(str_sons) + ")"

    def get_variables(self):
        res = set()
        for son in self.sons:
            res |= son.get_variables()
        return res

    def accept_visitor(self, formula_visitor):
        return formula_visitor.on_and(self)

class Or(Formula):
    """
    Encode the disjunction of a list of Formulas.

    The constructor takes a variable number of arguments.  In case you
    want the disjunction of a single enumerator list, use Or(*list).

    :param args: The formulas you want the conjunction of
    """

    def __init__(self, *args):
        self.sons = list(args)

    def __str__(self):
        str_sons = [str(son) for son in self.sons]
        return "(" + " \\/ ".join(str_sons) + ")"

    def get_variables(self):
        res = set()
        for son in self.sons:
            res |= son.get_variables()
        return res

    def accept_visitor(self, formula_visitor):
        return formula_visitor.on_or(self)

class Implies(Formula):
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __str__(self):
        return "({} -> {})".format(self.left, self.right)

    def get_variables(self):
        return self.left.get_variables() | self.right.get_variables()

    def accept_visitor(self, formula_visitor):
        return formula_visitor.on_implies(self)

class Iff(Formula):
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __str__(self):
        return "({} <-> {})".format(self.left, self.right)

    def get_variables(self):
        return self.left.get_variables() | self.right.get_variables()

    def accept_visitor(self, formula_visitor):
        return formula_visitor.on_iff(self)

TRUE = And()
FALSE = Or()
