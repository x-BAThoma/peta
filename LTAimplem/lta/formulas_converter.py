"""
Provide the class FormulaConverter which contains methods for
converting formulas defined in formulas.py.

The conversion method take a `layer' argument which is used to
instanciate the input formula at different layers.

The conversion treats State, Edges and Parameter variables separately,
ensuringfor example that only a single version of the parameter
variables exists.
"""
import pysmt.shortcuts as pysmt_s
import pysmt.typing as pysmt_t
from pysmt.walkers.dag import DagWalker
import lta.formulas as fm
import lta.lta as lta

class FormulaConverter:
    """
    A class for managing conversions between LTAs custom formulas and
    pysmt ones.

    This class maintains a dictionary associating the formulas.py
    variables to their pysmt translations.  Unless you know what you
    are doing, a single instance of this object should exist per LTA.
    """
    def __init__(self):
        self.variable_translation = {}
        self.reverse_converter = _ReverseConverter(self.variable_translation)

    def add_variable(self, pysmt_name, variable):
        """Adds a new variable name to be managed.

        Adds a binding from pysmt_name to variable so that whenever
        pysmt_name is encountered in a pysmt formula, the
        FormulaManager knows that it should be converted to variable.

        :param pysmt_name: the name of the pysmt variable to be
            managed
        :param variable: the corresponding formulas.py variable
        """
        self.variable_translation[pysmt_name] = variable

    def convert_state(self, state, layer):
        """Convert a State into a pysmt integer variable.

        Different variables are produced for different values of the
        layer argument.  This can be used to encode a CLTA for more
        than one cycle.

        :param state: The State to be converted
        :param layer: The layer where the state is converted

        :returns: a pysmt variable encoding state at the input layer
        """
        if layer is None:
            raise ValueError("State {} cannot be instanciated at None layer")
        name = "S{}({})".format(layer, state.name)
        self.add_variable(name, state)
        return pysmt_s.Symbol(name, pysmt_t.INT)

    def convert_parameter(self, param):
        """Convert a parameter variable into a pysmt integer variable.

        There is no need for a layer parameter since parameters values
        are preserved during an execution.

        :param param: the Parameter to be converted

        :returns: a pysmt variable encoding the parameter
        """
        name = "P({})".format(param.name)
        self.add_variable(name, param)
        return pysmt_s.Symbol(name, pysmt_t.INT)

    def convert_edge(self, edge, layer):
        """Convert an edge variable into a pysmt variable.

        Different variables are produced for different values of the
        layer argument.  This can be used to encode a CLTA for more
        than one cycle.  For Edges, the layer argument should be the
        same as the one of the source state.

        :param edge: the Edge to be converted
        :param layer: the layer where the edge is instanciated

        :returns: a pysmt variable encoding edge at the input layer
        """
        if layer is None:
            raise ValueError("Edge {} cannot be instanciated at None layer")
        name = "E{}({}, {})".format(layer, edge.src.name, edge.dest.name)
        self.add_variable(name, edge)
        return pysmt_s.Symbol(name, pysmt_t.INT)

    def convert_var(self, var, layer):
        """Convert any type of Variable into a pysmt integer variable.

        Call either convert_state(var, layer), convert_edge(var,
        layer) or convert_parameter(var) according to the type of var.

        Different variables are produced for different values of the
        layer argument.  This can be used to encode a CLTA for more
        than one cycle.

        In case var is of type _StateNextLayer (from lta.py), then the
        variable is instantiated at layer layer+1.  This is used by
        formulas that links successive layers.

        :param var: either a State, a Parameter or an Edge from
            lta.py.
        :param layer: the layer where the variable should be converted
            (if var is not a Parameter)

        :returns: a pysmt variable encoding var at the input layer
        """
        var_converter = lta.VariableVisitor(
            on_state=lambda state: self.convert_state(state, layer),
            on_state_next_layer=lambda state: self.convert_state(state, layer+1),
            on_parameter=lambda param: self.convert_parameter(param),
            on_edge=lambda edge: self.convert_edge(edge, layer)
        )
        return var.accept_visitor(var_converter)

    def convert_term(self, term, layer):
        """Convert a formulas.py Term into a pysmt term.

        :param term: a formulas.py Term
        :param layer: the layer where the term should be converted

        :returns: a pysmt term encoding term at the input layer
        """
        res = pysmt_s.Int(term.constant)
        for var, factor in term.factors.items():
            res += pysmt_s.Int(factor) * self.convert_var(var, layer)
        return res

    def convert_proposition(self, prop, layer):
        """Convert a formulas.py Proposition into a pysmt formula.

        :param prop: a formulas.py Proposition
        :param layer: the layer where prop should be converted

        :returns: a pysmt formula encoding prop at the input layer
        """
        left = self.convert_term(prop.left_term, layer)
        right = self.convert_term(prop.right_term, layer)
        binary_op_converter = fm.BinaryOpVisitor(
            on_gt=lambda _: pysmt_s.GT(left, right),
            on_ge=lambda _: pysmt_s.GE(left, right),
            on_eq=lambda _: pysmt_s.Equals(left, right),
            on_ne=lambda _: pysmt_s.NotEquals(left, right),
            on_le=lambda _: pysmt_s.LE(left, right),
            on_lt=lambda _: pysmt_s.LT(left, right),
            default=None
        )
        return prop.operator.accept_visitor(binary_op_converter)

    def convert_boolean_variable(self, bool_var, layer):
        """Convert a formulas.py BooleanVariable into a pysmt formula.

        :param bool_var: a formulas.py BooleanVariable

        :returns: a pysmt boolean variable encoding bool_var
        """
        name = "B{}({})".format(layer, bool_var.name)
        return pysmt_s.Symbol(name, pysmt_t.BOOL)

    def convert_not(self, not_formula, layer):
        """Convert a formulas.py Not formula into a pysmt formula.

        :param not_formula: the formulas.py Not formula to be
            converted
        :param layer: the layer at which not_formula should be
            converted

        :returns: a pysmt formula encoding not_formula at the input
                  layer
        """
        converted_son = self.convert_formula(not_formula.son, layer)
        return pysmt_s.Not(converted_son)

    def convert_and(self, and_formula, layer):
        """Convert a formulas.py And formula into a pysmt formula.

        :param and_formula: the formulas.py And formula to be
            converted
        :param layer: the layer at which and_formula should be
            converted

        :returns: a pysmt formula encoding and_formula at the input
                  layer
        """
        converted_sons = [self.convert_formula(son, layer) for son in and_formula.sons]
        return pysmt_s.And(*converted_sons)

    def convert_or(self, or_formula, layer):
        """Convert a formulas.py Or formula into a pysmt formula.

        :param or_formula: the formulas.py Or formula to be
            converted
        :param layer: the layer at which or_formula should be
            converted

        :returns: a pysmt formula encoding or_formula at the input
                  layer
        """
        converted_sons = [self.convert_formula(son, layer) for son in or_formula.sons]
        return pysmt_s.Or(*converted_sons)

    def convert_implies(self, implies_formula, layer):
        """Convert a formulas.py Implies formula into a pysmt formula.

        :param implies_formula: the formulas.py Implies formula to be
            converted
        :param layer: the layer at which implies_formula should be
            converted

        :returns: a pysmt formula encoding implies_formula at the
                  input layer
        """
        converted_left = self.convert_formula(implies_formula.left, layer)
        converted_right = self.convert_formula(implies_formula.right, layer)
        return pysmt_s.Implies(converted_left, converted_right)

    def convert_iff(self, iff_formula, layer):
        """Convert a formulas.py Iff formula into a pysmt formula.

        :param iff_formula: the formulas.py Iff formula to be
            converted
        :param layer: the layer at which iff_formula should be
            converted

        :returns: a pysmt formula encoding iff_formula at the
                  input layer
        """
        converted_left = self.convert_formula(iff_formula.left, layer)
        converted_right = self.convert_formula(iff_formula.right, layer)
        return pysmt_s.Iff(converted_left, converted_right)
        
    def convert_formula(self, formula, layer):
        """Convert a formulas.py Formula into a pysmt formula.

        Dispatch the work to either convert_boolean_variable,
        convert_proposition, convert_not, convert_and or convert_or.

        :param formula: a formulas.py Formula
        :param layer: the layer at which formula should be converted

        :returns: a pysmt formula encoding formula at the input layer
        """
        formula_converter = fm.FormulaVisitor(
            on_boolean_variable=lambda bool_var: self.convert_boolean_variable(bool_var, layer),
            on_proposition=lambda prop: self.convert_proposition(prop, layer),
            on_not=lambda not_formula: self.convert_not(not_formula, layer),
            on_and=lambda and_formula: self.convert_and(and_formula, layer),
            on_or=lambda or_formula: self.convert_or(or_formula, layer),
            on_implies=lambda implies_formula: self.convert_implies(implies_formula, layer),
            on_iff=lambda iff_formula: self.convert_iff(iff_formula, layer),
            default=None
        )
        return formula.accept_visitor(formula_converter)

    def from_py_smt(self, pysmt_formula):
        """Convert a pysmt formula back to a formulas.py one.

        :param pysmt_formula: a pysmt_formula with variables handled
            by the FormulaConverter

        :returns: a formulas.py formula.
        """
        return self.reverse_converter.walk(pysmt_formula)

class _ReverseConverter(DagWalker):
    """
    An instanciation of the DagWalker class of pysmt used to convert
    pysmt formulas back into formulas.py ones.
    """

    def __init__(self, variable_translation):
        super().__init__()
        self.variable_translation = variable_translation

    def walk_symbol(self, formula, args, **kwargs):
        name = formula.symbol_name()
        if formula.symbol_type().is_bool_type():
            # This is a utility boolean variable
            return fm.BooleanVariable(name)
        return fm.Term(self.variable_translation[name])

    def walk_int_constant(self, formula, args, **kwargs):
        val = formula.constant_value()
        return fm.Term(val)

    def walk_bool_constant(self, formula, args, **kwargs):
        if formula.constant_value():
            return fm.TRUE
        else:
            return fm.FALSE

    def walk_not(self, formula, args, **kwargs):
        return fm.Not(args[0])

    def walk_and(self, formula, args, **kwargs):
        return fm.And(*args)

    def walk_or(self, formula, args, **kwargs):
        return fm.Or(*args)

    def walk_implies(self, formula, args, **kwargs):
        return fm.Implies(args[0], args[1])

    def walk_iff(self, formula, args, **kwargs):
        return fm.Iff(args[0], args[1])
    
    def walk_equals(self, formula, args, **kwargs):
        return args[0] == args[1]

    def walk_le(self, formula, args, **kwargs):
        return args[0] <= args[1]

    def walk_lt(self, formula, args, **kwargs):
        return args[0] < args[1]

    def walk_plus(self, formula, args, **kwargs):
        return sum(args, fm.Term(0))

    def walk_times(self, formula, args, **kwargs):
        result = fm.Term(1)
        for term in args:
            # Will rise an exception if there is more than one non constant term.
            result *= term
        return result

    def walk_minus(self, formula, args, **kwargs):
        return args[0] - args[1]
