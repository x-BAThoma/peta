""" Provide the classes LTA and CLTA to manipulate (cyclic) linear threshold automata
"""
from collections import namedtuple
import lta.formulas as fm

class Variable:
    """
    Parent class for representing the variables of the formulas
    """
    def __hash__(self):
        pass
    def __eq__(self, other):
        pass
    def accept_visitor(self, visitor):
        """ Used for pattern matching, calls the appropriate function from
        a VariableVisitor object.
        """
    def term(self):
        """ Return the fm.Term associated with the state/parameter/edge """
        return fm.Term(self)

class State(Variable):
    """Specialisation of the Variable class for representing state
    variables
    """
    def __init__(self, name):
        self.name = name
        self._hash = None

    def __hash__(self):
        if self._hash is None:
            self._hash = hash((type(self), self.name))
        return self._hash

    def __eq__(self, other):
        return (type(self), self.name) == (type(other), other.name)

    def __str__(self):
        return "S({})".format(self.name)

    def accept_visitor(self, visitor):
        """Used for pattern matching, calls the appropriate function
        from a VariableVisitor object.
        """
        return visitor.on_state(self)

    def term_prev_layer(self):
        """Return a Term that can be used in the previous layer.

        Normally, formulas over an lta can only use variables from a
        single layer.  However, this condition can easilly be relaxed
        to also allow state variable from the next layer.  This method
        allow converting a state into a term that will be treated as
        belonging to the previous layer.

        :returns: a formulas.py Term that can be used together with
                  variables of the previous layer.
        """
        return _StatePrevLayer(self.name).term()
        

class _StatePrevLayer(State):
    """A variant of state that can be used to have variables of layer
    l+1 in a formula of layer l
    """
    def __str__(self):
        return "S'({})".format(self.name)

    def accept_visitor(self, visitor):
        return visitor.on_state_next_layer(self)

class Parameter(Variable):
    """Specialisation of the Variable class for representing parameter
    variables
    """
    def __init__(self, name):
        self.name = name
        self._hash = None

    def __hash__(self):
        if self._hash is None:
            self._hash = hash((type(self), self.name))
        return self._hash

    def __eq__(self, other):
        return (type(self), self.name) == (type(other), other.name)

    def __str__(self):
        return "P({})".format(self.name)

    def accept_visitor(self, visitor):
        """Used for pattern matching, calls the appropriate function
        from a VariableVisitor object.
        """
        return visitor.on_parameter(self)

class Edge(Variable):
    """Specialisation of the Variable class for representing edge
    variables
    """

    def __init__(self, src, dest):
        self.src = src
        self.dest = dest
        self._hash = None

    def __hash__(self):
        if self._hash is None:
            self._hash = hash((type(self), self.src, self.dest))
        return self._hash

    def __eq__(self, other):
        return (type(self), self.src, self.dest) == (type(other), other.src, other.dest)

    def __str__(self):
        return "E({}, {})".format(self.src.name, self.dest.name)

    def accept_visitor(self, visitor):
        return visitor.on_edge(self)

VariableVisitor = namedtuple(
    "VariableVisitor",
    [
        "on_parameter",
        "on_state",
        "on_state_next_layer",
        "on_edge"
    ]
)

class LTA:
    """ A class for building Layered Threshold Automata """

    def __init__(self, length):
        """ Initialises an empty LTA of given length """
        self.parameters = set()
        self.guards = [{} for _ in range(length)]

    def __len__(self):
        return len(self.guards)

    def get_parameters(self):
        return self.parameters

    def get_states(self, layer_index):
        return self.guards[layer_index].keys()

    def get_successors(self, layer_index, state):
        return self.guards[layer_index][state].keys()
    
    def get_guard(self, layer_index, src, dest):
        return self.guards[layer_index][src].get(dest, fm.FALSE)

    def get_edge_variable(self, layer_index, src, dest):
        """ Return an edge variable between states src and dest. the index
        layer_index must corresponds to the layer of the source state,
        dest must then be in the subsequent layer
        """
        next_layer_index = self.next_layer_index(layer_index)
        if src not in self.get_states(layer_index):
            raise KeyError("No state {} in layer {}".format(src, layer_index))
        if dest not in self.get_states(next_layer_index):
            raise KeyError("No state {} in layer {}".format(dest, next_layer_index))
        return Edge(src, dest)

    def add_parameter(self, param_name):
        """ Add a parameter called param_name to the automaton and return it
        """
        param = Parameter(param_name)
        if param in self.get_parameters():
            raise KeyError(
                "There is already a parameter called {}".format(param_name)
            )
        self.parameters.add(param)
        return param

    def add_state(self, layer_index, state_name):
        """ Add a state called state_name to the given layer of the automaton
        and return it
        """
        state = State(state_name)
        if state in self.get_states(layer_index):
            raise KeyError(
                "There is already a state called {}".format(state_name)
            )
        self.guards[layer_index][state] = {}
        return state

    def check_guard(self, layer_index, guard):
        """ Return True iff the input guard contains only variables that are
        parameters or state of the given layer. If the input layer is
        None, then will return True only if no state are in the variables
        of the formula.
        """
        var_checker = VariableVisitor(
            on_state=lambda state: (
                layer_index is not None and
                state in self.get_states(layer_index)
            ),
            on_state_next_layer=lambda state: (
                layer_index is not None and
                state in self.get_states(self.next_layer_index(layer_index))
            ),
            on_parameter=lambda param: (param in self.parameters),
            on_edge=lambda edge: (
                layer_index is not None and
                edge.src in self.get_states(layer_index)
            )
        )
        for var in guard.get_variables():
            if not var.accept_visitor(var_checker):
                return False
        return True

    def next_layer_index(self, layer_index):
        """ Return layer index of the layer that follows the input one """
        return self.get_layer_index(layer_index + 1)

    def get_layer_index(self, layer):
        """Return the index of the layer corresponding to the input integer,
        using modulo when applicable """
        if 0 <= layer < len(self):
            return layer
        raise IndexError("Attempted to access beyond the last layer")

    def set_guard(self, layer_index, src, dest, guard):
        """ Adds a guard between state src of the given layer to the state
        dest of the next one.
        """
        next_index = self.next_layer_index(layer_index)
        if src not in self.get_states(layer_index):
            raise KeyError("No state {} in layer {}".format(src, layer_index))
        if dest not in self.get_states(next_index):
            raise KeyError("No state {} in layer {}".format(dest, layer_index))
        if not self.check_guard(layer_index, guard):
            raise ValueError("{} contains unsupported variables".format(guard))
        self.guards[layer_index][src][dest] = guard

    def get_parameter_constraints(self):
        """Return conditions p >= 0 for every parameter p of the lta

        :returns: A list of formulas.py formulas
        """
        return [p.term() >= 0 for p in self.get_parameters()]

    def out_constraints(self, layer_index):
        """Return the list of constraints that concerns the states and
        out going edges of the given layer index.

        :param layer_index: The index of the layer of the LTA that
            should be encoded

        :returns: A list of formulas.py formulas
        """
        no_negative_states = [s.term() >= 0 for s in self.get_states(layer_index)]
        
        no_negative_edges = []
        for src in self.get_states(layer_index):
            for dest in self.get_successors(layer_index, src):
                edge = self.get_edge_variable(layer_index, src, dest)
                no_negative_edges.append(edge.term() >= 0)

        out_flow_coherence = []
        for src in self.get_states(layer_index):
            sum_out_edges = fm.Term(0)
            for dest in self.get_successors(layer_index, src):
                edge = self.get_edge_variable(layer_index, src, dest)
                sum_out_edges += edge.term()
            out_flow_coherence.append(src.term() >= sum_out_edges)

        guard_coherence = []
        for src in self.get_states(layer_index):
            for dest in self.get_successors(layer_index, src):
                edge = self.get_edge_variable(layer_index, src, dest)
                guard = self.get_guard(layer_index, src, dest)
                guard_coherence.append(fm.Or(edge.term() == 0, guard))

        return no_negative_states + no_negative_edges + out_flow_coherence + guard_coherence

    def in_constraints(self, layer_index):
        """Return the list of constraints that concerns the outgoing
        edges of the given layer index as well as the states of the
        next layer_index.

        :param layer_index: The (cyclic) index of the layer of the
            (C)LTA that should be encoded

        :returns: A list of formulas.py formulas
        """
        next_index = self.next_layer_index(layer_index)
        # We first compute the incoming edges for each state at layer next_index
        in_edges = {s: [] for s in self.get_states(next_index)}
        for src in self.get_states(layer_index):
            for dest in self.get_successors(layer_index, src):
                edge = self.get_edge_variable(layer_index, src, dest)
                in_edges[dest].append(edge)
        # We can now build the formulas
        in_flow_coherence = []
        for dest, edges in in_edges.items():
            sum_edges = sum([edge.term() for edge in edges])
            # We need to use term_prev_layer in order to tell formulas_converter.py
            # that this state belongs to the next layer of the lta compared to
            # other variables of the formula.
            dest_var = dest.term_prev_layer() 
            in_flow_coherence.append(sum_edges == dest_var)
        return in_flow_coherence

class CLTA(LTA):
    """ A class for building Cyclic Layered Threshold Automata """
    def get_layer_index(self, layer):
        return layer % len(self)
