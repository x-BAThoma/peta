import lta.formulas as fm

def test0():
    t = fm.Term(0)
    assert str(t) == "0"

def test1():
    t = fm.Term("test")
    assert str(t) == "test"

def test2():
    t = fm.Term(1) + fm.Term("x")
    assert str(t) == "x + 1"

def test3():
    t = -3*fm.Term("y")
    assert str(t) == "-3 y"

def test4():
    t = -(fm.Term("x") + 2*fm.Term("y") - fm.Term(3))
    assert str(t) == "-x - 2 y + 3"
