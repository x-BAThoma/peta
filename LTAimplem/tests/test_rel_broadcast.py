import lta.formulas as fm
from lta.lta import LTA
from lta.lta_encoder2 import LTAEncoder

def setup_rel_broadcast():
    rel_broadcast = LTA(3)
    n = rel_broadcast.add_parameter("n")
    tn = n.term()
    t = rel_broadcast.add_parameter("t")
    tt = t.term()
    f = rel_broadcast.add_parameter("f")
    tf = f.term()
    
    v0 = rel_broadcast.add_state(0, "v0")
    tv0 = v0.term()
    v1 = rel_broadcast.add_state(0, "v1")
    tv1 = v1.term()
    p = rel_broadcast.add_state(1, "p")
    tp = p.term()
    acc = rel_broadcast.add_state(2, "acc")
    tacc = acc.term()

    rel_broadcast.set_guard(0, v0, p, tv1 + tf > tt)
    rel_broadcast.set_guard(0, v1, p, fm.TRUE)
    rel_broadcast.set_guard(1, p, acc, tp + tf >= tn - tt)

    
edge_v0_p = rel_broadcast.get_edge_variable(0, v0, p)
rel_broadcast.set_guard(0, v1, p, fm.TRUE)
edge_v1_p = rel_broadcast.get_edge_variable(0, v1, p)
rel_broadcast.set_guard(1, p, acc, p.term() + f.term() >= n.term() - t.term())
edge_p_acc = rel_broadcast.get_edge_variable(1, p, acc)

fair_v0 = fm.Implies(v1.term() > t.term(), v0.term() == edge_v0_p.term())
fair_v1 = fm.Implies(fm.TRUE, v1.term() == edge_v1_p.term())
fair_p = fm.Implies(p.term() >= n.term() - t.term(), p.term() == edge_p_acc.term())

encoder_correct = LTAEncoder(rel_broadcast, 0, 3)
encoder_correct.add_assertions(None, 2 * t.term() < n.term())
encoder_correct.add_assertions(None, f.term() <= t.term())
encoder_correct.add_assertions(0, v0.term() + v1.term() + f.term() == n.term())

encoder_wrong = LTAEncoder(rel_broadcast, 0, 3)
encoder_wrong.add_assertions(None, 2 * t.term() < n.term())
encoder_wrong.add_assertions(None, f.term() <= t.term() + 1) #One too many faults
encoder_wrong.add_assertions(0, v0.term() + v1.term() + f.term() == n.term())

def test0():
    encoder_correct.add_assertions(0, v1.term() == 0)
    encoder_correct.add_assertions(2, acc.term() > 0)
    assert not encoder_correct.solve()
    
def test1():
    encoder_wrong.add_assertions(0, v1.term() == 0)
    encoder_wrong.add_assertions(2, acc.term() > 0)
    assert encoder_wrong.solve()

def test2():
    assert encoder_correct.solve()
    encoder_correct.add_assertions(0, v1.term() > t.term())
    encoder_correct.add_assertions(2, acc.term() + f.term() < n.term())
    encoder_correct.add_assertions(0, fair_v0)
    encoder_correct.add_assertions(0, fair_v1)
    encoder_correct.add_assertions(1, fair_p)
    assert not encoder_correct.solve()

def test3():
    encoder_wrong.add_assertions(0, v1.term() > t.term())
    encoder_wrong.add_assertions(2, acc.term() + f.term() < n.term())
    encoder_wrong.add_assertions(0, fair_v0)
    encoder_wrong.add_assertions(0, fair_v1)
    encoder_wrong.add_assertions(1, fair_p)
    assert encoder_wrong.solve()
